<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Bank;

class BankSeeder extends Seeder
{
    public function run()
    {
        $bank = array(
            [
                "id" => "1",
                "title" => "Bank Mandiri", 
                "photo" => "bank/mandiri.png"
            ]
        );

        foreach($bank AS $b){
            Bank::create([
                'id' => $b['id'],
                'title' => $b['title'],
                'photo' => $b['photo']
            ]);
        };
    }
}
