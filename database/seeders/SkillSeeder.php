<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Skill;
use Illuminate\Support\Str;

class SkillSeeder extends Seeder
{
    public function run()
    {
        $skill = array(
            [
                "id" => "1",
                "title" => "PHP"
            ],
            [
                "id" => "2",
                "title" => "Codeigniter"
            ],
            [
                "id" => "3",
                "title" => "Laravel"
            ],
            [
                "id" => "4",
                "title" => "YII"
            ]
        );
        foreach($skill as $s)
        {
            Skill::create([
                'id' => $s['id'],
                'title' => Str::title($s['title']),
                'slug' => Str::slug($s['title'])
            ]);
        };
    }
}
