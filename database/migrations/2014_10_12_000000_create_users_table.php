<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('ktp',16)->nullable();
            $table->string('name',100);
            $table->string('email',150);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone',15)->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->string('password');
            $table->string('photo')->nullable();
            $table->string('place_birth')->nullable();
            $table->date('date_birth')->nullable();
            $table->enum('gender',['Pria','Wanita'])->nullable();
            $table->longText('address')->nullable();
            $table->integer('province_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->bigInteger('subdistrict_id')->nullable();
            $table->bigInteger('village_id')->nullable();
            $table->string('postcode',5)->nullable();
            $table->longText('bio')->nullable();
            $table->string('url')->nullable();
            $table->enum('is_notify',['y','n'])->default('n');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            
        });
    }
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}