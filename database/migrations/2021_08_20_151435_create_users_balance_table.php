<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersBalanceTable extends Migration
{
    public function up()
    {
        Schema::create('users_balance', function (Blueprint $table) {
            $table->id();
            $table->enum('type',['-','D','W'])->default('-');
            $table->integer('project_id')->default(0);
            $table->integer('payment_method_id')->default(0);
            $table->integer('user_bank_id')->default(0);
            $table->float('amount',20,0)->default(0);
            $table->float('fee_transfer',20,0)->default(0);
            $table->float('fee',20,0)->default(0);
            $table->enum('st',['-','Penyetoran dalam proses','Penyetoran Berhasil','Penyetoran gagal','Penarikan dalam proses','Penarikan Berhasil','Penarikan Gagal'])->default('-');
            $table->integer('created_by')->default(0);
            $table->timestamp('created_at');
        });
    }
    public function down()
    {
        Schema::dropIfExists('users_balance');
    }
}
