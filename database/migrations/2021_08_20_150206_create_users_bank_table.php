<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersBankTable extends Migration
{
    public function up()
    {
        Schema::create('users_bank', function (Blueprint $table) {
            $table->id();
            $table->integer('bank_id');
            $table->string('bank_account');
            $table->enum('st',['Dalam verifikasi','Terverifikasi','Tidak valid'])->default('Dalam verifikasi');
            $table->integer('created_by');
            $table->timestamp('created_at');
            $table->integer('verified_by');
            $table->timestamp('verified_at')->default(\DB::raw('CURRENT_TIMESTAMP'));;
        });
    }
    public function down()
    {
        Schema::dropIfExists('users_bank');
    }
}
