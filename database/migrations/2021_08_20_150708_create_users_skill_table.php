<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersSkillTable extends Migration
{
    public function up()
    {
        Schema::create('users_skill', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('skill');
        });
    }
    public function down()
    {
        Schema::dropIfExists('users_skill');
    }
}
