<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegionalTable extends Migration
{
    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('lat');
            $table->string('lng');
            $table->softDeletes();
        });
        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('subdistricts', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->softDeletes();
        });
        Schema::create('villages', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->softDeletes();
        });
    }
    public function down()
    {
        Schema::table('provinces', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('subdistricts', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('villages', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
