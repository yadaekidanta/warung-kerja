function auth_content(obj)
{
    $("#login_page").hide();
    $("#register_page").hide();
    $("#forgot_page").hide();
    $("#" + obj).show();
}
function do_auth(tombol, form, url, method)
{
    $(tombol).submit(function() {
        return false;
    });
    let data = $(form).serialize();
    $(tombol).prop("disabled", true);
    $.ajax({
        type: method,
        url: url,
        data: data,
        dataType: 'json',
        beforeSend: function() {

        },
        success: function(response) {
            $("#notif_messages").html(response.message);
            if (response.alert == "success") {
                $("#custom_notif").removeClass('hide');
                $("#custom_notif").addClass('show');
                $(form)[0].reset();
                if(response.callback){
                    location.reload();
                }
                setTimeout(function() {
                    $(tombol).prop("disabled", false);
                    $("#custom_notif").removeClass('show');
                    $("#custom_notif").addClass('hide');
                    auth_content('login_page');
                }, 2000);
            } else {
                $("#custom_notif").removeClass('hide');
                $("#custom_notif").addClass('show');
                setTimeout(function() {
                    $("#custom_notif").removeClass('show');
                    $("#custom_notif").addClass('hide');
                    $(tombol).prop("disabled", false);
                }, 2000);
            }
        },
    });
}