function auth_content(obj) {
    $("#login_page").hide();
    $("#register_page").hide();
    $("#forgot_page").hide();
    $("#" + obj).show();
}

function do_auth(tombol, form, url, method) {
    $(tombol).submit(function() {
        return false;
    });
    let data = $(form).serialize();
    $(tombol).prop("disabled", true);
    $.ajax({
        type: method,
        url: url,
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $("#spinner-loading").toggleClass('d-none')
        },
        success: function(response) {
            // alert()
            $("#register_form_button").prop('disabled', true)
            $("#login_form_button").prop('disabled', true)
            $("#spinner-loading").toggleClass('d-none')
            if (response.alert == "success") {
                $("#register_form_button").prop('disabled', false)
                $("#login_form_button").prop('disabled', false)
                success_toastr(response.message);
                $(form)[0].reset();
                if (response.callback == 'reload') {
                    location.href = "/";
                } else if (response.callback == 'reset') {
                    location.href = '../';
                } else {
                    auth_content('login_page');
                }
                setTimeout(function() {
                    $(tombol).prop("disabled", false);
                }, 2000);
            } else {
                error_toastr(response.message);
                setTimeout(function() {
                    $(tombol).prop("disabled", false);
                }, 2000);
            }
        },
    });
}