<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use Illuminate\Support\Facades\Auth;
class AccountController extends Controller
{
    use ResponseView, ReplyJson;
    public function index()
    {
        $profile = User::where('id', Auth::guard('member')->user()->id)->first();
        return $this->render_view('account.main', compact('profile'));
    }
}