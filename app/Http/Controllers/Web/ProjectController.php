<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    use ResponseView, ReplyJson;
    public function index()
    {
        return $this->render_view('project.main');
    }
    public function create()
    {
    }
    public function store(Request $request)
    {
        // request name of data :
        // title (string)
        // skills (array)
        // bidBudgetFrom (string)
        // bidBudgetTo (string)
        // description (text)
        // finishDay (integer)
        // attachment (type file) (optional , can null or length 0) using count($request->file()) to check the attachment exists or not

        $data = null; // change the value

        return response()->json([
            'message' => "Berhasil menambahkan proyek!",
            'data' => $data,
        ]);
    }

    public function create_bid(Request $request)
    {
        // request name of data :
        // amount (string)
        // attachment (type file) (optional , can null or length 0) using count($request->file()) to check the attachment exists or not
        // message (text)

        $data = null; // change the value

        return response()->json([
            'message' => "Berhasil menambahkan bid baru!",
            'data' => $data,
        ]);
    }

    public function show(Project $project)
    {
        //
    }
    public function edit(Project $project)
    {
        //
    }
    public function update(Request $request, Project $project)
    {
        //
    }
    public function destroy(Project $project)
    {
        //
    }

    public function detail(Project $project)
    {
        $project = null; //comment or change the value this after success get data from database!
        return $this->render_view('project.detail', $project);
    }

    public function my_projects()
    {
        $profile = User::where('id', Auth::guard('member')->user()->id)->first();
        return $this->render_view('account.my_project', compact('profile'));
    }

    public function my_bids()
    {
        
        $profile = User::where('id', Auth::guard('member')->user()->id)->first();
        return $this->render_view('account.my_bid', compact('profile'));
    }

    public function my_products()
    {
        $profile = User::where('id', Auth::guard('member')->user()->id)->first();
        return $this->render_view('account.my_product', compact('profile'));
    }

    public function my_purchases()
    {
        $profile = User::where('id', Auth::guard('member')->user()->id)->first();
        return $this->render_view('account.my_purchases', compact('profile'));
    }

    public function my_sales()
    {
        $profile = User::where('id', Auth::guard('member')->user()->id)->first();
        return $this->render_view('account.my_sales', compact('profile'));
    }

    public function my_finance()
    {
        $profile = User::where('id', Auth::guard('member')->user()->id)->first();
        return $this->render_view('account.my_finance', compact('profile'));
    }

    public function my_points()
    {
        $profile = User::where('id', Auth::guard('member')->user()->id)->first();
        return $this->render_view('account.my_points', compact('profile'));
    }

    public function my_referals()
    {
        $profile = User::where('id', Auth::guard('member')->user()->id)->first();
        return $this->render_view('account.my_referals', compact('profile'));
    }
}