<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Traits\ReplyJson;
use Illuminate\Http\Request;
use App\Traits\ResponseView;

class CartController extends Controller
{
    use ResponseView, ReplyJson;

    public function index()
    {
        return $this->render_view('cart.main');
    }
}
