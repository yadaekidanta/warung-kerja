<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;

class CategoryController extends Controller
{
    use ResponseView, ReplyJson;
    public function index()
    {
        return $this->render_view('auth.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Category $category)
    {
        //
    }
    public function edit(Category $category)
    {
        //
    }
    public function update(Request $request, Category $category)
    {
        //
    }
    public function destroy(Category $category)
    {
        //
    }
}