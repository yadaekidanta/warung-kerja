<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Province;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Rules\MatchOldPassword;
use App\Mail\PasswordChangedMail;
use App\Mail\DeactivationMail;

class ProfileController extends Controller
{
    use ResponseView, ReplyJson;
    public function index()
    {
        $profile = User::where('id', Auth::guard('member')->user()->id)->first();
        return $this->render_view('profile.main',compact('profile'));
    }
    public function edit()
    {
        $province = Province::get();
        $profile = User::where('id', Auth::guard('member')->user()->id)->first();
        return $this->render_view('profile.input',compact('profile','province'));    
    }
    public function profilesave(Request $request)
    {
        $user = User::where('id', Auth::guard('member')->user()->id)->first();
        // if (request()->file('photo')) {
        //     Storage::delete($user->photo);
        //     $file = request()->file('photo')->store("employee");
        //     $user->photo = $file;
        // }
        $user->name = Str::title($request->name);
        $user->date_birth = $request->date_birth;
        $user->gender = $request->gender;
        $user->place_birth = $request->place_birth;
        $user->ktp = $request->ktp;
        $user->postcode = $request->postcode;
        $user->bio = $request->bio;
        $user->address = $request->address;
        $user->province_id = $request->province;
        $user->city_id = $request->city;
        $user->subdistrict_id = $request->subdistrict;
        $user->village_id = $request->village;
        $user->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Profile Updated',
        ]);
    }
    public function cpassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('current_password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('current_password'),
                ]);
            } elseif ($errors->has('new_password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('new_password'),
                ]);
            } elseif ($errors->has('new_confirm_password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('new_confirm_password'),
                ]);
            }
        }
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
        $profile = User::where('id',Auth::guard('member')->user()->id)->first();
        Mail::to($profile->email)->send(new PasswordChangedMail($profile));
        $profile = Auth::guard('member')->user();
        Auth::logout($profile);
        return response()->json([
            'alert' => 'success',
            'message' => 'Password changed successfully',
            'redirect' => route('web.auth.password_changed'),
        ]);
    }

    public function addSkills(Request $request)
    {
        // request name of data :
        // skills (array)

        $data = null; // change the value

        return response()->json([
            'message' => "Berhasil menambahkan proyek!",
            'data' => $data,
        ]);
    }
}