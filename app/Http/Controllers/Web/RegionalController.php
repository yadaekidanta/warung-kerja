<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Province;
use App\Models\City;
use App\Models\User;
use App\Models\Subdistrict;
use App\Models\Village;

class RegionalController extends Controller
{
    public function province(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Province::where('name','like','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.regional.province_list', compact('collection'));
        }
        return view('page.office.regional.province');
    }
    public function city(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = City::select(
                "provinces.id as id_prov",
                "provinces.name as name_prov",
                "cities.id as id_ct",
                "cities.name as name_ct"
            )
            ->leftJoin("provinces", "provinces.id", "=", DB::raw('SUBSTR(cities.id,1,2)'))
            ->where('provinces.name','like','%'.$keywords.'%')
            ->orwhere('cities.name','like','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.regional.city_list', compact('collection'));
        }
        return view('page.office.regional.city');
    }
    public function get_city(Request $request,User $user)
    {
        $collection = City::select(
            "provinces.id as id_prov",
            "provinces.name as name_prov",
            "cities.id as id_ct",
            "cities.name as name_ct"
        )
        ->leftJoin("provinces", "provinces.id", "=", DB::raw('SUBSTR(cities.id,1,2)'))
        ->where('provinces.id',$request->id_province)->get();
        $list = "<option>Pilih Kota/Kabupaten</option>";
        foreach($collection as $row){
            $list.="<option value='$row->id_ct'$row->id==$user->city_id?selected:''>$row->name_ct</option>";
        }
        echo $list;
    }
    public function subdistrict(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Subdistrict::select(
                "provinces.id as id_prov",
                "provinces.name as name_prov",
                "cities.id as id_ct",
                "cities.name as name_ct",
                "subdistricts.id as id_sd",
                "subdistricts.name as name_sd"
            )
            ->leftJoin("provinces", "provinces.id", "=", DB::raw('SUBSTR(subdistricts.id,1,2)'))
            ->leftJoin("cities", "cities.id", "=", DB::raw('SUBSTR(subdistricts.id,1,4)'))
            ->where('provinces.name','like','%'.$keywords.'%')
            ->orwhere('cities.name','like','%'.$keywords.'%')
            ->orwhere('subdistricts.name','like','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.regional.subdistrict_list', compact('collection'));
        }
        return view('page.office.regional.subdistrict');
    }
    public function get_subdistrict(Request $request, User $user)
    {
        $collection = Subdistrict::select(
            "provinces.id as id_prov",
            "provinces.name as name_prov",
            "cities.id as id_ct",
            "cities.name as name_ct",
            "subdistricts.id as id_sd",
            "subdistricts.name as name_sd"
        )
        ->leftJoin("provinces", "provinces.id", "=", DB::raw('SUBSTR(subdistricts.id,1,2)'))
        ->leftJoin("cities", "cities.id", "=", DB::raw('SUBSTR(subdistricts.id,1,4)'))
        ->where('cities.id',$request->id_city)->get();
        $list = "<option>Pilih Kecamatan</option>";
        foreach($collection as $row){
            $list.="<option value='$row->id_sd'$row->id==$user->subdistrict_id?selected:''>$row->name_sd</option>";
        }
        echo $list;
    }
    public function village(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Village::select(
                "provinces.id as id_prov",
                "provinces.name as name_prov",
                "cities.id as id_ct",
                "cities.name as name_ct",
                "subdistricts.id as id_sd",
                "subdistricts.name as name_sd",
                "villages.id as id_v",
                "villages.name as name_v"
            )
            ->leftJoin("provinces", "provinces.id", "=", DB::raw('SUBSTR(villages.id,1,2)'))
            ->leftJoin("cities", "cities.id", "=", DB::raw('SUBSTR(villages.id,1,4)'))
            ->leftJoin("subdistricts", "subdistricts.id", "=", DB::raw('SUBSTR(villages.id,1,7)'))
            ->where('provinces.name','like','%'.$keywords.'%')
            ->orwhere('cities.name','like','%'.$keywords.'%')
            ->orwhere('subdistricts.name','like','%'.$keywords.'%')
            ->orwhere('villages.name','like','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.regional.village_list', compact('collection'));
        }
        return view('page.office.regional.village');
    }
    public function get_village(Request $request,User $user)
    {
        $collection = Village::select(
            "provinces.id as id_prov",
            "provinces.name as name_prov",
            "cities.id as id_ct",
            "cities.name as name_ct",
            "subdistricts.id as id_sd",
            "subdistricts.name as name_sd",
            "villages.id as id_v",
            "villages.name as name_v"
        )
        ->leftJoin("provinces", "provinces.id", "=", DB::raw('SUBSTR(villages.id,1,2)'))
        ->leftJoin("cities", "cities.id", "=", DB::raw('SUBSTR(villages.id,1,4)'))
        ->leftJoin("subdistricts", "subdistricts.id", "=", DB::raw('SUBSTR(villages.id,1,7)'))
        ->where('subdistricts.id',$request->id_subdistrict)->get();
        $list = "<option>Pilih Kelurahan/Desa</option>";
        foreach($collection as $row){
            $list.="<option value='$row->id_v'$row->id==$user->village_id?selected:''>$row->name_v</option>";
        }
        echo $list;
    }
}
