<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    use ResponseView, ReplyJson;
    public function index()
    {
        return $this->render_view('product.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        // request name of data :
        // title (string)
        // description (text)
        // tags (array string)
        // price (string)
        // deliverable (type file) using count($request->file()) to check the attachment exists or not
        // trial_version (type file) using count($request->file()) to check the attachment exists or not


        $data = null; // change the value

        return response()->json([
            'message' => "Berhasil menambahkan proyek!",
            'data' => $request->all(),
        ]);
    }
    public function show(Product $product)
    {
        //
    }
    public function edit(Product $product)
    {
        //
    }
    public function update(Request $request, Product $product)
    {
        //
    }
    public function destroy(Product $product)
    {
        //
    }

    public function add_cart(Request $request)
    {
        // request name of data :
        // id_product (string)
        $id_user = Auth::id();
        $data = null; // change the value

        return response()->json([
            'message' => "Berhasil menambahkan product!",
            'data' => $data,
        ]);
    }

    public function detail(Project $project)
    {
        $project = null; //comment or change the value this after success get data from database!
        return $this->render_view('product.detail', $project);
    }
}