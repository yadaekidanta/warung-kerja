<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;

class HomeController extends Controller
{
    use ResponseView, ReplyJson;
    public function index()
    {
        return $this->render_view('home.main');
    }
}