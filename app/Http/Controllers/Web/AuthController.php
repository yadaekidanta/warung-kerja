<?php

namespace App\Http\Controllers\Web;

use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Traits\ReplyJson;
use App\Traits\ResponseView;
use App\Models\UserVerify;
use App\Mail\RequestPasswordMail;
use App\Mail\PasswordChangedMail;
use App\Mail\VerificationMail;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Exception;

class AuthController extends Controller
{
    use ResponseView, ReplyJson;
    public function __construct()
    {
        $this->middleware('guest:member')->except('resend_mail','verification','do_logout');
    }
    public function index()
    {
        return $this->render_view('auth.main');
    }

    public function register()
    {
        return $this->render_view('auth.main', ['register' => true]);
    }

    public function do_register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|unique:users|min:9|max:13',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }elseif ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }
        }
        $user = new User;
        $user->name = Str::title($request->name);
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        $token = Str::random(64);

        UserVerify::create([
            'user_id' => $user->id,
            'token' => $token
        ]);
        Mail::to($request->email)->send(new VerificationMail($user, $token));
        return response()->json([
            'alert' => 'success',
            'message' => 'Terima kasih telah bergabung di Warung Kerja'. $request->name,
        ]);
    }
    public function do_login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }
        }
        if(Auth::guard('member')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            if (Auth::guard('member')->user()->email_verified_at) {
                return response()->json([
                    'alert' => 'success',
                    'message' => 'Selamat datang '. Auth::guard('member')->user()->name,
                    'callback' => 'reload',
                ]);
            }else{
                $user = Auth::guard('member')->user();
                Auth::logout($user);
                return response()->json([
                    'alert' => 'error',
                    'message' => 'Harap verifikasi email anda terlebih dahulu',
                ]);
            }
        }else{
            return response()->json([
                'alert' => 'error',
                'message' => 'Maaf, sepertinya ada beberapa kesalahan yang terdeteksi, silakan coba lagi.',
            ]);
        }
    }
    public function do_forgot(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }
        }
        $user = User::where('email',$request->email)->first();

        $token = Str::random(64);
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);
        $data = array(
            'token' => $token,
            'user' => $user
        );
        Mail::to($request->email)->send(new RequestPasswordMail($data));
        return response()->json([
            'alert' => 'success',
            'message' => 'Kami telah mengirim email tautan reset kata sandi Anda!',
            'callback' => 'forgot',
        ]);
    }
    public function do_verify($token)
    {
        $verifyUser = UserVerify::where('token', $token)->first();

        $message = 'Maaf email Anda tidak dapat diidentifikasi.';

        if (!is_null($verifyUser)) {
            $user = $verifyUser->user;

            if (!$user->email_verified_at) {
                $verifyUser->user->email_verified_at = date("Y-m-d H:i:s");
                $verifyUser->user->save();
                $message = "Email Anda telah diverifikasi. Anda sekarang dapat masuk.";
            } else {
                $message = "Email Anda sudah diverifikasi. Anda sekarang dapat masuk.";
            }
        }

        return redirect()->route('web.auth')->with('message', $message);
    }
    public function do_reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required|exists:password_resets',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('token')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('token'),
                ]);
            } elseif ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            } elseif ($errors->has('password_confirmation')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password_confirmation'),
                ]);
            }
        }

        $updatePassword = DB::table('password_resets')
            ->where([
                'token' => $request->token
            ])
            ->first();

        if (!$updatePassword) {
            return response()->json([
                'alert' => 'error',
                'message' => 'Token tidak valid!',
            ]);
        }
        $user = User::where('email', $updatePassword->email)
        ->update(['password' => Hash::make($request->password)]);
        
        $users = User::where('email', $updatePassword->email)
        ->first();
        DB::table('password_resets')->where(['email' => $updatePassword->email])->delete();
        Mail::to($users->email)->send(new PasswordChangedMail($users));
        return response()->json([
            'alert' => 'success',
            'message' => 'Kata sandi Anda telah diubah!',
            'callback' => 'reset',
        ]);
    }
    public function password_changed()
    {
        return view('page.web.desktop.auth.password_changed');
    }
    public function resend_mail()
    {
        $users = User::where('id', Auth::user()->id)->first();
        $token = Str::random(64);
        UserVerify::create([
            'user_id' => $users->id,
            'token' => $token
        ]);
        Mail::to($users->email)->send(new VerificationMail($users, $token));
        return response()->json([
            'alert' => 'info',
            'message' => 'Kami telah mengirim ulang aktivasi ke email Anda'
        ]);
    }
    public function verification()
    {
        return view('page.web.profile.notice');
    }
    public function deactivated(User $user)
    {
        return view('page.web.auth.deactivate');
    }
    public function do_logout()
    {
        $user = Auth::guard('member')->user();
        Auth::logout($user);
        return redirect()->route('web.home');
    }
    public function reset($token)
    {
        return $this->render_view('auth.reset',compact('token'));
    }
}