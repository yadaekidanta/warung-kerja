<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\CartController;
use App\Http\Controllers\Web\AuthController;
use App\Http\Controllers\Web\HomeController;
use App\Http\Controllers\Web\AccountController;
use App\Http\Controllers\Web\ProductController;
use App\Http\Controllers\Web\ProfileController;
use App\Http\Controllers\Web\RegionalController;
use App\Http\Controllers\Web\ProjectController;
use App\Http\Controllers\Web\CategoryController;
use App\Http\Controllers\web\PortofolioController;
use Tabuna\Breadcrumbs\Trail;

Route::group(['domain' => ''], function() {
    Route::name('web.')->group(function(){
        Route::get('', [HomeController::class, 'index'])->name('home');
        Route::get('category', [CategoryController::class, 'index'])->name('category');
        Route::get('product', [ProductController::class, 'index'])->name('product');
        Route::get('product/detail/{id}', [ProductController::class, 'detail'])->name('product.detail');
        Route::get('project', [ProjectController::class, 'index'])->name('project');
        Route::get('project/detail/{id}', [ProjectController::class, 'detail'])->name('project.detail');
        Route::get('auth', [AuthController::class, 'index'])->name('auth');
        Route::get('auth/register', [AuthController::class, 'register'])->name('auth.view.register');
        Route::resource('finance', FinanceController::class);
        Route::resource('balance', BalanceController::class);
        Route::resource('topup', TopUpController::class);
        Route::resource('withdraw', WithdrawController::class);
        Route::prefix('auth')->name('auth.')->group(function(){
            Route::post('login',[AuthController::class, 'do_login'])->name('login');
            Route::post('register',[AuthController::class, 'do_register'])->name('register');
            Route::post('forgot',[AuthController::class, 'do_forgot'])->name('forgot');
            Route::post('check',[AuthController::class, 'check_phone'])->name('check');
            Route::get('password_changed',[AuthController::class, 'password_changed'])->name('password_changed');
            Route::get('reset/{token}',[AuthController::class, 'reset'])->name('getreset');
            Route::post('reset',[AuthController::class, 'do_reset'])->name('reset');
            Route::get('verify/{auth:email}',[AuthController::class, 'do_verify'])->name('verify');
            Route::get('deactivated', [AuthController::class, 'deactivated'])->name('deactivated');
        });
        Route::middleware(['auth:member'])->group(function(){
            Route::get('profile/{user:username}/show', [ProfileController::class, 'index'])->name('profile');
            Route::get('profile/edit', [ProfileController::class, 'edit'])->name('profile.edit');
            Route::post('save', [ProfileController::class, 'profilesave'])->name('profilesave');
            Route::post('cpassword', [ProfileController::class, 'cpassword'])->name('cpassword');
            Route::get('notice',[AuthController::class, 'verification'])->name('verification.notice');
            Route::get('resend',[AuthController::class, 'resend_mail'])->name('auth.resend.mail');
            Route::get('logout',[AuthController::class, 'do_logout'])->name('auth.logout');
            
            Route::get('cart',[CartController::class, 'index'])->name('cart');

            Route::get('account', [AccountController::class, 'index'])->name('account');
            Route::get('my_projects/listing', [ProjectController::class, 'my_projects'])->name('my_projects.listing');
            Route::get('my_bids/listing', [ProjectController::class, 'my_bids'])->name('my_bids.listing');
            Route::get('my_products/listing', [ProjectController::class, 'my_products'])->name('my_products.listing');
            Route::get('my_purchases', [ProjectController::class, 'my_purchases'])->name('my_purchases');
            Route::get('my_sales', [ProjectController::class, 'my_sales'])->name('my_sales');
            Route::get('my_finance', [ProjectController::class, 'my_finance'])->name('my_finance');
            Route::get('my_points', [ProjectController::class, 'my_points'])->name('my_points');
            Route::get('my_referals', [ProjectController::class, 'my_referals'])->name('my_referals');

            Route::post('project/create', [ProjectController::class, 'store'])->name('project.create');
            Route::post('project/create_bid', [ProjectController::class, 'create_bid'])->name('project.create_bid');

            Route::post('product/create', [ProductController::class, 'store'])->name('product.create');
            Route::post('product/add_cart', [ProductController::class, 'add_cart'])->name('product.add_cart');

            Route::post('portofolio/create', [PortofolioController::class, 'store'])->name('portofolio.create');
            Route::post('skills/create', [ProfileController::class, 'addSkills'])->name('skills.create');
            

            Route::get('province', [RegionalController::class, 'province'])->name('regional.province');
            Route::get('city', [RegionalController::class, 'city'])->name('regional.city');
            Route::get('subdistrict', [RegionalController::class, 'subdistrict'])->name('regional.subdistrict');
            Route::get('village', [RegionalController::class, 'village'])->name('regional.village');
            Route::post('city/get', [RegionalController::class, 'get_city'])->name('regional.get_city');
            Route::post('subdistrict/get', [RegionalController::class, 'get_subdistrict'])->name('regional.get_subdistrict');
            Route::post('village/get', [RegionalController::class, 'get_village'])->name('regional.get_village');
        });
    });
});