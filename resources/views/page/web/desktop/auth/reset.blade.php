<x-web-layout title="Forget Password" keyword="Warung Kerja">
    <!-- Content ============================================= -->
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="tabs mx-auto mb-0 clearfix" id="tab-login-register" style="max-width: 500px;">
                    <div id="register_page">
                        <div class="card mb-0 mt-3">
                            <div class="card-body" style="padding: 40px;">
                                <h4>Atur Ulang Kata Sandi</h4>
                                <form id="forget_form" class="row mb-0">
                                    <div class="col-12 form-group">
                                        <input type="hidden" name="token" value="{{$token}}" class="form-control" />
                                    </div>
                                    <div class="col-12 form-group">
                                        <label for="register-form-password">Kata Sandi Baru:</label>
                                        <input type="password" id="forget-form-password" name="password" value="" class="form-control" />
                                    </div>
                                    <div class="col-12 form-group">
                                        <label for="register-form-password">Konfirmasi Kata Sandi Baru:</label>
                                        <input type="password" id="forget-form-password" name="password_confirmation" value="" class="form-control" />
                                    </div>
                                    <div class="col-12 form-group">
                                        <button type="button" class="button button-3d button-black m-0" onclick="do_auth('#forget_form_button','#forget_form','{{route('web.auth.reset')}}','POST');" id="forget_form_button" name="forget_form_button" value="forget">Ubah Sandi</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</x-web-layout>