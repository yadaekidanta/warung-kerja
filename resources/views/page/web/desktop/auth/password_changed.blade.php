<x-web-layout title="Change Password" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap">
            <div class="container center clearfix">
                <div class="heading-block center">
                    <h1>Kata sandi telah berubah</h1>
                    <span>Kata sandi Anda berhasil diubah. Silahkan Masuk
                        <br/> masuk ke akun Anda dan mulai proyek baru.</span>
                </div>
            </div>
        </div>
    </section>
</x-web-layout>