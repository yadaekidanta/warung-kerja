<x-web-layout title="Masuk / Buat Akun" keyword="Warung Kerja">
    <!-- Content ============================================= -->
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="tabs mx-auto mb-0 clearfix" id="tab-login-register" style="max-width: 500px;">
                    <ul class="tab-nav tab-nav2 center clearfix">
                        <li class="inline-block"><a class="cursor_pointer" onclick="auth_content('login_page');">Masuk</a></li>
                        <li class="inline-block"><a class="cursor_pointer" onclick="auth_content('register_page');">Buat Akun</a></li>
                    </ul>
                    <div id="login_page">
                        @isset($register)
                            <input id="state_register" type="hidden" value="{{$register}}">
                        @endisset
                        <div class="card mb-0 mt-3">
                            <div class="card-body" style="padding: 40px;">
                                <form id="login_form" class="mb-0">
                                    <h3> Masuk ke akun Anda</h3>
                                    <div class="row">
                                        <div class="col-12 form-group">
                                            <label for="login-form-username">Email:</label>
                                            <input type="text" id="login-form-username" name="email" value="" class="form-control" />
                                        </div>
                                        <div class="col-12 form-group">
                                            <label for="login-form-password">Kata Sandi:</label>
                                            <input type="password" id="login-form-password" name="password" value="" class="form-control" />
                                        </div>
                                        <div class="col-12 form-group">
                                            <button type="button" class="button button-3d button-black m-0" onclick="do_auth('#login_form_button','#login_form','{{route('web.auth.login')}}','POST');" id="tombol_login_email" name="login_form_button" value="login">
                                            <span>Masuk</span>
                                            <!-- <i class="icon-reload"></i> -->
                                            <div id="spinner-loading" class="spinner-border spinner-border-sm d-none" role="status"></div>
                                            </button>
                                            <a onclick="auth_content('forgot_page');" class="cursor_pointer float-end">Lupa Kata Sandi?</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="register_page">
                        <div class="card mb-0 mt-3">
                            <div class="card-body" style="padding: 40px;">
                                <h4>Daftarkan diri Anda di Warung Kerja sekarang</h4>
                                <form id="register_form" class="row mb-0">
                                    <div class="col-12 form-group">
                                        <label for="register-form-name">Nama:</label>
                                        <input type="text" id="register-form-name" name="name" value="" class="form-control" />
                                    </div>
                                    <div class="col-12 form-group">
                                        <label for="register-form-email">Email:</label>
                                        <input type="text" id="register-form-email" name="email" value="" class="form-control" />
                                    </div>
                                    <div class="col-12 form-group">
                                        <label for="register-form-phone">No HP:</label>
                                        <input type="text" id="register-form-phone" name="phone" value="" class="form-control" />
                                    </div>
                                    <div class="col-12 form-group">
                                        <label for="register-form-password">Kata Sandi:</label>
                                        <input type="password" id="register-form-password" name="password" value="" class="form-control" />
                                    </div>
                                    <div class="col-12 form-group">
                                        <button type="button" class="button button-3d button-black m-0" onclick="do_auth('#register_form_button','#register_form','{{route('web.auth.register')}}','POST');" id="register_form_button" name="register_form_button" value="register">
                                            <span>Buat Akun</span>
                                            <div id="spinner-loading" class="spinner-border spinner-border-sm d-none" role="status"></div>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="forgot_page">
                        <div class="card mb-0 mt-3">
                            <div class="card-body" style="padding: 40px;">
                                <h4>Masukkan email Anda untuk mengatur ulang kata sandi Anda.</h4>
                                <form id="forgot_form" class="row mb-0">
                                    <div class="col-12 form-group">
                                        <label for="forgot_email">Email:</label>
                                        <input type="text" name="email" class="form-control" />
                                    </div>
                                    <div class="col-12 form-group">
                                        <button type="button" class="button button-3d button-black m-0" onclick="do_auth('#forgot_form_button','#forgot_form','{{route('web.auth.forgot')}}','POST');" id="forgot_form_button">Kirim Permintaan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- #content end -->
    @section('custom_js')
        <script type="text/javascript">
            if ($("#state_register").val()) {
                auth_content("register_page");
            }else{
                auth_content("login_page");
            }
        </script>
    @endsection
</x-web-layout>