<x-web-layout title="Detail Proyek" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap" style="overflow: visible;margin-bottom:-10%;">
            <!-- Wave Shape Divider ============================================= -->
            <!-- Section Courses ============================================= -->
            <div class="section parallax" style="margin-top:-6%;padding: 80px 0 60px; background-image: url('{{asset('img/icon-pattern.jpg')}}'); background-size: auto; background-repeat: repeat"  data-bottom-top="background-position:0px 100px;" data-top-bottom="background-position:0px -500px;">
                <!-- Wave Shape Divider ============================================= -->
                <div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
                
                <div class="container">
                    <div class="mb-3 p-3" style="margin-top: -30px;">
                        <span>
                            <i class="icon-home2 hover-efect" data-bs-toggle="modal" data-bs-target="#modalBidProject"></i>
                            <a href="{{route('web.home')}}">Home</a>
                            <span style="font-size: 12px;">></span>
                            <a href="{{route('web.project')}}">Browse Project</a>
                            <span style="font-size: 12px;">></span>
                            <span>Detail Product</span>
                        </span>
                    </div>

                    <div class="row">
                        <div class="col-lg-3">
                            <div class="card p-2">
                                <div class="entry-image">
                                    <a href="{{asset('img/courses/5.jpg')}}" data-lightbox="image">
                                        <img src="{{asset('img/courses/5.jpg')}}" alt="Standard Post with Image">
                                    </a>
                                </div>

                                <button class="button button-rounded button-small mt-2" style="width: 95%;">
                                    <span>Beli</span>
                                </button>
                                
                                <form class="m-0" id="formAddToCart">
                                    @csrf
                                    <input id="id_product" name="id_product" type="hidden" value="1">
                                    <button type="submit" class="button button-rounded button-small mt-2" style="width: 95%; background-color: rgb(76,154,199, .15); border: 1px solid rgb(76,154,199);">
                                        <span style="color: rgb(76,154,199); text-shadow: none;">Tambah ke Keranjang</span>
                                        <i class="icon-line-shopping-cart" style="color: rgb(76,154,199); text-shadow: none;"></i>
                                    </button>
                                </form>


                            </div>

                            <div class="card p-2 mt-2">
                                <!-- <div class="row justify-content-center align-items-center"> -->
                                    <div class="d-flex align-items-center">
                                        <a href="javascript:void(0);" class="nobg">
                                            <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                    style="width: 43px;height:43px; object-fit: cover;">
                                        </a>
                                        <div class="ms-3 mt-2">
                                            <a href="javascript:void(0);">
                                                misterrizky
                                            </a>
                                            <div class="rating-stars mb-2"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                                        </div>
                                    </div>

                                    <button class="button button-rounded button-small mt-3">
                                        <span>Ask Owner</span>
                                        <i class="icon-rocketchat hover-efect" data-bs-toggle="modal" data-bs-target="#modalBidProject"></i>
                                    </button>
                                    <button class="button button-rounded button-small mt-1">
                                        <span>Chat</span>
                                        <i class="icon-chat hover-efect" data-bs-toggle="modal" data-bs-target="#modalBidProject"></i>
                                    </button>
                                <!-- </div> -->
                            </div>
                        </div>

                        <div class="col-lg-9">
                            <div class="card p-3">
                                <div class="ps-3">
                                    <h2>JUAL TEMPLATE EXCEL UNTUK PENCATATAN TRANSAKSI KEUANGAN SEDERHANA PERUSAHAAN ANDA</h2>
                                </div>
                                <div class="card-body">
                                    <div class="head-owner row justify-content-end">
                                        <span class="float-end">20 Agustus 2021</span>
                                    </div>
        
                                    <p class="mb-2 mt-4 card-title-sub text-uppercase fw-normal ls1">
                                        <span class="badge bg-success text-light-40 mt-2">Codeigniter</span>
                                        <span class="badge bg-success text-light-40 mt-2">PHP</span>
                                        <span class="badge bg-success text-light-40 mt-2">Laravel</span>
                                    </p>
        
                                    <hr class="col">
                                    
                                    <p class="card-text text-black-50 mb-1">
                                        Saya membutuhkan developer / team developer untuk membuat web & mobile app dari aplikasi yang sudah ada dengan beberapa penyesuaian dan penambahan menggunakan API. - Sumber aplikasi: https://codecanyon.net/item/colibrism-the-ultimate-php-modern-social-media-sharing-platform/26612898 - Dokumentasi API: https://www.colibri-sm.ru/api_docs/ Ada pun penambahan yang ingin dimasukkan ke dalam aplikasi adalah API dari: https://www.coingecko.com/id/api Aplikasi yang ingin dibuat kurang lebih seperti Stockbit...
                                    </p>        
                                    
                                    <h3 class="m-0 mb-1 mt-5">Screenshots</h3>
        
                                    <hr class="col">
        
                                    <div class="row align-items-center">
                                        
                                        <div class="col-3">
                                            <div class="portfolio-image">
                                                <div class="grid-inner">
                                                    <div class="border border-secondary overflow-hidden" style="height: 115px; width: 135; padding: 1px;">
                                                        <a href="portfolio-single.html">
                                                            <img src="{{asset('img/courses/1.jpg')}}" alt="Open Imagination">
                                                        </a>
                                                    </div>
                                                    <div class="bg-overlay">
                                                        <div class="bg-overlay-content dark" data-hover-animate="fadeIn">
                                                            <a href="{{asset('img/courses/1.jpg')}}" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350" data-lightbox="image" title="Image"><i class="icon-eye"></i></a>
                                                        </div>
                                                        <div class="bg-overlay-bg dark" data-hover-animate="fadeIn"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-3">
                                            <div class="portfolio-image">
                                                <div class="grid-inner">
                                                    <div class="border border-secondary overflow-hidden" style="height: 115px; width: 135; padding: 1px;">
                                                        <a href="portfolio-single.html">
                                                            <img src="{{asset('img/courses/2.jpg')}}" alt="Open Imagination">
                                                        </a>
                                                    </div>
                                                    <div class="bg-overlay">
                                                        <div class="bg-overlay-content dark" data-hover-animate="fadeIn">
                                                            <a href="{{asset('img/courses/2.jpg')}}" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350" data-lightbox="image" title="Image"><i class="icon-eye"></i></a>
                                                        </div>
                                                        <div class="bg-overlay-bg dark" data-hover-animate="fadeIn"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        
                                    </div>

                                    <!-- riviews -->

                                    <h3 class="m-0 mb-1 mt-5">Riviews</h3>

                                    <hr class="col">

                                    <div>
                                        <div class="row">
                                            <div class="col-2">
                                                <div class="d-flex align-items-center" style="flex-direction: column;">
                                                    <a href="javascript:void(0);" class="nobg">
                                                        <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                style="width: 43px;height:43px; object-fit: cover;">
                                                    </a>
                                                    <div class="mt-2">
                                                        <a href="javascript:void(0);">
                                                            misterrizky
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
    
                                            <div class="col-10">
                                                <div class="card p-2 bg-light">
                                                    <div class="rating-stars mb-2"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i></div>


                                                    <div>Produk nya sangat bagus sekali</div>
    
                                                    <div class="row mt-3">
                                                        <div class="col-3">
                                                            <div class="portfolio-image">
                                                                <div class="grid-inner">
                                                                    <div class="border border-secondary overflow-hidden" style="height: 90px; width: 135px; padding: 1px;">
                                                                        <a href="portfolio-single.html">
                                                                            <img src="{{asset('img/courses/2.jpg')}}" alt="Open Imagination">
                                                                        </a>
                                                                    </div>
                                                                    <div class="bg-overlay">
                                                                        <div class="bg-overlay-content dark" data-hover-animate="fadeIn">
                                                                            <a href="{{asset('img/courses/2.jpg')}}" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350" data-lightbox="image" title="Image">
                                                                                <i class="icon-eye"></i>
                                                                            </a>
                                                                        </div>
                                                                        <div class="bg-overlay-bg dark" data-hover-animate="fadeIn"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
    
                                                    <div><span class="font-date mt-2">18/02/2021 09:21:34 WIB</span></div>
    
                                                </div>
                                            </div>
                                        </div>

                                        <nav aria-label="Page navigation example" class="mt-5">
                                            <ul class="pagination justify-content-end">
                                                <li class="page-item">
                                                    <a class="page-link" href="#" aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                    </a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="#" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>

                                    <!-- /riviews -->

                                    
                                    <!-- comments -->

                                    <h3 class="m-0 mb-1 mt-5">Komentar</h3>

                                    <hr class="col">
                                    
                                    <div>
                                        <div class="row">
                                            <div class="col-2">
                                                <div class="d-flex align-items-center" style="flex-direction: column;">
                                                    <a href="javascript:void(0);" class="nobg">
                                                        <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                style="width: 43px;height:43px; object-fit: cover;">
                                                    </a>
                                                    <div class="mt-2">
                                                        <a href="javascript:void(0);">
                                                            misterrizky
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
    
                                            <div class="col-10">
                                                <div class="card p-2 bg-light">
                                                    <div>Saya berminat</div>
    
                                                    <div><span class="font-date mt-2">18/02/2021 09:21:34 WIB</span></div>
    
                                                </div>
                                            </div>
                                        </div>

                                        <nav aria-label="Page navigation example" class="mt-5">
                                            <ul class="pagination justify-content-end">
                                                <li class="page-item">
                                                    <a class="page-link" href="#" aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                    </a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="#" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    
                                    <!-- /comments -->

                                    <!-- write a comment -->

                                    <h3 class="m-0 mb-1 mt-5">Tulis komentar</h3>

                                    <hr class="col">
                                    
                                    <div>
                                        <div class="row">
                                            <div class="col-2">
                                                <div class="d-flex align-items-center" style="flex-direction: column;">
                                                    <a href="javascript:void(0);" class="nobg">
                                                        <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                style="width: 43px;height:43px; object-fit: cover;">
                                                    </a>
                                                    <div class="mt-2">
                                                        <a href="javascript:void(0);">
                                                            misterrizky
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
    
                                            <div class="col-10">
                                                <form action="">
                                                    <!-- wyswg -->
                                                    <div class="mb-4">
                                                        <textarea id="comment" name="comment"></textarea>
                                                    </div>
                                                    <!-- wyswg -->
    
                                                    <button type="submit" class="button button-rounded button-small mt-1 float-end">
                                                        <span>Kirim</span>
                                                        <i class="icon-line-send hover-efect" data-bs-toggle="modal" data-bs-target="#modalBidProject"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- /write a comment -->

                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>

                <!-- Wave Shape Divider - Bottom ============================================= -->
                <div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
            </div>
        </div>                         
    </section>

    <!-- Modal bid project -->
    <div class="modal fade" id="modalBidProject" tabindex="-1" aria-labelledby="modalBidProject" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalCreateProjectLabel">PLace New Bid</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="formNewBid">
                    @csrf
                    <div class="modal-body">

                        <div class="mb-4">
                            <label for="amount" class="form-label">Amount</label>
                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                <input type="text" id="amount" name="amount" class="form-control"  required>
                            </div>
                        </div>
        
                        <!-- wyswg -->
                        <div class="mb-4">
                            <label for="message" class="form-label">Message</label>
                            <textarea id="message" name="message"></textarea>
                        </div>
                        <!-- wyswg -->

                        <div class="mb-4">
                            <label for="formFileAttachment" class="form-label">Attachment</label>
                            <input class="form-control" type="file" id="fileAttachmentNewBid" name="attachment">
                        </div>

                        <button type="submit" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Place new bid</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /end Modal bid project -->

    @section('custom_js')
    <script>
        $(document).ready(function() {
            $("#amount").keyup(function(){
                let rupiah = formatRupiah($(this).val())
                $("#amount").val(rupiah)
            });

            /* Fungsi formatRupiah */
            function formatRupiah(angka){
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

                
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
            }

            // handle form place new bid
            $("#formNewBid").submit(function(event) {
                event.preventDefault();
                var form_data = new FormData($(this)[0]);

                $.ajax({
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: false,
                    url: "{{route('web.project.create_bid')}}",
                    data: form_data,

                    success: function(response) {
                        success_toastr(response.message);
                        // console.log(response.data);
                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                        error_toastr(errorThrown);
                    }

                });
            })

            $("#formAddToCart").submit(function(e){
                e.preventDefault();
                var form_data = new FormData($(this)[0]);
                $.ajax({
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: false,
                    url: "{{route('web.product.add_cart')}}",
                    data: form_data,

                    success: function(response) {
                        success_toastr(response.message);
                        console.log(response.data);
                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                        error_toastr(errorThrown);
                    }

                });
            })
        });
    </script>
    @endsection
    
</x-web-layout>