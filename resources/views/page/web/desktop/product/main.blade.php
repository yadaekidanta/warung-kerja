<x-web-layout title="List Produk" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap" style="overflow: visible;margin-bottom:-10%;">
            <!-- Wave Shape Divider ============================================= -->
            <!-- Section Courses ============================================= -->
            <div class="section parallax" style="margin-top:-6%;padding: 80px 0 60px; background-image: url('{{asset('img/icon-pattern.jpg')}}'); background-size: auto; background-repeat: repeat"  data-bottom-top="background-position:0px 100px;" data-top-bottom="background-position:0px -500px;">
                <!-- Wave Shape Divider ============================================= -->
                <div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
                <div class="container">
                    <div class="row">
                        <div class="sidebar col-lg-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="sidebar-widgets-wrap mb-5">
                                        <div class="widget widget-filter-links" style="margin-bottom:-12%;">
                                            <h4>Pilih Kategori</h4>
                                            <ul class="custom-filter ps-2" data-container="#product" data-active-class="active-filter">
                                                <li class="widget-filter-reset active-filter"><a href="javascript:;" data-filter="*">Hapus filter</a></li>
                                                <li><a href="javascript:;" data-filter=".wk-webdev">Web Dev</a></li>
                                            </ul>
                                        </div>
                                        <div class="widget widget-filter-links">
                                            <h4>Berdasarkan</h4>
                                            <ul class="product-sorting ps-2">
                                                <li class="widget-filter-reset active-filter"><a href="javascript:;" data-sort-by="original-order">Hapus Filter</a></li>
                                                <li><a href="javascript:;" data-sort-by="name">Nama</a></li>
                                                <li><a href="javascript:;" data-sort-by="price_lh">Harga Terendah</a></li>
                                                <li><a href="javascript:;" data-sort-by="price_hl">Harga Tertinggi</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
                        <div class="postcontent col-lg-9 order-lg-last">
                            <div class="heading-block border-bottom-0">
                                <button data-bs-toggle="modal" data-bs-target="#modalCreateProduct" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Buat Produk</button>
                                <h3>Cari Produk</h3>
                            </div>
							<div id="product" class="shop row grid-container gutter-20" data-layout="fitRows" style="margin-top:-7%;">
                                <div class="product col-lg-12 wk-webdev">
                                    <div class="card course-card hover-effect border-0">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <h4 class="card-title fw-bold mb-2 product-title">
                                                        <a href="{{route('web.product.detail', 1)}}">
                                                            JUAL TEMPLATE EXCEL UNTUK PENCATATAN TRANSAKSI KEUANGAN SEDERHANA PERUSAHAAN ANDA
                                                        </a>
                                                    </h4>
                                                    <p class="mb-2 card-title-sub text-uppercase fw-normal ls1">
                                                        <p class="mb-2 card-title-sub text-uppercase fw-normal ls1">
                                                            <span class="badge bg-danger text-light-50" style="margin-top: 3px;">Codeigniter</span>
                                                            <span class="badge bg-danger text-light-50" style="margin-top: 3px;">PHP</span>
                                                            <span class="badge bg-danger text-light-50" style="margin-top: 3px;">Laravel</span>
                                                        </p>
                                                    </p>
                                                    <div class="row align-items-center justify-content-between mb-4">
                                                        <div class="col-4 rating-stars">
                                                            <div>
                                                                <b>Rating produk</b>
                                                            </div>
                                                            <i class="icon-star3"></i>
                                                            <i class="icon-star3"></i>
                                                            <i class="icon-star3"></i>
                                                            <i class="icon-star3"></i>
                                                            <i class="icon-star-half-full"></i>
                                                            <span>4.7</span>
                                                        </div>

                                                        <div class="col-4 rating-stars">
                                                            <div>
                                                                <b>Rating penjual</b>
                                                            </div>
                                                            <i class="icon-star3"></i>
                                                            <i class="icon-star3"></i>
                                                            <i class="icon-star3"></i>
                                                            <i class="icon-star3"></i>
                                                            <i class="icon-star-half-full"></i>
                                                            <span>4.7</span>
                                                        </div>

                                                        <div class="col-4">
                                                            <div class="product-price badge alert-primary float-end">Rp 1,000,000</div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="rating-stars float-start mt-0">
                                                    </div> -->
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="entry-image">
                                                        <a href="{{asset('img/courses/5.jpg')}}" data-lightbox="image">
                                                            <img src="{{asset('img/courses/5.jpg')}}" alt="Standard Post with Image">
                                                        </a>
                                                    </div>
                                                </div>
                                                <p class="card-text text-black-50 mb-1">
                                                    Perkenalkan  nama saya Ika SR Salah seorang Freelance di Projects.co.id. Apakah anda memiliki sebuah perusahaan? Dan anda membutuhkan sebuah template EXCEL untuk mengelola administrasi perusahaan anda? Namun, anda belum menemukan template yang cocok. Nah, saya menjual template EXCEL Spreadsheet Sederhana untuk pencatatan transaksi laporan keungan perusahaan anda. Apa saja yang bisa anda dapatkan, berikut di antaranya : 1. Free Tutorial Bagi anda, yang tidak memiliki basic akuntansi...
                                                </p>
                                            </div>
                                        </div>
                                        <div class="card-footer py-3 d-flex justify-content-between align-items-center bg-white text-muted">
                                            <!-- <input id="id_product" type="text" value="{{auth()->user()}}"> -->
                                            <form class="m-0" id="formAddToCart">
                                                @csrf
                                                <input id="id_product" name="id_product" type="hidden" value="1">
                                                <button type="submit" id="btnAddToCart" data-bs-toggle="tooltip" data-bs-placement="top" title="Tambahkan ke keranjang" class="text-dark hover btn-bg-none float-end">
                                                    <i class="icon-line-shopping-cart"></i>
                                                </button>
                                            </form>
                                            <a href="#" class="text-dark position-relative">
                                                <i class="icon-line2-user"></i>
                                                <span class="author-number">1</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product col-lg-12 wk-webdev">
                                    <div class="card course-card hover-effect border-0">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <h4 class="card-title fw-bold mb-2 product-title">
                                                        <a href="javascript:;">
                                                            Excel formula kalkulator kebutuhan gizi
                                                        </a>
                                                    </h4>
                                                    <p class="mb-2 card-title-sub text-uppercase fw-normal ls1">
                                                        <p class="mb-2 card-title-sub text-uppercase fw-normal ls1">
                                                            <span class="badge bg-info text-light-50" style="margin-top: 3px;">Ms Excel</span>
                                                        </p>
                                                    </p>
                                                    <div class="rating-stars float-start mt-0"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                                                    <div class="product-price badge alert-primary float-end">Rp 75,000</div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="entry-image">
                                                        <a href="{{asset('img/courses/5.jpg')}}" data-lightbox="image">
                                                            <img src="{{asset('img/courses/5.jpg')}}" alt="Standard Post with Image">
                                                        </a>
                                                    </div>
                                                </div>
                                                <p class="card-text text-black-50 mb-1">
                                                    Menghitung kebutuhan gizi dengan mudah, tinggal memasukan data berat badan, tinggi badan, usia dan aktivitas harian - Dapat menghitung kebutuhan kalori dengan formula gold standar - Formula berbeda untuk perempuan dan laki-laki - Mendapat keterangan zat gizi makro sesuai tipe tubuh - Bisa digunakan untuk banyak klien
                                                </p>
                                            </div>
                                        </div>
                                        <div class="card-footer py-3 d-flex justify-content-between align-items-center bg-white text-muted">
                                            <a href="javascript:;" class="text-dark float-end">
                                                <i class="icon-line-shopping-cart"></i>
                                            </a>
                                            <a href="#" class="text-dark position-relative">
                                                <i class="icon-line2-user"></i>
                                                <span class="author-number">1</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Wave Shape Divider - Bottom ============================================= -->
                <div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
            </div>
        </div>
    </section>

    <!-- Modal create project -->
    <div class="modal fade" id="modalCreateProduct" tabindex="-1" aria-labelledby="modalCreateProductLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalCreateProductLabel">BUAT PRODUK</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="formCreateProduct">
                @csrf
                <div class="modal-body">
                    <div class="mb-4">
                        <label for="title" class="form-label">Title</label>
                        <input type="text" class="form-control" id="title" name="title" required>
                    </div>

                    <!-- select 2 library -->
                    <div class="mb-4">
                        <label for="requirements">Tags</label>
                        <div class="form-group">
                            <select id="skills" name="tags[]" class="form-select select-skills" data-width="100%" style="height: 100px;" multiple="multiple" required>
                                <option selected="selected">orange</option>
                                <option>white</option>
                                <option>purple</option>
                            </select>
                        </div>
                    </div>
                    <!-- /select 2 library -->
    
                    <!-- wyswg -->
                    <div class="mb-4">
                        <label for="description" class="form-label">Description</label>
                        <textarea id="description" name="description"></textarea>
                    </div>
                    <!-- wyswg -->

                    <div class="mb-4">
                        <label for="formDeliverable" class="form-label">Deliverable</label>
                        <input class="form-control" type="file" id="formDeliverable" name="deliverable">
                    </div>

                    <div class="mb-4">
                        <label for="formTrialVersion" class="form-label">Trial Version</label>
                        <input class="form-control" type="file" id="formTrialVersion" name="trial_version">
                    </div>


                    <div class="mb-4">
                        <label for="price" class="form-label">Price</label>
                        <div class="input-group">
                            <span class="input-group-text">Rp</span>
                            <input type="text" id="price" name="price" class="form-control"  required>
                        </div>
                    </div>
                    <button type="submit" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Simpan dan publish</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- /End modal create project-->



    
    @section('custom_js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
		jQuery(document).ready( function($){
			$(window).on( 'pluginIsotopeReady', function(){
				$('#product').isotope({
					transitionDuration: '0.65s',
					getSortData: {
						name: '.product-title',
						price_lh: function( itemElem ) {
							if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
								var price = $(itemElem).find('.product-price ins').text();
							} else {
								var price = $(itemElem).find('.product-price').text();
							}

							priceNum = price.split("Rp ");

							return parseFloat( priceNum[1].replaceAll(",", ""));
						},
						price_hl: function( itemElem ) {
							if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
								var price = $(itemElem).find('.product-price ins').text();
							} else {
								var price = $(itemElem).find('.product-price').text();
							}

							priceNum = price.split("Rp ");
							return parseFloat( priceNum[1].replaceAll(",", ""));
						}
					},
					sortAscending: {
						name: true,
						price_lh: true,
						price_hl: false
					}
				});

				$('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
					var element = $(this),
						elementFilter = element.children('a').attr('data-filter'),
						elementFilterContainer = element.parents('.custom-filter').attr('data-container');

					elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

					element.append('<span>'+ elementFilterCount +'</span>');

				});

				$('.product-sorting li').click( function() {
					$('.product-sorting').find('li').removeClass( 'active-filter' );
					$(this).addClass( 'active-filter' );
					var sortByValue = $(this).find('a').attr('data-sort-by');
					$('#product').isotope({ sortBy: sortByValue });
					return false;
				});


                $(".select-skills").select2({
                    placeholder: 'Choose tags',
                    tags: true,
                    tokenSeparators: [',', ' '],
                    dropdownParent: $('#modalCreateProduct')
                });

                $("#price").keyup(function(){
                    let rupiah = formatRupiah($(this).val())
                    $("#price").val(rupiah)
                });

                /* Fungsi formatRupiah */
                function formatRupiah(angka){
                    // console.log(typeof angka);
                    var number_string = angka.replace(/[^,\d]/g, '').toString(),
                    split   		= number_string.split(','),
                    sisa     		= split[0].length % 3,
                    rupiah     		= split[0].substr(0, sisa),
                    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
        
                    // tambahkan titik jika yang di input sudah menjadi angka ribuan
                    if(ribuan){
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }

                    
                    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                    return rupiah;
                }

                $("#formAddToCart").submit(function(e){
                    e.preventDefault();
                    var form_data = new FormData($(this)[0]);
                    $.ajax({
                        type: 'post',
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: false,
                        url: "{{route('web.product.add_cart')}}",
                        data: form_data,

                        success: function(response) {
                            success_toastr(response.message);
                            console.log(response.data);
                        },

                        error: function(jqXHR, textStatus, errorThrown) {
                            error_toastr(errorThrown);
                        }

                    });
                })

                // handle form create projects
                $("#formCreateProduct").submit(function(event) {
                    event.preventDefault();

                    var form_data = new FormData($(this)[0]);
                    
                    $.ajax({
                        type: 'post',
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: false,
                        url: "{{route('web.product.create')}}",
                        data: form_data,

                        success: function(response) {
                            success_toastr(response.message);
                            // console.log(response.data);
                        },

                        error: function(jqXHR, textStatus, errorThrown) {
                            error_toastr(errorThrown);
                        }

                    });
                })
			});
		});
	</script>
    @endsection
</x-web-layout>