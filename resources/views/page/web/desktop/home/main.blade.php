<x-web-layout title="Selamat Datang" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap" style="overflow: visible; margin-bottom:-7%;">
            <!-- Wave Shape Divider ============================================= -->
            <div class="wave-bottom" style="position: absolute; top: -12px; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
            <div class="container">
                <div class="heading-block border-bottom-0 my-4 center">
                    <h3>Proyek Baru</h3>
                    {{-- <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla natus mollitia ipsum. Voluptatibus, perspiciatis placeat.</span> --}}
                </div>
                <!-- Categories ============================================= -->
                <div class="row course-categories clearfix mb-4">
                    <div class="col-lg-2 col-sm-3 col-6 mt-4">
                        <div class="card hover-effect">
                            <img class="card-img" src="{{asset('img/categories/music.jpg')}}" alt="Card image">
                            <a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(251,51,100,0.8);">
                                <span><i class="icon-music1"></i>Music</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-3 col-6 mt-4">
                        <div class="card hover-effect">
                            <img class="card-img" src="{{asset('img/categories/development.jpg')}}" alt="Card image">
                            <a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(29,74,103,0.8);">
                                <span><i class="icon-code1"></i>Development</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-3 col-6 mt-4">
                        <div class="card hover-effect">
                            <img class="card-img" src="{{asset('img/categories/teacher.jpg')}}" alt="Card image">
                            <a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(50,71,66,0.8);">
                                <span><i class="icon-line2-user"></i>Teacher</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-3 col-6 mt-4">
                        <div class="card hover-effect">
                            <img class="card-img" src="{{asset('img/categories/food.jpg')}}" alt="Card image">
                            <a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(70,58,69,0.8);">
                                <span><i class="icon-food"></i>Food</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-3 col-6 mt-4">
                        <div class="card hover-effect">
                            <img class="card-img" src="{{asset('img/categories/business.jpg')}}" alt="Card image">
                            <a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(252,108,52,0.8);">
                                <span><i class="icon-chart-bar1"></i>Business</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-3 col-6 mt-4">
                        <div class="card hover-effect">
                            <img class="card-img" src="{{asset('img/categories/health.jpg')}}" alt="Card image">
                            <a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(158,108,87,0.8);">
                                <span><i class="icon-heartbeat"></i>Health Fitness</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-3 col-6 mt-4">
                        <div class="card hover-effect">
                            <img class="card-img" src="{{asset('img/categories/lifestyle.jpg')}}" alt="Card image">
                            <a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(164,108,119,0.85);">
                                <span><i class="icon-line2-game-controller"></i>Lifestyle</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-3 col-6 mt-4">
                        <div class="card hover-effect">
                            <img class="card-img" src="{{asset('img/categories/language.jpg')}}" alt="Card image">
                            <a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(80,167,159,0.8);">
                                <span><i class="icon-line2-globe"></i>Language</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-3 col-6 mt-4">
                        <div class="card hover-effect">
                            <img class="card-img" src="{{asset('img/categories/paint.jpg')}}" alt="Card image">
                            <a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(23,116,234,0.8);">
                                <span><i class="icon-paint-brush"></i>Paint</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-3 col-6 mt-4">
                        <div class="card hover-effect">
                            <img class="card-img" src="{{asset('img/categories/photography.jpg')}}" alt="Card image">
                            <a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(108,156,148,0.85);">
                                <span><i class="icon-line2-camera"></i>Photography</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-3 col-6 mt-4">
                        <div class="card hover-effect">
                            <img class="card-img" src="{{asset('img/categories/marketing.jpg')}}" alt="Card image">
                            <a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(227,141,66,0.8);">
                                <span><i class="icon-line-share"></i>Digital Marketing</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-3 col-6 mt-4">
                        <div class="card hover-effect">
                            <img class="card-img" src="{{asset('img/categories/academics.jpg')}}" alt="Card image">
                            <a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(39,103,240,0.8);">
                                <span><i class="icon-line-book"></i>Academics</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <!-- Section Courses ============================================= -->
            <div class="section topmargin-lg parallax" style="padding: 80px 0 60px; background-image: url('{{asset('img/icon-pattern.jpg')}}'); background-size: auto; background-repeat: repeat"  data-bottom-top="background-position:0px 100px;" data-top-bottom="background-position:0px -500px;">
                <!-- Wave Shape Divider ============================================= -->
                <div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
                <div class="container">
                    <div class="heading-block border-bottom-0 mb-5 center">
                        <h3>Produk Baru</h3>
                    </div>
                    <div class="clear"></div>
                    <div class="row mt-2">
                        <!-- Course 1 ============================================= -->
                        <div class="col-md-6 col-lg-4 mb-5">
                            <div class="card course-card hover-effect border-0">
                                <a href="#"><img class="card-img-top" src="{{asset('img/courses/1.jpg')}}" alt="Card image cap"></a>
                                <div class="card-body">
                                    <h4 class="card-title fw-bold mb-2"><a href="#">Produk 1</a></h4>
                                    <p class="mb-2 card-title-sub text-uppercase fw-normal ls1"><a href="#" class="text-black-50">Desain</a></p>
                                    <div class="rating-stars mb-2"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                                    <p class="card-text text-black-50 mb-1">Beberapa contoh teks cepat untuk membangun judul kartu dan membuat sebagian besar konten kartu.</p>
                                </div>
                                <div class="card-footer py-3 d-flex justify-content-between align-items-center bg-white text-muted">
                                    <div class="badge alert-primary">Rp 100,000</div>
                                    <a href="#" class="text-dark position-relative" style="margin-left:50%;">
                                        <i class="icon-line-shopping-cart"></i>
                                    </a>
                                    <a href="#" class="text-dark position-relative">
                                        <i class="icon-line2-user"></i>
                                        <span class="author-number">1</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 mb-5">
                            <div class="card course-card hover-effect border-0">
                                <a href="#"><img class="card-img-top" src="{{asset('img/courses/2.jpg')}}" alt="Card image cap"></a>
                                <div class="card-body">
                                    <h4 class="card-title fw-bold mb-2"><a href="#">Produk 2</a></h4>
                                    <p class="mb-2 card-title-sub text-uppercase fw-normal ls1"><a href="#" class="text-black-50">Desain</a></p>
                                    <div class="rating-stars mb-2"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                                    <p class="card-text text-black-50 mb-1">Beberapa contoh teks cepat untuk membangun judul kartu dan membuat sebagian besar konten kartu.</p>
                                </div>
                                <div class="card-footer py-3 d-flex justify-content-between align-items-center bg-white text-muted">
                                    <div class="badge alert-primary">Rp 100,000</div>
                                    <a href="#" class="text-dark position-relative" style="margin-left:50%;">
                                        <i class="icon-line-shopping-cart"></i>
                                    </a>
                                    <a href="#" class="text-dark position-relative">
                                        <i class="icon-line2-user"></i>
                                        <span class="author-number">1</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 mb-5">
                            <div class="card course-card hover-effect border-0">
                                <a href="#"><img class="card-img-top" src="{{asset('img/courses/3.jpg')}}" alt="Card image cap"></a>
                                <div class="card-body">
                                    <h4 class="card-title fw-bold mb-2"><a href="#">Produk 3</a></h4>
                                    <p class="mb-2 card-title-sub text-uppercase fw-normal ls1"><a href="#" class="text-black-50">Desain</a></p>
                                    <div class="rating-stars mb-2"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                                    <p class="card-text text-black-50 mb-1">Beberapa contoh teks cepat untuk membangun judul kartu dan membuat sebagian besar konten kartu.</p>
                                </div>
                                <div class="card-footer py-3 d-flex justify-content-between align-items-center bg-white text-muted">
                                    <div class="badge alert-primary">Rp 100,000</div>
                                    <a href="#" class="text-dark position-relative" style="margin-left:50%;">
                                        <i class="icon-line-shopping-cart"></i>
                                    </a>
                                    <a href="#" class="text-dark position-relative">
                                        <i class="icon-line2-user"></i>
                                        <span class="author-number">1</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 mb-5">
                            <div class="card course-card hover-effect border-0">
                                <a href="#"><img class="card-img-top" src="{{asset('img/courses/4.jpg')}}" alt="Card image cap"></a>
                                <div class="card-body">
                                    <h4 class="card-title fw-bold mb-2"><a href="#">Produk 4</a></h4>
                                    <p class="mb-2 card-title-sub text-uppercase fw-normal ls1"><a href="#" class="text-black-50">Desain</a></p>
                                    <div class="rating-stars mb-2"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                                    <p class="card-text text-black-50 mb-1">Beberapa contoh teks cepat untuk membangun judul kartu dan membuat sebagian besar konten kartu.</p>
                                </div>
                                <div class="card-footer py-3 d-flex justify-content-between align-items-center bg-white text-muted">
                                    <div class="badge alert-primary">Rp 100,000</div>
                                    <a href="#" class="text-dark position-relative" style="margin-left:50%;">
                                        <i class="icon-line-shopping-cart"></i>
                                    </a>
                                    <a href="#" class="text-dark position-relative">
                                        <i class="icon-line2-user"></i>
                                        <span class="author-number">1</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 mb-5">
                            <div class="card course-card hover-effect border-0">
                                <a href="#"><img class="card-img-top" src="{{asset('img/courses/5.jpg')}}" alt="Card image cap"></a>
                                <div class="card-body">
                                    <h4 class="card-title fw-bold mb-2"><a href="#">Produk 5</a></h4>
                                    <p class="mb-2 card-title-sub text-uppercase fw-normal ls1"><a href="#" class="text-black-50">Desain</a></p>
                                    <div class="rating-stars mb-2"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                                    <p class="card-text text-black-50 mb-1">Beberapa contoh teks cepat untuk membangun judul kartu dan membuat sebagian besar konten kartu.</p>
                                </div>
                                <div class="card-footer py-3 d-flex justify-content-between align-items-center bg-white text-muted">
                                    <div class="badge alert-primary">Rp 100,000</div>
                                    <a href="#" class="text-dark position-relative" style="margin-left:50%;">
                                        <i class="icon-line-shopping-cart"></i>
                                    </a>
                                    <a href="#" class="text-dark position-relative">
                                        <i class="icon-line2-user"></i>
                                        <span class="author-number">1</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 mb-5">
                            <div class="card course-card hover-effect border-0">
                                <a href="#"><img class="card-img-top" src="{{asset('img/courses/6.jpg')}}" alt="Card image cap"></a>
                                <div class="card-body">
                                    <h4 class="card-title fw-bold mb-2"><a href="#">Produk 6</a></h4>
                                    <p class="mb-2 card-title-sub text-uppercase fw-normal ls1"><a href="#" class="text-black-50">Desain</a></p>
                                    <div class="rating-stars mb-2"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                                    <p class="card-text text-black-50 mb-1">Beberapa contoh teks cepat untuk membangun judul kartu dan membuat sebagian besar konten kartu.</p>
                                </div>
                                <div class="card-footer py-3 d-flex justify-content-between align-items-center bg-white text-muted">
                                    <div class="badge alert-primary">Rp 100,000</div>
                                    <a href="#" class="text-dark position-relative" style="margin-left:50%;">
                                        <i class="icon-line-shopping-cart"></i>
                                    </a>
                                    <a href="#" class="text-dark position-relative">
                                        <i class="icon-line2-user"></i>
                                        <span class="author-number">1</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Wave Shape Divider - Bottom ============================================= -->
                <div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
            </div>
            <!-- Instructors Section ============================================= -->
            <div class="section bg-transparent" style="padding: 60px 0 40px;">
                <div class="container">
                    <div class="heading-block border-bottom-0 mb-5 center">
                        <h3>Pekerja Terbaik</h3>
                    </div>
                    <div class="clear"></div>
                    <div class="row">
                        <!-- Instructors 1 ============================================= -->
                        <div class="col-xl-1-5 col-lg-4 col-sm-6 mb-4">
                            <div class="feature-box hover-effect shadow-sm fbox-center fbox-bg fbox-light fbox-lg fbox-effect">
                                <div class="fbox-icon">
                                    <i><img src="{{asset('img/instructor/1.jpg')}}" class="border-0 bg-transparent shadow-sm" style="z-index: 2;" alt="Image"></i>
                                </div>
                                <div class="fbox-content">
                                    <h3 class="mb-4 nott ls0">
                                        <a href="javascript:;" class="text-dark">
                                            Rizky R
                                        </a>
                                        <br>
                                        <small class="subtitle nott color">
                                            Peringkat
                                        </small>
                                    </h3>
                                    <p class="text-dark"><strong>2342</strong> Point</p>
                                    <p class="text-dark mt-0"><strong>23</strong> Proyek</p>
                                </div>
                            </div>
                        </div>
                        <!-- Instructors 2 ============================================= -->
                        <div class="col-xl-1-5 col-lg-4 col-sm-6 mb-4">
                            <div class="feature-box hover-effect shadow-sm fbox-center fbox-bg fbox-light fbox-lg fbox-effect">
                                <div class="fbox-icon">
                                    <i><img src="{{asset('img/instructor/2.jpg')}}" class="border-0 bg-transparent shadow-sm" style="z-index: 2;" alt="Image"></i>
                                </div>
                                <div class="fbox-content">
                                    <h3 class="mb-4 nott ls0">
                                        <a href="javascript:;" class="text-dark">
                                            Rizky R
                                        </a>
                                        <br>
                                        <small class="subtitle nott color">
                                            Peringkat
                                        </small>
                                    </h3>
                                    <p class="text-dark"><strong>7563</strong> Point</p>
                                    <p class="text-dark mt-0"><strong>29</strong> Proyek</p>
                                </div>
                            </div>
                        </div>
                        <!-- Instructors 3 ============================================= -->
                        <div class="col-xl-1-5 col-lg-4 col-sm-6 mb-4">
                            <div class="feature-box hover-effect shadow-sm fbox-center fbox-bg fbox-light fbox-lg fbox-effect">
                                <div class="fbox-icon">
                                    <i><img src="{{asset('img/instructor/3.jpg')}}" class="border-0 bg-transparent shadow-sm" style="z-index: 2;" alt="Image"></i>
                                </div>
                                <div class="fbox-content">
                                    <h3 class="mb-4 nott ls0">
                                        <a href="javascript:;" class="text-dark">
                                            Rizky R
                                        </a>
                                        <br>
                                        <small class="subtitle nott color">
                                            Peringkat
                                        </small>
                                    </h3>
                                    <p class="text-dark"><strong>1131</strong> Point</p>
                                    <p class="text-dark mt-0"><strong>11</strong> Proyek</p>
                                </div>
                            </div>
                        </div>
                        <!-- Instructors 4 ============================================= -->
                        <div class="col-xl-1-5 col-lg-4 col-sm-6 mb-4">
                            <div class="feature-box hover-effect shadow-sm fbox-center fbox-bg fbox-light fbox-lg fbox-effect">
                                <div class="fbox-icon">
                                    <i><img src="{{asset('img/instructor/4.jpg')}}" class="border-0 bg-transparent shadow-sm" style="z-index: 2;" alt="Image"></i>
                                </div>
                                <div class="fbox-content">
                                    <h3 class="mb-4 nott ls0">
                                        <a href="javascript:;" class="text-dark">
                                            Rizky R
                                        </a>
                                        <br>
                                        <small class="subtitle nott color">
                                            Peringkat
                                        </small>
                                    </h3>
                                    <p class="text-dark"><strong>1232</strong> Point</p>
                                    <p class="text-dark mt-0"><strong>12</strong> Proyek</p>
                                </div>
                            </div>
                        </div>
                        <!-- Instructors 4 ============================================= -->
                        <div class="col-xl-1-5 col-lg-4 col-sm-6 mb-4">
                            <div class="feature-box hover-effect shadow-sm fbox-center fbox-bg fbox-light fbox-lg fbox-effect">
                                <div class="fbox-icon">
                                    <i><img src="{{asset('img/instructor/4.jpg')}}" class="border-0 bg-transparent shadow-sm" style="z-index: 2;" alt="Image"></i>
                                </div>
                                <div class="fbox-content">
                                    <h3 class="mb-4 nott ls0">
                                        <a href="javascript:;" class="text-dark">
                                            Rizky R
                                        </a>
                                        <br>
                                        <small class="subtitle nott color">
                                            Peringkat
                                        </small>
                                    </h3>
                                    <p class="text-dark"><strong>1232</strong> Point</p>
                                    <p class="text-dark mt-0"><strong>12</strong> Proyek</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Featues Section ============================================= -->
            <div class="section mt-5 mb-0" style="padding: 120px 0; background-image: url('{{asset('img/icon-pattern-bg.jpg')}}'); background-size: auto; background-repeat: repeat">
                <!-- Wave Shape ============================================= -->
                <div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="row dark clearfix">
                                <!-- Feature - 1 ============================================= -->
                                <div class="col-md-6">
                                    <div class="feature-box media-box bottommargin">
                                        <div class="fbox-icon">
                                            <a href="#">
                                                <i class="icon-line2-book-open rounded-0 bg-transparent text-start"></i>
                                            </a>
                                        </div>
                                        <div class="fbox-content">
                                            <h3 class="text-white">21,000 Proyek Menunggu</h3>
                                            <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi rem, facilis nobis voluptatum est voluptatem accusamus molestiae eaque perspiciatis mollitia.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Feature - 3 ============================================= -->
                                <div class="col-md-6">
                                    <div class="feature-box media-box bottommargin">
                                        <div class="fbox-icon">
                                            <a href="#">
                                                <i class="icon-line2-user rounded-0 bg-transparent text-start"></i>
                                            </a>
                                        </div>
                                        <div class="fbox-content">
                                            <h3 class="text-white">Ahli Freelancer</h3>
                                            <p class="text-white">Quos, non, esse eligendi ab accusantium voluptatem. Maxime eligendi beatae, atque tempora ullam. Vitae delectus quia, consequuntur rerum quo.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Registration Foem ============================================= -->
                        <div class="col-lg-4">
                            <div class="card shadow" data-animate="shake" style="opacity: 1 !important">
                                <div class="card-body">
                                    <div class="badge registration-badge shadow-sm alert-primary">GRATIS</div>
                                    <h4 class="card-title ls-1 mt-4 fw-bold h5">Dapatkan Akun Gratis Anda!</h4>
                                    <h6 class="card-subtitle mb-4 fw-normal text-uppercase ls2 text-black-50">Pendaftaran Gratis disini.</h6>
                                    <div class="form-widget">
                                        <div class="form-result"></div>
                                        <form class="row mb-0" id="template-contactform" name="template-contactform" action="include/form.php" method="post">
                                            <div class="form-process">
                                                <div class="css3-spinner">
                                                    <div class="css3-spinner-scaler"></div>
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <button class="button button-rounded w-100 button-large bg-color text-white nott ls0 mx-0" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Daftar Sekarang</button>
                                                <br>
                                                <small style="display: block; font-size: 12px; margin-top: 15px; color: #AAA;"><em>Kami akan melakukan yang terbaik untuk menghubungi Anda</em></small>
                                            </div>
                                            <div class="col-12 form-group d-none">
                                                <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
                                            </div>
                                            <input type="hidden" name="prefix" value="template-contactform-">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Wave Shape ============================================= -->
                <div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
            </div>
            <!-- Promo Section ============================================= -->
            <div class="section m-0" style="padding: 120px 0; background: #FFF url('{{asset('img/instructor.jpg')}}') no-repeat left top / cover;">
                <div class="container" style="">
                    <div class="row">
                        <div class="col-md-7"></div>
                        <div class="col-md-5">
                            <div class="heading-block border-bottom-0 mb-4 mt-5">
                                <h3>Menjadi Pekerja!</h3>
                                <span>Lakukan apa yang kamu sukai.</span>
                            </div>
                            <p class="mb-2">Monotonectally conceptualize covalent strategic theme areas and cross-unit deliverables.</p>
                            <p>Consectetur adipisicing elit. Voluptate incidunt dolorum perferendis accusamus nesciunt et est consequuntur placeat, dolor quia.</p>
                            <a href="#" class="button button-rounded button-xlarge ls0 ls0 nott fw-normal m-0">Mulai Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="promo promo-light promo-full p-5 footer-stick d-none" style="padding: 60px 0 !important;">
                <div class="container clearfix">
                    <div class="row align-items-center">
                        <div class="col-12 col-lg">
                            <h3 class="ls-1">Call us today at <span>+1.22.57412541</span> or Email us at <span>support@canvas.com</span></h3>
                            <span class="text-black-50">We strive to provide Our Customers with Top Notch Support to make their Theme Experience Wonderful.</span>
                        </div>
                        <div class="col-12 col-lg-auto mt-4 mt-lg-0">
                            <a href="#" class="button button-xlarge button-rounded nott ls0 fw-normal m-0">Start Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-web-layout>