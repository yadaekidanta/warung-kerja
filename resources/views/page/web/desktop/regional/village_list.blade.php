<table class="table table-striped gy-7 gs-7">
    <thead>
        <tr class="fw-bold fs-6 text-gray-800 border-bottom-2 border-gray-200">
            <th class="min-w-200px">Name</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($collection as $item)
        <tr>
            <td>
                <div class="d-flex align-items-center">
                    <div class="d-flex justify-content-start flex-column">
                        <a href="javascript:;" class="text-dark fw-bolder text-hover-primary fs-6">{{$item->name_v}}</a>
                        <span class="text-muted fw-bold text-muted d-block fs-7">
                            {{$item->name_sd}}, {{$item->name_ct}} ,{{$item->name_prov}}
                        </span>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.office.pagination')}}