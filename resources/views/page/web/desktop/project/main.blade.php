<x-web-layout title="List Proyek" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap" style="overflow: visible;margin-bottom:-10%;">
            <!-- Wave Shape Divider ============================================= -->
            <!-- Section Courses ============================================= -->
            <div class="section parallax" style="margin-top:-6%;padding: 80px 0 60px; background-image: url('{{asset('img/icon-pattern.jpg')}}'); background-size: auto; background-repeat: repeat"  data-bottom-top="background-position:0px 100px;" data-top-bottom="background-position:0px -500px;">
                <!-- Wave Shape Divider ============================================= -->
                <div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
                <div class="container">
                    <div class="mb-3 p-3" style="margin-top: -30px;">
                        <span>
                            <i class="icon-home2 hover-efect" data-bs-toggle="modal" data-bs-target="#modalBidProject"></i>
                            <a href="{{route('web.home')}}">Home</a>
                            <span style="font-size: 12px;">></span>
                            <span>Browse Project</span>
                        </span>
                    </div>

                    <div class="row">
                        <div class="sidebar col-lg-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="sidebar-widgets-wrap mb-5">
                                        <div class="widget widget-filter-links" style="margin-bottom:-12%;">
                                            <h4>Pilih Kategori</h4>
                                            <ul class="custom-filter ps-2" data-container="#project" data-active-class="active-filter">
                                                <li class="widget-filter-reset active-filter"><a href="javascript:;" data-filter="*">Hapus filter</a></li>
                                                <li><a href="javascript:;" data-filter=".wk-webdev">Web Dev</a></li>
                                            </ul>
                                        </div>
                                        <div class="widget widget-filter-links">
                                            <h4>Berdasarkan</h4>
                                            <ul class="project-sorting ps-2">
                                                <li class="widget-filter-reset active-filter"><a href="javascript:;" data-sort-by="original-order">Hapus Filter</a></li>
                                                <li><a href="javascript:;" data-sort-by="name">Nama</a></li>
                                                <li><a href="javascript:;" data-sort-by="price_lh">Harga Terendah</a></li>
                                                <li><a href="javascript:;" data-sort-by="price_hl">Harga Tertinggi</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
                        <div class="postcontent col-lg-9 order-lg-last">
                            <div class="heading-block border-bottom-0">
                                <button data-bs-toggle="modal" data-bs-target="#modalCreateProject" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Buat Proyek</button>
                                <h3>Cari Proyek</h3>
                            </div>
							<div id="project" class="shop row grid-container gutter-20" data-layout="fitRows" style="margin-top:-7%;">

                                <div class="project col-lg-12 wk-webdev">
                                    <div class="card course-card hover-effect border-0">
                                        <div class="card-body" style="position: relative;">
                                            <span class="float-end">20 Agustus 2021</span>
                                            <a href="javascript:void(0);" class="nobg">
                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                     style="width: 30px;height:30px; object-fit: cover;">
                                            </a>
                                            <a href="javascript:void(0);" style="font-size:12px;">
                                                misterrizky
                                            </a>
                                            <div class="rating-stars mb-2"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                                            <h4 class="card-title fw-bold mb-2 project-title">
                                                <a href="{{route('web.project.detail', 1)}}">Membuat Web dan Mobile App dari API</a>
                                                <div class="badge alert-primary float-end">Terpublish</div>
                                            </h4>
                                            <p class="mb-2 card-title-sub text-uppercase fw-normal ls1">
                                                <span class="badge bg-success text-light-50" style="margin-top: 3px;">Codeigniter</span>
                                                <span class="badge bg-success text-light-50" style="margin-top: 3px;">PHP</span>
                                                <span class="badge bg-success text-light-50" style="margin-top: 3px;">Laravel</span>
                                            </p>
                                            <p class="card-text text-black-50 mb-1">
                                                Saya membutuhkan developer / team developer untuk membuat web & mobile app dari aplikasi yang sudah ada dengan beberapa penyesuaian dan penambahan menggunakan API. - Sumber aplikasi: https://codecanyon.net/item/colibrism-the-ultimate-php-modern-social-media-sharing-platform/26612898 - Dokumentasi API: https://www.colibri-sm.ru/api_docs/ Ada pun penambahan yang ingin dimasukkan ke dalam aplikasi adalah API dari: https://www.coingecko.com/id/api Aplikasi yang ingin dibuat kurang lebih seperti Stockbit...
                                            </p>

                                            <!-- <div class="container-popup"> -->
                                                <div class="popup-user-bid d-none">
                                                    <div class="position-relative h-100">

                                                        <!-- list user bid -->
                                                        
                                                        <div class="row row-cols-4">
                                                            
                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>
                                                            
                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>
                                                            
                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>
                                                            
                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>
                                                            
                                                            
                                                        </div>

                                                        <!-- /end list user bid -->


                                                        <!-- pagination -->
                                                        
                                                        <nav aria-label="Page navigation" class="user-bid-navigation start-50 translate-middle-x">
                                                            <ul class="pagination">
                                                                <li class="page-item">
                                                                <a class="page-link" href="#" aria-label="Previous">
                                                                    <span aria-hidden="true">&laquo;</span>
                                                                </a>
                                                                </li>
                                                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                                <li class="page-item">
                                                                <a class="page-link" href="#" aria-label="Next">
                                                                    <span aria-hidden="true">&raquo;</span>
                                                                </a>
                                                                </li>
                                                            </ul>
                                                        </nav>

                                                        <!-- /end pagination -->

                                                    </div>
                                                </div>
                                            <!-- </div> -->
                                        </div>
                                        <div class="card-footer py-3 bg-white text-muted">
                                            <div class="project-price badge alert-primary">Rp 1,000,000 - 10,000,000</div>
                                            
                                            <div class="text-dark position-relative float-end bid-user-container" data-bs-toggle="tooltip" data-bs-placement="top" title="User Bids" role="button">
                                                <i class="icon-line2-user"></i>
                                                <span class="author-number">1</span>
                                            </div>
                                            
                                            <div class="text-dark float-end me-4" data-bs-toggle="tooltip" data-bs-placement="top" title="Place new bid" role="button">
                                                <i class="icon-hand-holding-usd hover-efect" data-bs-toggle="modal" data-bs-target="#modalBidProject"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="project col-lg-12 wk-webdev">
                                    <div class="card course-card hover-effect border-0">
                                        <div class="card-body" style="position: relative;">
                                            <span class="float-end">20 Agustus 2021</span>
                                            <a href="javascript:void(0);" class="nobg">
                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                     style="width: 30px;height:30px; object-fit: cover;">
                                            </a>
                                            <a href="javascript:void(0);" style="font-size:12px;">
                                                misterrizky
                                            </a>
                                            <div class="rating-stars mb-2">
                                                <i class="icon-star3"></i>
                                                <i class="icon-star3"></i>
                                                <i class="icon-star3"></i>
                                                <i class="icon-star3"></i>
                                                <i class="icon-star-half-full"></i>
                                                <span>4.7</span></div>
                                            <h4 class="card-title fw-bold mb-2 project-title">
                                                <a href="javascript:;">Membuat Web dan Mobile App dari API</a>
                                                <div class="badge alert-primary float-end">Terpublish</div>
                                            </h4>
                                            <p class="mb-2 card-title-sub text-uppercase fw-normal ls1">
                                                <span class="badge bg-success text-light-50" style="margin-top: 3px;">Codeigniter</span>
                                                <span class="badge bg-success text-light-50" style="margin-top: 3px;">PHP</span>
                                                <span class="badge bg-success text-light-50" style="margin-top: 3px;">Laravel</span>
                                            </p>
                                            <p class="card-text text-black-50 mb-1">
                                                Saya membutuhkan developer / team developer untuk membuat web & mobile app dari aplikasi yang sudah ada dengan beberapa penyesuaian dan penambahan menggunakan API. - Sumber aplikasi: https://codecanyon.net/item/colibrism-the-ultimate-php-modern-social-media-sharing-platform/26612898 - Dokumentasi API: https://www.colibri-sm.ru/api_docs/ Ada pun penambahan yang ingin dimasukkan ke dalam aplikasi adalah API dari: https://www.coingecko.com/id/api Aplikasi yang ingin dibuat kurang lebih seperti Stockbit...
                                            </p>

                                            <!-- <div class="container-popup"> -->
                                                <div class="popup-user-bid d-none">
                                                    <div class="position-relative h-100">

                                                        <!-- list user bid -->
                                                        
                                                        <div class="row row-cols-4">
                                                            
                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>
                                                            
                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>
                                                            
                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>
                                                            
                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>

                                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="misterrizky" class="nobg mt-3 col">
                                                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                                    style="width: 50px;height:50px; object-fit: cover;">
                                                            </a>
                                                            
                                                            
                                                        </div>

                                                        <!-- /end list user bid -->


                                                        <!-- pagination -->
                                                        
                                                        <nav aria-label="Page navigation" class="user-bid-navigation start-50 translate-middle-x">
                                                            <ul class="pagination">
                                                                <li class="page-item">
                                                                <a class="page-link" href="#" aria-label="Previous">
                                                                    <span aria-hidden="true">&laquo;</span>
                                                                </a>
                                                                </li>
                                                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                                <li class="page-item">
                                                                <a class="page-link" href="#" aria-label="Next">
                                                                    <span aria-hidden="true">&raquo;</span>
                                                                </a>
                                                                </li>
                                                            </ul>
                                                        </nav>

                                                        <!-- /end pagination -->

                                                    </div>
                                                </div>
                                            <!-- </div> -->
                                        </div>
                                        <div class="card-footer py-3 bg-white text-muted">
                                            <div class="project-price badge alert-primary">Rp 1,000,000 - 10,000,000</div>
                                            
                                            <div class="text-dark position-relative float-end bid-user-container" data-bs-toggle="tooltip" data-bs-placement="top" title="User Bids" role="button">
                                                <i class="icon-line2-user"></i>
                                                <span class="author-number">1</span>
                                            </div>
                                            
                                            <div class="text-dark float-end me-4" data-bs-toggle="tooltip" data-bs-placement="top" title="Place new bid" role="button">
                                                <i class="icon-hand-holding-usd hover-efect" data-bs-toggle="modal" data-bs-target="#modalBidProject"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Wave Shape Divider - Bottom ============================================= -->
                <div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
            </div>
        </div>





    </section>

    <!-- Modal create project -->
    <div class="modal fade" id="modalCreateProject" tabindex="-1" aria-labelledby="modalCreateProjectLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalCreateProjectLabel">BUAT PROYEK</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="formCreateProjects">
                @csrf
                <div class="modal-body">
                    <div class="mb-4">
                        <label for="title" class="form-label">Title</label>
                        <input type="text" class="form-control" id="title" name="title" required>
                    </div>

                    <!-- select 2 library -->
                    <div class="mb-4">
                        <label for="requirements">Skill Requirements</label>
                        <div class="form-group">
                            <select id="skills" name="skills[]" class="form-select select-skills" data-width="100%" style="height: 100px;" multiple="multiple" required>
                                <option selected="selected">orange</option>
                                <option>white</option>
                                <option>purple</option>
                            </select>
                        </div>
                    </div>
                    <!-- /select 2 library -->
    
                    <!-- wyswg -->
                    <div class="mb-4">
                        <label for="description" class="form-label">Description</label>
                        <textarea id="description" name="description"></textarea>
                    </div>
                    <!-- wyswg -->

                    <div class="mb-4">
                        <label for="formFileAttachment" class="form-label">Attachment</label>
                        <input class="form-control" type="file" id="formFileAttachment" name="attachment">
                    </div>

                    <div class="mb-4">
                        <label for="finishDay" class="form-label">Finish Day</label>
                        <div class="input-group">
                            <input type="number" class="form-control" name="finishDay" id="finishDay" required>
                            <span class="input-group-text">Hari</span>
                        </div>
                    </div>

                    <div class="mb-4">
                        <label for="bidBudgetFrom" class="form-label">Budget Range</label>
                        <div class="input-group">
                            <span class="input-group-text">Rp</span>
                            <input type="text" id="bidBudgetFrom" name="bidBudgetFrom" class="form-control"  required>
                            <span class="input-group-text">-</span>
                            <input type="text" id="bidBudgetTo" name="bidBudgetTo" class="form-control" required>
                        </div>
                    </div>
                    <button type="submit" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Simpan dan publish</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- /End modal create project-->

    <!-- Modal bid project -->
    <div class="modal fade" id="modalBidProject" tabindex="-1" aria-labelledby="modalBidProject" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalCreateProjectLabel">PLace New Bid</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="formNewBid">
                    @csrf
                    <div class="modal-body">

                        <div class="mb-4">
                            <label for="amount" class="form-label">Amount</label>
                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                <input type="text" id="amount" name="amount" class="form-control"  required>
                            </div>
                        </div>
        
                        <!-- wyswg -->
                        <div class="mb-4">
                            <label for="message" class="form-label">Message</label>
                            <textarea id="message" name="message"></textarea>
                        </div>
                        <!-- wyswg -->

                        <div class="mb-4">
                            <label for="formFileAttachment" class="form-label">Attachment</label>
                            <input class="form-control" type="file" id="fileAttachmentNewBid" name="attachment">
                        </div>

                        <button type="submit" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Place new bid</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /end Modal bid project -->


    @section('custom_js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
		jQuery(document).ready( function($){
			$(window).on('pluginIsotopeReady', function(){
				$('#project').isotope({
					transitionDuration: '0.65s',
					getSortData: {
						name: '.project-title',
						price_lh: function( itemElem ) {
							if( $(itemElem).find('.project-price').find('ins').length > 0 ) {
								var price = $(itemElem).find('.project-price ins').text();
							} else {
								var price = $(itemElem).find('.project-price').text();
							}

							priceNum = price.split("Rp ");

							return parseFloat( priceNum[1].replaceAll(",", ""));
						},
						price_hl: function( itemElem ) {
							if( $(itemElem).find('.project-price').find('ins').length > 0 ) {
								var price = $(itemElem).find('.project-price ins').text();
							} else {
								var price = $(itemElem).find('.project-price').text();
							}

							priceNum = price.split("Rp ");

							return parseFloat( priceNum[1].replaceAll(",", ""));
						}
					},
					sortAscending: {
						name: true,
						price_lh: true,
						price_hl: false
					}
				});

                // pop up for user bid project
                $('.bid-user-container').click(function () {
                    let element = $(this).parent().parent().children(".card-body").children(".popup-user-bid")
                    element.toggleClass("d-none")
                })
                // function popUpUserBid(element){
                //     alert()
                //     console.log(element.parentElement);
                // }

				$('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
					var element = $(this),
						elementFilter = element.children('a').attr('data-filter'),
						elementFilterContainer = element.parents('.custom-filter').attr('data-container');

					elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

					element.append('<span>'+ elementFilterCount +'</span>');

				});

				$('.project-sorting li').click( function() {
					$('.project-sorting').find('li').removeClass( 'active-filter' );
					$(this).addClass( 'active-filter' );
					var sortByValue = $(this).find('a').attr('data-sort-by');
					$('#project').isotope({ sortBy: sortByValue });
					return false;
				});

                $(".select-skills").select2({
                    placeholder: 'Choose tags',
                    tags: true,
                    tokenSeparators: [',', ' '],
                    dropdownParent: $('#modalCreateProject')
                });

                $("#bidBudgetFrom").keyup(function(){
                    let rupiah = formatRupiah($(this).val())
                    $("#bidBudgetFrom").val(rupiah)
                });

                $("#bidBudgetTo").keyup(function(){
                    let rupiah = formatRupiah($(this).val())
                    $("#bidBudgetTo").val(rupiah)
                });
                
                $("#amount").keyup(function(){
                    let rupiah = formatRupiah($(this).val())
                    $("#amount").val(rupiah)
                });

                /* Fungsi formatRupiah */
                function formatRupiah(angka){
                    var number_string = angka.replace(/[^,\d]/g, '').toString(),
                    split   		= number_string.split(','),
                    sisa     		= split[0].length % 3,
                    rupiah     		= split[0].substr(0, sisa),
                    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
        
                    // tambahkan titik jika yang di input sudah menjadi angka ribuan
                    if(ribuan){
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }

                    
                    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                    return rupiah;
                }

                // handle form create projects
                $("#formCreateProjects").submit(function(event) {
                    event.preventDefault();

                    var form_data = new FormData($(this)[0]);
                    
                    $.ajax({
                        type: 'post',
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: false,
                        url: "{{route('web.project.create')}}",
                        data: form_data,

                        success: function(response) {
                            success_toastr(response.message);
                            // console.log(response.data);
                        },

                        error: function(jqXHR, textStatus, errorThrown) {
                            error_toastr(errorThrown);
                        }

                    });
                })

                // handle form place new bid
                $("#formNewBid").submit(function(event) {
                    event.preventDefault();
                    var form_data = new FormData($(this)[0]);

                    $.ajax({
                        type: 'post',
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: false,
                        url: "{{route('web.project.create_bid')}}",
                        data: form_data,

                        success: function(response) {
                            success_toastr(response.message);
                            // console.log(response.data);
                        },

                        error: function(jqXHR, textStatus, errorThrown) {
                            error_toastr(errorThrown);
                        }

                    });
                })

                
			});
		});
	</script>
    @endsection
</x-web-layout>