<x-web-layout title="Detail Proyek" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap" style="overflow: visible;margin-bottom:-10%;">
            <!-- Wave Shape Divider ============================================= -->
            <!-- Section Courses ============================================= -->
            <div class="section parallax" style="margin-top:-6%;padding: 80px 0 60px; background-image: url('{{asset('img/icon-pattern.jpg')}}'); background-size: auto; background-repeat: repeat"  data-bottom-top="background-position:0px 100px;" data-top-bottom="background-position:0px -500px;">
                <!-- Wave Shape Divider ============================================= -->
                <div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
                
                <div class="container">
                    <div class="mb-3 p-3" style="margin-top: -30px;">
                        <span>
                            <i class="icon-home2 hover-efect" data-bs-toggle="modal" data-bs-target="#modalBidProject"></i>
                            <a href="{{route('web.home')}}">Home</a>
                            <span style="font-size: 12px;">></span>
                            <a href="{{route('web.project')}}">Browse Project</a>
                            <span style="font-size: 12px;">></span>
                            <span>Detail Project</span>
                        </span>
                    </div>

                    <div class="card col-lg p-3">
                        <div class="ps-3">
                            <h2>Membuat Web dan Mobile App dari API</h2>
                        </div>
                        <div class="card-body">
                            <div class="head-owner">
                                <div class="d-flex">
                                    <a href="javascript:void(0);" class="nobg">
                                        <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"
                                                style="width: 50px;height:50px; object-fit: cover;">
                                    </a>
                                    <div class="ms-3">
                                        <a href="javascript:void(0);">
                                            misterrizky
                                        </a>
                                        <div class="rating-stars mb-2"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                                    </div>
                                </div>
                                <span class="float-end">20 Agustus 2021</span>
                            </div>

                            <p class="mb-2 mt-4 card-title-sub text-uppercase fw-normal ls1">
                                <span class="badge bg-success text-light-40 mt-2">Codeigniter</span>
                                <span class="badge bg-success text-light-40 mt-2">PHP</span>
                                <span class="badge bg-success text-light-40 mt-2">Laravel</span>
                            </p>

                            <hr class="col">
                            
                            <p class="card-text text-black-50 mb-1">
                                Saya membutuhkan developer / team developer untuk membuat web & mobile app dari aplikasi yang sudah ada dengan beberapa penyesuaian dan penambahan menggunakan API. - Sumber aplikasi: https://codecanyon.net/item/colibrism-the-ultimate-php-modern-social-media-sharing-platform/26612898 - Dokumentasi API: https://www.colibri-sm.ru/api_docs/ Ada pun penambahan yang ingin dimasukkan ke dalam aplikasi adalah API dari: https://www.coingecko.com/id/api Aplikasi yang ingin dibuat kurang lebih seperti Stockbit...
                            </p>

                            <hr class="col">

                            <div class="project-status">
                                <div class="d-flex">
                                    <div>
                                        <span>Published Budget: Rp 750,000 - 1,500,000</span>
                                        <span>Published Date: 17/04/2022 11:21:06 WIB</span>
                                        <span>Select Deadline: 17/05/2022 11:21:06 WIB</span>
                                        <span>Finish Days: 30</span>
                                    </div>

                                    <div>
                                        <span>Project Status: Rp 750,000 - 1,500,000</span>
                                        <span>Bid Count: 17/04/2022 11:21:06 WIB</span>
                                    </div>
                                </div>
                            </div>

                            <p class="mb-2 mt-4 card-title-sub text-uppercase fw-normal ls1">
                                <button data-bs-toggle="modal" data-bs-target="#modalBidProject" class="button button-rounded button-small mt-2">
                                    <span>Place New Bid</span>
                                    <i class="icon-hand-holding-usd hover-efect" data-bs-toggle="modal" data-bs-target="#modalBidProject"></i>
                                </button>
                                <button class="button button-rounded button-small mt-2">
                                    <span>Ask Owner</span>
                                    <i class="icon-rocketchat hover-efect" data-bs-toggle="modal" data-bs-target="#modalBidProject"></i>
                                </button>
                                <button class="button button-rounded button-small mt-2">
                                    <span>Chat</span>
                                    <i class="icon-chat hover-efect" data-bs-toggle="modal" data-bs-target="#modalBidProject"></i>
                                </button>
                            </p>

                            <hr class="col">

                            <h3>User Bids</h3>
                            <div class="d-flex">

                                <a href="javascript:void(0);" class="mt-3">
                                    <div class="list-user-bids-detail-projects p-2">
                                        <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"style="width: 50px;height:50px; object-fit: cover;">
                                        <a href="javascript:void(0);">
                                            misterrizky
                                        </a>
                                        <div class="rating-stars mb-2"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                                    </div>
                                </a>

                                <a href="javascript:void(0);" class="mt-3">
                                    <div class="list-user-bids-detail-projects p-2">
                                        <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"style="width: 50px;height:50px; object-fit: cover;">
                                        <a href="javascript:void(0);">
                                            misterrizky
                                        </a>
                                        <div class="rating-stars mb-2"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                                    </div>
                                </a>

                                
                            </div>
                        </div>
                    </div>
                    
                </div>

                <!-- Wave Shape Divider - Bottom ============================================= -->
                <div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
            </div>
        </div>                         
    </section>

    <!-- Modal bid project -->
    <div class="modal fade" id="modalBidProject" tabindex="-1" aria-labelledby="modalBidProject" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalCreateProjectLabel">PLace New Bid</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="formNewBid">
                    @csrf
                    <div class="modal-body">

                        <div class="mb-4">
                            <label for="amount" class="form-label">Amount</label>
                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                <input type="text" id="amount" name="amount" class="form-control"  required>
                            </div>
                        </div>
        
                        <!-- wyswg -->
                        <div class="mb-4">
                            <label for="message" class="form-label">Message</label>
                            <textarea id="message" name="message"></textarea>
                        </div>
                        <!-- wyswg -->

                        <div class="mb-4">
                            <label for="formFileAttachment" class="form-label">Attachment</label>
                            <input class="form-control" type="file" id="fileAttachmentNewBid" name="attachment">
                        </div>

                        <button type="submit" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Place new bid</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /end Modal bid project -->

    @section('custom_js')
    <script>
        $(document).ready(function() {
            $("#amount").keyup(function(){
                let rupiah = formatRupiah($(this).val())
                $("#amount").val(rupiah)
            });

            /* Fungsi formatRupiah */
            function formatRupiah(angka){
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

                
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
            }

            // handle form place new bid
            $("#formNewBid").submit(function(event) {
                event.preventDefault();
                var form_data = new FormData($(this)[0]);

                $.ajax({
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: false,
                    url: "{{route('web.project.create_bid')}}",
                    data: form_data,

                    success: function(response) {
                        success_toastr(response.message);
                        // console.log(response.data);
                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                        error_toastr(errorThrown);
                    }

                });
            })
        });
    </script>
    @endsection
    
</x-web-layout>