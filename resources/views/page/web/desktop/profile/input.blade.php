<x-web-layout title="Ubah Profil" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row clearfix">
                    <div class="col-md-9">
                        <a href="{{route('web.profile', Str::before($profile->email, '@'))}}" class="button button-rounded button-small m-0 ms-2 float-end">Kembali</a>
                        <img src="{{$profile->image}}" class="alignleft img-circle img-thumbnail my-0" alt="Avatar" style="max-width: 84px;">
                        <div class="heading-block border-0">
                            <h3>{{Auth::guard('member')->user()->name}}</h3>
                            <span>{{Auth::guard('member')->user()->bio}}</span>
                        </div>
                        <div class="clear"></div>
                        <div class="row clearfix">
                            <div class="col-lg-12">
                                <h5 class="me-3 mt-3">Informasi Dasar</h5>
                                <div class="card">
                                    <div class="card-body">
                                        <form id="form_basic_info">
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label>Nama</label>
                                                    <input type="text" name="name" class="form-control" placeholder="Full name" value="{{$profile->name}}" />
                                                </div>
                                                <div class="col-3">
                                                    <label>Tanggal Lahir</label>
                                                    <input type="date" name="date_birth" class="form-control" value="{{$profile->date_birth}}" />
                                                </div>
                                                <div class="col-3">
                                                    <label>Tempat Lahir</label>
                                                    <input type="text" name="place_birth" class="form-control" value="{{$profile->place_birth}}" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-3">
                                                    <label>Jenis Kelamin</label>
                                                    <select class="form-control" name="gender">
                                                        <option value="" selected disabled hidden>Pilih Jenis Kelamin</option>
                                                        <option value="Pria" {{$profile->gender=='Pria'?'selected':''}}>Pria</option>
                                                        <option value="Wanita" {{$profile->gender=='Wanita'?'selected':''}}>Wanita</option>
                                                    </select>
                                                </div>
                                                <div class="col-3">
                                                    <label>NIK</label>
                                                    <input type="tel" name="ktp" class="form-control" value="{{$profile->ktp}}" />
                                                </div>
                                                <div class="col-6">
                                                    <label>Pengenalan Diri</label>
                                                    <textarea name="bio" class="form-control">{{$profile->bio}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label>Alamat</label>
                                                    <textarea name="address" class="form-control">{{$profile->address}}</textarea>
                                                </div>
                                                <div class="col-6">
                                                    <label>Kode Pos</label>
                                                    <input name="postcode" type="tel" maxlength="5" class="form-control" value="{{$profile->postcode}}" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label>Provinsi</label>
                                                    <select name="province" id="province" class="form-select">
                                                        <option value="">Pilih Provinsi</option>
                                                        @foreach($province as $pr)
                                                            <option value="{{$pr->id}}"{{$pr->id==$profile->province_id?'selected':''}}>{{$pr->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-6">
                                                    <label>Kota/Kabupaten</label>
                                                    <select name="city" id="city" class="form-select">
                                                        <option value="">Pilih Kota/Kabupaten</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label>Kecamatan</label>
                                                    <select name="subdistrict" id="subdistrict" class="form-select">
                                                        <option value="">Pilih Kecamatan</option>
                                                    </select>
                                                </div>
                                                <div class="col-6">
                                                    <label>Kelurahan/Desa</label>
                                                    <select name="village" id="village" class="form-select">
                                                        <option value="">Pilih Kelurahan/Desa</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <button id="tombol_simpan_basic" onclick="handle_upload('#tombol_simpan_basic','#form_basic_info','{{route('web.profilesave')}}','POST')" class="button button-rounded button-small m-0 ms-2 float-end">Simpan Perubahan</button>
                                            {{-- <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#profile_form','{{route('web.profilesave')}}','POST')" class="btn btn-primary" >Save Changes</button> --}}
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 mt-3">
                                <h5 class="me-3 mt-3">Keamanan Akun</h5>
                                <div class="card">
                                    <div class="card-body">
                                        <form id="form_password">
                                            <div class="form-group row">
                                                <div class="col-4">
                                                    <label>Kata Sandi saat ini</label>
                                                    <input type="password" class="form-control" name="current_password" id="current_password" />
                                                </div>
                                                <div class="col-4">
                                                    <label>Kata Sandi</label>
                                                    <input type="password" class="form-control" name="new_password" id="new_password" />
                                                </div>
                                                <div class="col-4">
                                                    <label>Konfirmasi Kata Sandi</label>
                                                    <input type="password" class="form-control" name="new_confirm_password" id="new_confirm_password" />
                                                </div>
                                            </div>
                                            <button id="tombol_simpan_password" onclick="handle_save('#tombol_simpan_password','#form_password','{{route('web.cpassword')}}','POST')" class="button button-rounded button-small m-0 ms-2 float-end">Ubah Kata Sandi</button>
                                            {{-- <button id="tombol_save" onclick="handle_save('#tombol_save','#password_form','{{route('office.profile.cpassword')}}','POST')" class="btn btn-primary" >Save Changes</button> --}}
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-100 line d-block d-md-none"></div>
                    <div class="col-md-3">
                        <div class="list-group">
                            <a href="{{route('web.profile', Str::before($profile->email, '@'))}}" class="{{request()->is('profile/*') ? 'active' : ''}} list-group-item list-group-item-action d-flex justify-content-between"><div>Profil</div><i class="icon-user"></i></a>
                            <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Proyek</div><i class="icon-line2-briefcase"></i></a>
                            <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Penawaran</div><i class="icon-hand-holding-usd"></i></a>
                            <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Produk</div><i class="icon-bag"></i></a>
                            <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Pembelian</div><i class="icon-line-credit-card"></i></a>
                            <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Penjualan</div><i class="icon-chart-line"></i></a>
                            <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Rujukan</div><i class="icon-handshake1"></i></a>
                            <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Keuangan</div><i class="icon-money"></i></a>
                            <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Poin</div><i class="icon-coins"></i></a>
                            <a href="{{route('web.auth.logout')}}" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Keluar</div><i class="icon-line2-logout"></i></a>
                        </div>
                        <div class="fancy-title topmargin title-border">
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <a href="javascript:;" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Tambah </a>
                                <h5 class="mt-3">Keterampilan</h5>
                                <p class="mb-2 card-title-sub text-uppercase fw-normal ls1">
                                    <span class="badge bg-secondary text-light-50" style="margin-top: 3px;">Codeigniter</span>
                                    <span class="badge bg-secondary text-light-50" style="margin-top: 3px;">PHP</span>
                                    <span class="badge bg-secondary text-light-50" style="margin-top: 3px;">Laravel</span>
                                    <span class="badge bg-secondary text-light-50" style="margin-top: 3px;">Laravel</span>
                                    <span class="badge bg-secondary text-light-50" style="margin-top: 3px;">Laravel</span>
                                    <span class="badge bg-secondary text-light-50" style="margin-top: 3px;">Laravel</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@section('custom_js')
<script>
    @if($profile->province_id)
    $('#province').val('{{$profile->province_id}}');
    setTimeout(function(){ 
        $('#province').trigger('change');
        setTimeout(function(){ 
            $('#city').val('{{$profile->city_id}}');
            $('#city').trigger('change');
            setTimeout(function(){ 
                $('#subdistrict').val('{{$profile->subdistrict_id}}');
                $('#subdistrict').trigger('change');
                setTimeout(function(){ 
                    $('#village').val('{{$profile->village_id}}');
                }, 2000);
            }, 2000);
        }, 2000);
    }, 1000);
    @endif
    $("#province").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('web.regional.get_city')}}",
            data: {id_province : $("#province").val()},
            success: function(response){
                $("#city").html(response);
            }
        });
    });
    $("#city").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('web.regional.get_subdistrict')}}",
            data: {id_city : $("#city").val()},
            success: function(response){
                $("#subdistrict").html(response);
            }
        });
    });
    $("#subdistrict").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('web.regional.get_village')}}",
            data: {id_subdistrict : $("#subdistrict").val()},
            success: function(response){
                $("#village").html(response);
            }
        });
    });
</script>
@endsection
</x-web-layout>