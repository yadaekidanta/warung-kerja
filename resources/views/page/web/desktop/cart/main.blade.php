<x-web-layout title="Cart" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap" style="overflow: visible;margin-bottom:-10%;">
            <!-- Wave Shape Divider ============================================= -->
            <!-- Section Courses ============================================= -->
            <div class="section parallax" style="margin-top:-6%;padding: 80px 0 60px; background-image: url('{{asset('img/icon-pattern.jpg')}}'); background-size: auto; background-repeat: repeat"  data-bottom-top="background-position:0px 100px;" data-top-bottom="background-position:0px -500px;">
                <!-- Wave Shape Divider ============================================= -->
                <div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
                <div class="container">
                    <div class="card py-2 px-4 mb-4">
                        <div class="row justify-content-between">
                            <div class="col-6">
                                <span>Produk</span>
                            </div>

                            <div class="col-6">
                                <div class="row justify-content-between">
                                    <div class="col text-center p-0">Harga Satuan</div>
                                    <div class="col text-center p-0">Kuantitas</div>
                                    <div class="col text-center p-0">Total Harga</div>
                                    <div class="col text-center p-0">Aksi</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card py-4 mb-4">
                        <div class="px-4">
                            <i class="icon-store me-2"></i>
                            <span class="me-4">OsiLab</span>
                            <i class="icon-chat" role="button"></i>
                        </div>
                        <hr>
                        <div class="px-4">
                            <div class="row justify-content-between align-items-center">
                                <div class="col-6">
                                    <div class="row align-items-center">
                                        <input type="checkbox" class="col-1" style="width: 18px; height: 18px;">
                                        <div class="col-3">
                                            <div class="row mt-3">
                                                <div class="">
                                                    <div class="portfolio-image">
                                                        <div class="grid-inner">
                                                            <div class="border border-secondary overflow-hidden" style="height: 65px; width: 100%; padding: 1px;">
                                                                <a href="portfolio-single.html">
                                                                    <img src="{{asset('img/courses/2.jpg')}}" alt="Open Imagination">
                                                                </a>
                                                            </div>
                                                            <div class="bg-overlay">
                                                                <div class="bg-overlay-content dark" data-hover-animate="fadeIn">
                                                                    <a href="{{asset('img/courses/2.jpg')}}" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350" data-lightbox="image" title="Image">
                                                                        <i class="icon-eye"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="bg-overlay-bg dark" data-hover-animate="fadeIn"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-8">
                                            <span>JUAL TEMPLATE EXCEL UNTUK PENCATATAN TRANSAKSI KEUANGAN...</span>
                                        </div>
                                    </div>
                                    
                                </div>
        
                                <div class="col-6">
                                    <div class="row justify-content-between">
                                        <div class="col p-0 text-center" id="price">Rp 800.000</div>
                                        <div class="col p-0 text-center">
                                            <i class="icon-plus1 hover" role="button" id="incrementQty"></i>
                                            <span class="mx-3" id="qty">1</span>
                                            <i class="icon-minus1 hover" role="button" id="decrementQty"></i>
                                        </div>
                                        <div class="col p-0 text-center" id="totalPrice">Rp 800.000</div>
                                        <a href="#" id="deleteCart" class="col p-0 text-center" role="button">Hapus</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Wave Shape Divider - Bottom ============================================= -->
                <div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
            </div>
        </div>
    </section>

    @section('custom_js')
    <script>
        $('document').ready(function(){

            $("#incrementQty").click(function(){
                $("#qty").html(parseInt($("#qty").html()) + 1)
                let qty = parseInt($("#qty").html())
                if (qty > 0) {
                    let price = $("#price").html().split('Rp ')[1].split('.').join('')
                    let countALL = (price * qty).toString()
                    let finalResult = formatRupiah(countALL)
                    // store the qty and totalPrice to DB 
                    // handle_updatePrice(countALL, qty)
                    $("#totalPrice").html(`Rp ${finalResult}`)
                }
            })

            $("#decrementQty").click(function(){
                let qty = parseInt($("#qty").html())
                if (qty > 1) {
                    $("#qty").html(parseInt($("#qty").html()) - 1)
                    let price = $("#price").html().split('Rp ')[1].split('.').join('')
                    let totalPrice = $("#totalPrice").html().split('Rp ')[1].split('.').join('')
                    let countALL = (totalPrice - price).toString()
                    let finalResult = formatRupiah(countALL)
                    // store the qty and totalPrice to DB 
                    // handle_updatePrice(countALL)
                    $("#totalPrice").html(`Rp ${finalResult}`)
                }
            })

            // function handle update price and qty product

            /* Fungsi formatRupiah */
            function formatRupiah(angka){
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

                
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
            }
            
        })
    </script>
    @endsection

</x-web-layout>