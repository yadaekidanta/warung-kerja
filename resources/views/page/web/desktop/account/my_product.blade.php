<x-web-layout title="Daftar Produk" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap p-0">
            <!-- Wave Shape Divider ============================================= -->
            <!-- Section Courses ============================================= -->
            <div class="section parallax m-0"  data-bottom-top="background-position:0px 100px;" data-top-bottom="background-position:0px -500px;">
                <!-- Wave Shape Divider ============================================= -->
                <div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
                <div class="container">
                    <div class="row">
                        <div class="postcontent col-9">
                            <div class="heading-block border-bottom-0">
                                <button data-bs-toggle="modal" data-bs-target="#modalCreateProduct" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Buat Produk</button>
                                <h3>Daftar Produk</h3>
                            </div>
							<div id="product" class="shop row grid-container gutter-20" data-layout="fitRows" style="margin-top:-7%;">
                            
                            <div class="product col-lg-12 wk-drafts">
                                    <div class="card course-card shadow hover-effect border-0">
                                        <div class="card-body" style="position: relative;">
                                            <table class="table table-borderless" cellpadding="10">
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 20%;">
                                                            <b class="float-end">Penjual :</b>
                                                        </td>
                                                        <td>
                                                            <a href="#">masrizky</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%;">
                                                            <b class="float-end">Logo :</b>
                                                        </td>
                                                        <td>
                                                            <div class="entry-image-container">
                                                                <a href="{{asset('img/courses/1.jpg')}}" data-lightbox="image">
                                                                    <img src="{{asset('img/courses/1.jpg')}}" alt="Standard Post with Image">
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b class="float-end">Judul :</b>
                                                        </td>
                                                        <td>
                                                            <span class="project-price badge alert-primary">Template excel</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b class="float-end">Deskripsi :</b>
                                                        </td>
                                                        <td>
                                                            <span class="card p-1 bg-light">Ini merupakan template excel untuk menghitung rata-rata pendafatan yang dilengkapi berbagai fitur</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b class="float-end">Product status :</b>
                                                        </td>
                                                        <td>
                                                            <span class="project-price badge alert-primary">Sedang berjalan</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b class="float-end">Date :</b>
                                                        </td>
                                                        <td>
                                                            <span>29/04/2022 13:54:02 WIB</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b class="float-end">Harga :</b>
                                                        </td>
                                                        <td>
                                                            <span class="project-price badge alert-primary">Rp 10,000,000</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="card-footer py-3 bg-white text-muted">
                                            
                                            <button  class="button button-rounded button-small m-0 float-end ms-3" style="background-color: rgb(76,154,199, .15); border: 1px solid rgb(76,154,199);" ><span style="color: rgb(76,154,199); text-shadow: none;">Hapus Produk</span></button>

                                            <button data-bs-toggle="modal" data-bs-target="#modalCreateProduct"
                                            style="border: 1px solid rgb(76,154,199);" class="button button-rounded button-small m-0 ms-3 float-end">Edit Produk</button>

                                            <a href="{{route('web.product.detail', 1)}}" style="border: 1px solid rgb(76,154,199);" class="button button-rounded button-small m-0 float-end">Preview Produk</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-3">
                            @include('theme.web.desktop.account.sidebar')
                        </div>
                    </div>
                </div>
                <!-- Wave Shape Divider - Bottom ============================================= -->
                <div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
            </div>
        </div>
    </section>

    <!-- Modal create project -->
    <div class="modal fade" id="modalCreateProduct" tabindex="-1" aria-labelledby="modalCreateProductLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalCreateProductLabel">BUAT PRODUK</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="formCreateProduct">
                @csrf
                <div class="modal-body">
                    <div class="mb-4">
                        <label for="title" class="form-label">Title</label>
                        <input type="text" class="form-control" id="title" name="title" required>
                    </div>

                    <div class="mb-4">
                        <label for="formLogo" class="form-label">Logo</label>
                        <input class="form-control" type="file" id="formLogo" name="deliverable">
                    </div>

                    <!-- select 2 library -->
                    <div class="mb-4">
                        <label for="requirements">Tags</label>
                        <div class="form-group">
                            <select id="skills" name="tags[]" class="form-select select-skills" data-width="100%" style="height: 100px;" multiple="multiple" required>
                                <option selected="selected">orange</option>
                                <option>white</option>
                                <option>purple</option>
                            </select>
                        </div>
                    </div>
                    <!-- /select 2 library -->
    
                    <!-- wyswg -->
                    <div class="mb-4">
                        <label for="description" class="form-label">Description</label>
                        <textarea class="description" id="section001" name="description"></textarea>
                    </div>
                    <!-- wyswg -->

                    <div class="mb-4">
                        <label for="formDeliverable" class="form-label">Deliverable</label>
                        <input class="form-control" type="file" id="formDeliverable" name="deliverable" required>
                    </div>

                    <div class="mb-4">
                        <label for="formTrialVersion" class="form-label">Trial Version</label>
                        <input class="form-control" type="file" id="formTrialVersion" name="trial_version">
                    </div>


                    <div class="mb-4">
                        <label for="price" class="form-label">Price</label>
                        <div class="input-group">
                            <span class="input-group-text">Rp</span>
                            <input type="text" id="price" name="price" class="form-control"  required>
                        </div>
                    </div>

                    <div class="mb-4">
                        <label for="price" class="form-label">Tambah Screenshot</label>
                        <div class="card p-2">
                            <div class="row m-0">
                                <div class="card p-1 border rounded-2 col-2 flex justify-content-center align-items-center me-3">
                                    <label for="choice-screenshot">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="#AAAAAA" width="55" viewBox="0 0 24 24"><path d="M19.479 10.092c-.212-3.951-3.473-7.092-7.479-7.092-4.005 0-7.267 3.141-7.479 7.092-2.57.463-4.521 2.706-4.521 5.408 0 3.037 2.463 5.5 5.5 5.5h13c3.037 0 5.5-2.463 5.5-5.5 0-2.702-1.951-4.945-4.521-5.408zm-7.479-1.092l4 4h-3v4h-2v-4h-3l4-4z"/></svg>
                                    </label>
                                    <span class="mt-1 text-sm"> Max 3 screenshot</span>
                                    <input class="d-none" type="file" id="choice-screenshot" name="screenshot[]" multiple>
                                </div>

                                <div class="container-preview-screenshot col m-0 d-flex">

                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Simpan dan publish</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- /End modal create project-->
    
    @section('custom_js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
		jQuery(document).ready( function($){
			$(window).on( 'pluginIsotopeReady', function(){
				$('#product').isotope({
					transitionDuration: '0.65s',
					getSortData: {
						name: '.product-title',
						// price_lh: function( itemElem ) {
						// 	if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
						// 		var price = $(itemElem).find('.product-price ins').text();
						// 	} else {
						// 		var price = $(itemElem).find('.product-price').text();
						// 	}

						// 	priceNum = price.split("Rp ");

						// 	return parseFloat( priceNum[1].replaceAll(",", ""));
						// },
						// price_hl: function( itemElem ) {
						// 	if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
						// 		var price = $(itemElem).find('.product-price ins').text();
						// 	} else {
						// 		var price = $(itemElem).find('.product-price').text();
						// 	}

						// 	priceNum = price.split("Rp ");
						// 	return parseFloat( priceNum[1].replaceAll(",", ""));
						// }
					},
					sortAscending: {
						name: true,
						// price_lh: true,
						// price_hl: false
					}
				});

				$('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
					var element = $(this),
						elementFilter = element.children('a').attr('data-filter'),
						elementFilterContainer = element.parents('.custom-filter').attr('data-container');

					elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

					element.append('<span>'+ elementFilterCount +'</span>');

				});

				$('.product-sorting li').click( function() {
					$('.product-sorting').find('li').removeClass( 'active-filter' );
					$(this).addClass( 'active-filter' );
					var sortByValue = $(this).find('a').attr('data-sort-by');
					$('#product').isotope({ sortBy: sortByValue });
					return false;
				});


                $(".select-skills").select2({
                    placeholder: 'Choose tags',
                    tags: true,
                    tokenSeparators: [',', ' '],
                    dropdownParent: $('#modalCreateProduct')
                });

                $("#price").keyup(function(){
                    let rupiah = formatRupiah($(this).val())
                    $("#price").val(rupiah)
                });
			});

            /* Fungsi formatRupiah */
            function formatRupiah(angka){
                // console.log(typeof angka);
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

                
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
            }

            $("#formAddToCart").submit(function(e){
                e.preventDefault();
                var form_data = new FormData($(this)[0]);
                $.ajax({
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: false,
                    url: "{{route('web.product.add_cart')}}",
                    data: form_data,

                    success: function(response) {
                        success_toastr(response.message);
                        console.log(response.data);
                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                        error_toastr(errorThrown);
                    }

                });
            })

            // handle form create projects
            $("#formCreateProduct").submit(function(event) {
                event.preventDefault();

                var form_data = new FormData($(this)[0]);
                
                $.ajax({
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: false,
                    url: "{{route('web.product.create')}}",
                    data: form_data,

                    success: function(response) {
                        success_toastr(response.message);
                        // console.log(response.data);
                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                        error_toastr(errorThrown);
                    }

                });
            })

            $("input#choice-screenshot").change(function(){
                var filesAmount = this.files.length;
                if (filesAmount <= 3) {
                    container_preview_screenshot.empty()
                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
    
                        reader.onload = function(event) {
                            container_preview_screenshot.append(`
                                    <div class="card card-screenshot p-1 border me-3 rounded-2 col-3 overflow-hidden">
                                            <div class="preview-screenshot-container">
                                                    <div class="delete-screenshot" role="button">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" fill="#343738" viewBox="0 0 24 24"><path d="M23 20.168l-8.185-8.187 8.185-8.174-2.832-2.807-8.182 8.179-8.176-8.179-2.81 2.81 8.186 8.196-8.186 8.184 2.81 2.81 8.203-8.192 8.18 8.192z"/></svg>
                                                    </div>
                                                    <div class="preview-screenshot">
                                                        <img src="${event.target.result}" alt="Standard Post with Image">
                                                    </div>
                                            </div>
                                    </div>
                            `)
    
                            $(".delete-screenshot").click(function(){
                                console.log($(this).parent().parent().remove());
                            });
                            // $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(container_preview_screenshot);
                        }
    
                        reader.readAsDataURL(this.files[i]);
                    }
                } else {
                    error_toastr("You can select only 3 images");
                }

            })

            let container_preview_screenshot = $(".container-preview-screenshot")
            let list_screenshot = [] 
            
		});
	</script>
    @endsection
</x-web-layout>