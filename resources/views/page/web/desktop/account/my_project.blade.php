<x-web-layout title="Daftar Proyek" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap p-0">
            <!-- Wave Shape Divider ============================================= -->
            <!-- Section Courses ============================================= -->
            <div class="section parallax m-0"  data-bottom-top="background-position:0px 100px;" data-top-bottom="background-position:0px -500px;">
                <!-- Wave Shape Divider ============================================= -->
                <div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
                <div class="container">
                    <div class="row">
                        <div class="postcontent col-9">
                            <div class="heading-block border-bottom-0">
                                <button data-bs-toggle="modal" data-bs-target="#modalCreateProject" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Buat Proyek</button>
                                <h3>Daftar Proyek</h3>
                            </div>
							<div id="product" class="shop row grid-container gutter-20" data-layout="fitRows" style="margin-top:-7%;">
                            
                            <div class="product col-lg-12 wk-drafts">
                                    <div class="card course-card shadow hover-effect border-0">
                                        <div class="card-body" style="position: relative;">
                                            <div class="row p-2">
                                                    <div class="col-2 d-flex flex-column align-items-center">
                                                        <div class="entry-image-container-small me-2 p-1 border radius-2">
                                                            <img src="{{asset('img/avatars/5m.png')}}" alt="Standard Post with Image">
                                                        </div>
                                                        <a href="javascript:void(0);">
                                                            misterrizky
                                                        </a>
                                                        <span class="text-muted text-sm">Owner</span>
                                                        <div class="rating-stars mb-2 text-sm"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                                                        <span class="text-muted text-sm">930 Poin</span>
                                                        <span class="text-muted text-sm">930 Peringkat</span>
                                                    
                                                    </div>
                                                    <div class="col ms-1">
                                                        <h4 class="card-title fw-bold mb-2 product-title">
                                                            <a href="{{route('web.product.detail', 1)}}">
                                                                JUAL TEMPLATE EXCEL UNTUK PENCATATAN TRANSAKSI KEUANGAN SEDERHANA PERUSAHAAN ANDA
                                                            </a>
                                                        </h4>
                                                        <p class="mb-2 card-title-sub text-uppercase fw-normal ls1">
                                                            <p class="mb-2 card-title-sub text-uppercase fw-normal ls1">
                                                                <span class="badge bg-danger text-light-50" style="margin-top: 3px;">Codeigniter</span>
                                                                <span class="badge bg-danger text-light-50" style="margin-top: 3px;">PHP</span>
                                                                <span class="badge bg-danger text-light-50" style="margin-top: 3px;">Laravel</span>
                                                            </p>
                                                        </p>
                                                        
                                                        <div class="row p-1 bg-light rounded">
                                                            <div class="col-6">
                                                                <ul class="list-group list-group-flush">
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Published Budget:</b> <span>Rp 150,000 - 200,000</span>
                                                                    </li>
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Published Date:</b> <span></span>
                                                                    </li>
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Select Deadline:</b> <span>Rp 150,000 - 200,000</span>
                                                                    </li>
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Finish Days:</b> <span>30</span>
                                                                    </li>
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Start Date:</b> <span></span>
                                                                    </li>
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Finish Deadline:</b> <span></span>
                                                                    </li>
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Finish Date:</b> <span></span>
                                                                    </li>
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Closed Date:</b> <span></span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-6">
                                                                <ul class="list-group list-group-flush">
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Project Status:</b> <span class="p-1 rounded alert-primary">Registered</span>
                                                                    </li>
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Project Ending:</b> <span></span>
                                                                    </li>
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Bid Count:</b> <span>0</span>
                                                                    </li>
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Need Weekly Report:</b> <span>No</span>
                                                                    </li>
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Accepted Worker:</b> <span></span>
                                                                    </li>
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Accepted Budget:</b> <span></span>
                                                                    </li>
                                                                    <li class="list-group-item d-flex align-items-center mt-1 borderless bg-light p-0">
                                                                        <b>Progress:</b> <span>0%</span> <div class="progress ms-2 border border-primary"></div>
                                                                    </li>
                                                                    <li class="list-group-item mt-1 borderless bg-light p-0">
                                                                        <b>Last Bump:</b> <span></span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="card-footer py-3 bg-white text-muted">
                                            
                                            <button  class="button button-rounded button-small m-0 float-end ms-3" style="background-color: rgb(76,154,199, .15); border: 1px solid rgb(76,154,199);" ><span style="color: rgb(76,154,199); text-shadow: none;">Hapus Proyek</span></button>

                                            <button data-bs-toggle="modal" data-bs-target="#modalCreateProject"
                                            style="border: 1px solid rgb(76,154,199);" class="button button-rounded button-small m-0 ms-3 float-end">Edit Proyek</button>

                                            <a href="{{route('web.product.detail', 1)}}" style="border: 1px solid rgb(76,154,199);" class="button button-rounded button-small m-0 float-end">Preview Proyek</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-3">
                            @include('theme.web.desktop.account.sidebar')
                        </div>
                    </div>
                </div>
                <!-- Wave Shape Divider - Bottom ============================================= -->
                <div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
            </div>
        </div>
    </section>

    <!-- Modal create project -->
    <div class="modal fade" id="modalCreateProject" tabindex="-1" aria-labelledby="modalCreateProjectLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalCreateProjectLabel">BUAT PROYEK</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="formCreateProduct">
                @csrf
                <div class="modal-body">
                    <div class="mb-4">
                        <label for="title" class="form-label">Title</label>
                        <input type="text" class="form-control" id="title" name="title" required>
                    </div>

                    <div class="mb-4">
                        <label for="formLogo" class="form-label">Logo</label>
                        <input class="form-control" type="file" id="formLogo" name="deliverable">
                    </div>

                    <!-- select 2 library -->
                    <div class="mb-4">
                        <label for="requirements">Tags</label>
                        <div class="form-group">
                            <select id="skills" name="tags[]" class="form-select select-skills" data-width="100%" style="height: 100px;" multiple="multiple" required>
                                <option selected="selected">orange</option>
                                <option>white</option>
                                <option>purple</option>
                            </select>
                        </div>
                    </div>
                    <!-- /select 2 library -->
    
                    <!-- wyswg -->
                    <div class="mb-4">
                        <label for="description" class="form-label">Description</label>
                        <textarea class="description" id="section001" name="description"></textarea>
                    </div>
                    <!-- wyswg -->

                    <div class="mb-4">
                        <label for="formDeliverable" class="form-label">Deliverable</label>
                        <input class="form-control" type="file" id="formDeliverable" name="deliverable" required>
                    </div>

                    <div class="mb-4">
                        <label for="formTrialVersion" class="form-label">Trial Version</label>
                        <input class="form-control" type="file" id="formTrialVersion" name="trial_version">
                    </div>


                    <div class="mb-4">
                        <label for="price" class="form-label">Price</label>
                        <div class="input-group">
                            <span class="input-group-text">Rp</span>
                            <input type="text" id="price" name="price" class="form-control"  required>
                        </div>
                    </div>

                    <button type="submit" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Simpan dan publish</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- /End modal create project-->
    
    @section('custom_js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
		jQuery(document).ready( function($){
			$(window).on( 'pluginIsotopeReady', function(){
				$('#product').isotope({
					transitionDuration: '0.65s',
					getSortData: {
						name: '.product-title',
						// price_lh: function( itemElem ) {
						// 	if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
						// 		var price = $(itemElem).find('.product-price ins').text();
						// 	} else {
						// 		var price = $(itemElem).find('.product-price').text();
						// 	}

						// 	priceNum = price.split("Rp ");

						// 	return parseFloat( priceNum[1].replaceAll(",", ""));
						// },
						// price_hl: function( itemElem ) {
						// 	if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
						// 		var price = $(itemElem).find('.product-price ins').text();
						// 	} else {
						// 		var price = $(itemElem).find('.product-price').text();
						// 	}

						// 	priceNum = price.split("Rp ");
						// 	return parseFloat( priceNum[1].replaceAll(",", ""));
						// }
					},
					sortAscending: {
						name: true,
						// price_lh: true,
						// price_hl: false
					}
				});

				$('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
					var element = $(this),
						elementFilter = element.children('a').attr('data-filter'),
						elementFilterContainer = element.parents('.custom-filter').attr('data-container');

					elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

					element.append('<span>'+ elementFilterCount +'</span>');

				});

				$('.product-sorting li').click( function() {
					$('.product-sorting').find('li').removeClass( 'active-filter' );
					$(this).addClass( 'active-filter' );
					var sortByValue = $(this).find('a').attr('data-sort-by');
					$('#product').isotope({ sortBy: sortByValue });
					return false;
				});


                $(".select-skills").select2({
                    placeholder: 'Choose tags',
                    tags: true,
                    tokenSeparators: [',', ' '],
                    dropdownParent: $('#modalCreateProject')
                });

                $("#price").keyup(function(){
                    let rupiah = formatRupiah($(this).val())
                    $("#price").val(rupiah)
                });
			});

            /* Fungsi formatRupiah */
            function formatRupiah(angka){
                // console.log(typeof angka);
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

                
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
            }

            $("#formAddToCart").submit(function(e){
                e.preventDefault();
                var form_data = new FormData($(this)[0]);
                $.ajax({
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: false,
                    url: "{{route('web.product.add_cart')}}",
                    data: form_data,

                    success: function(response) {
                        success_toastr(response.message);
                        console.log(response.data);
                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                        error_toastr(errorThrown);
                    }

                });
            })

            // handle form create projects
            $("#formCreateProduct").submit(function(event) {
                event.preventDefault();

                var form_data = new FormData($(this)[0]);
                
                $.ajax({
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: false,
                    url: "{{route('web.product.create')}}",
                    data: form_data,

                    success: function(response) {
                        success_toastr(response.message);
                        // console.log(response.data);
                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                        error_toastr(errorThrown);
                    }

                });
            })

            $("input#choice-screenshot").change(function(){
                var filesAmount = this.files.length;
                if (filesAmount <= 3) {
                    container_preview_screenshot.empty()
                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
    
                        reader.onload = function(event) {
                            container_preview_screenshot.append(`
                                    <div class="card card-screenshot p-1 border me-3 rounded-2 col-3 overflow-hidden">
                                            <div class="preview-screenshot-container">
                                                    <div class="delete-screenshot" role="button">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" fill="#343738" viewBox="0 0 24 24"><path d="M23 20.168l-8.185-8.187 8.185-8.174-2.832-2.807-8.182 8.179-8.176-8.179-2.81 2.81 8.186 8.196-8.186 8.184 2.81 2.81 8.203-8.192 8.18 8.192z"/></svg>
                                                    </div>
                                                    <div class="preview-screenshot">
                                                        <img src="${event.target.result}" alt="Standard Post with Image">
                                                    </div>
                                            </div>
                                    </div>
                            `)
    
                            $(".delete-screenshot").click(function(){
                                console.log($(this).parent().parent().remove());
                            });
                            // $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(container_preview_screenshot);
                        }
    
                        reader.readAsDataURL(this.files[i]);
                    }
                } else {
                    error_toastr("You can select only 3 images");
                }

            })

            let container_preview_screenshot = $(".container-preview-screenshot")
            let list_screenshot = [] 
            
		});
	</script>
    @endsection
</x-web-layout>