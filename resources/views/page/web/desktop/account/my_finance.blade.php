<x-web-layout title="Daftar Penjualan" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap p-0">
            <!-- Wave Shape Divider ============================================= -->
            <!-- Section Courses ============================================= -->
            <div class="section parallax m-0">
                <!-- Wave Shape Divider ============================================= -->
                <div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
                <div class="container">
                    <div class="row">
                        <div class="postcontent col-9">

                            <div class="heading-block m-0 border-bottom-0">
                                <h3 class="">Keuangan</h3>
                            </div>
                            
                            <div class="row clearfix mt-4">
                                <div class="col-lg-12">
                                    <div class="tabs tabs-alt clearfix" id="tabs-profile">
                                        <ul class="tab-nav clearfix">
                                            <li><a href="#tab-balance"><i class="icon-money"></i> Balance</a></li>
                                            <li><a href="#tab-history"><i class="icon-history"></i> Credit History</a></li>
                                            <li><a href="#tab-deposit"><i class="icon-money-bill-wave"></i> Deposit</a></li>
                                        </ul>
                                        <div class="tab-container">
                                            <div class="tab-content clearfix" id="tab-balance">
                                                <div class="card-container p-2 px-4 pb-4">
                                                    <div class="container-card-user position-relative d-flex justify-content-center">
                                                        <!-- card -->
                                                        <div class="card-user">
                                                            <span>
                                                                <h2 class="text-light m-0">Rp 1.000.000</h2>
                                                            </span>
                                                            <span class="text-light">1129760684</span>
                                                            <span class="text-light text-sm">Sayidina Ahmdal Qososyi</span>
                                                        </div>
                                                        <!-- /card -->
                                                        <div class="info-credit">
                                                            <div class="d-flex justify-content-between">
                                                                <b>Bank</b>
                                                                <span class="text-sm">Bank Negara Indonesia (BNI)</span>
                                                            </div>
                                                            <div class="d-flex justify-content-between">
                                                                <b>Branch</b>
                                                                <span class="text-sm">Aikmel</span>
                                                            </div>
                                                            <div class="d-flex justify-content-between">
                                                                <b>Cut-off method</b>
                                                                <span class="text-sm">Manual</span>
                                                            </div>
                                                            <div class="d-flex justify-content-between">
                                                                <b>Jumlah Cut-off</b>
                                                                <span class="text-sm">Rp 10.000</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="divide-vertical"></div>
                                                    <div class="col">
                                                        <h2 class="text-heading-color">Pengaturan pembayaran</h2>

                                                        <form class="row g-2">
                                                            <div class="col-12">
                                                                <label for="bank-input">Bank</label>
                                                                <select type="password" class="form-select" name="bank" id="bank-input">
                                                                    <option selected>Pilih Bank</option>
                                                                    <option value="bni">Bank Negara Indonesia (BNI)</option>
                                                                    <option value="bri">Bank Rakyat Indonesia (BRI)</option>
                                                                    <option value="mandiri">Bank Mandiri</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-12">
                                                                <label for="branch-input">Cabang</label>
                                                                <input type="text" class="form-control" id="branch-input" name="branch" value="Aikmel" aria-describedby="branchHelpInline">
                                                                <span id="branchHelpInline" class="form-text">
                                                                    <i>Kantor cabang bank anda.</i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <label for="city-input">Kota</label>
                                                                <input type="text" id="city-input" name="city" class="form-control" value="Selong" aria-describedby="kotaHelpInline">
                                                                <span id="kotaHelpInline" class="form-text">
                                                                    <i>Kota tempat bank anda.</i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <label for="account-number-input">Nomor Rekening</label>
                                                                <input type="text" id="account-number-input" name="account_number" class="form-control" value="1129760684" aria-describedby="accountNumberHelpInline">
                                                                <span id="accountNumberHelpInline" class="form-text">
                                                                    <i>Nomor rekening, <b>Pastikan benar</b>.</i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <label for="name-input">Nama</label>
                                                                <input type="text" id="name-input" name="name" class="form-control" value="Sayidina ahmadal qososyi" aria-describedby="nameHelpInline">
                                                                <span id="nameHelpInline" class="form-text">
                                                                    <i>Nama pemilik rekening.</i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <label for="bank-input">Cut-off method</label>
                                                                <select class="form-select" id="bank-input" aria-describedby="methodCutOffHelpInline">
                                                                    <option selected>Pilih Metode Cut-off</option>
                                                                    <option value="bni">Manual</option>
                                                                    <option value="bri">Mingguan</option>
                                                                    <option value="mandiri">Bulanan</option>
                                                                    <option value="mandiri">Tahunan</option>
                                                                </select>
                                                                <span id="methodCutOffHelpInline" class="form-text">
                                                                    <i>Kapan saldo anda akan ditransfer ke rekening anda.</i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <label for="bank-input">Jumlah Cut-off</label>
                                                                <input type="text" class="form-control" value="Rp 50.000" aria-describedby="minCutOffHelpInline">
                                                                <span id="minCutOffHelpInline" class="form-text">
                                                                    <i>Setelah melewati nilai ini, saldo akan ditrasfer ke rekening anda.</i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <button class="m-0 mt-4 button button-small rounded">Simpan Perubahan</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-content clearfix" id="tab-history">
                                                <table class="table display table-striped table-border" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">No</th>
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">Type</th>
                                                            <th class="text-center">Title</th>
                                                            <th class="text-center">Amount</th>
                                                            <th class="text-center">Balance</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="tab-content clearfix" id="tab-deposit">
                                                <table class="table display table-striped table-border" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">No</th>
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">Bank</th>
                                                            <th class="text-center">Amount</th>
                                                            <th class="text-center">Fee</th>
                                                            <th class="text-center">status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-3">
                            @include('theme.web.desktop.account.sidebar')
                        </div>
                    </div>
                </div>
                <!-- Wave Shape Divider - Bottom ============================================= -->
                <div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
            </div>
        </div>
    </section>

    <!-- Modal create project -->
    <div class="modal fade" id="modalCreateProject" tabindex="-1" aria-labelledby="modalCreateProjectLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalCreateProjectLabel">BUAT PROYEK</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="formCreateProduct">
                @csrf
                <div class="modal-body">
                    <div class="mb-4">
                        <label for="title" class="form-label">Title</label>
                        <input type="text" class="form-control" id="title" name="title" required>
                    </div>

                    <div class="mb-4">
                        <label for="formLogo" class="form-label">Logo</label>
                        <input class="form-control" type="file" id="formLogo" name="deliverable">
                    </div>

                    <!-- select 2 library -->
                    <div class="mb-4">
                        <label for="requirements">Tags</label>
                        <div class="form-group">
                            <select id="skills" name="tags[]" class="form-select select-skills" data-width="100%" style="height: 100px;" multiple="multiple" required>
                                <option selected="selected">orange</option>
                                <option>white</option>
                                <option>purple</option>
                            </select>
                        </div>
                    </div>
                    <!-- /select 2 library -->
    
                    <!-- wyswg -->
                    <div class="mb-4">
                        <label for="description" class="form-label">Description</label>
                        <textarea class="description" id="section001" name="description"></textarea>
                    </div>
                    <!-- wyswg -->

                    <div class="mb-4">
                        <label for="formDeliverable" class="form-label">Deliverable</label>
                        <input class="form-control" type="file" id="formDeliverable" name="deliverable" required>
                    </div>

                    <div class="mb-4">
                        <label for="formTrialVersion" class="form-label">Trial Version</label>
                        <input class="form-control" type="file" id="formTrialVersion" name="trial_version">
                    </div>


                    <div class="mb-4">
                        <label for="price" class="form-label">Price</label>
                        <div class="input-group">
                            <span class="input-group-text">Rp</span>
                            <input type="text" id="price" name="price" class="form-control"  required>
                        </div>
                    </div>

                    <button type="submit" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Simpan dan publish</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- /End modal create project-->
    
    @section('custom_js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript" src="{{asset('semicolon/js/components/bs-datatable.js')}}"></script>
    <script>
		jQuery(document).ready( function($){
			$(window).on( 'pluginIsotopeReady', function(){
				$('#product').isotope({
					transitionDuration: '0.65s',
					getSortData: {
						name: '.product-title',
						// price_lh: function( itemElem ) {
						// 	if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
						// 		var price = $(itemElem).find('.product-price ins').text();
						// 	} else {
						// 		var price = $(itemElem).find('.product-price').text();
						// 	}

						// 	priceNum = price.split("Rp ");

						// 	return parseFloat( priceNum[1].replaceAll(",", ""));
						// },
						// price_hl: function( itemElem ) {
						// 	if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
						// 		var price = $(itemElem).find('.product-price ins').text();
						// 	} else {
						// 		var price = $(itemElem).find('.product-price').text();
						// 	}

						// 	priceNum = price.split("Rp ");
						// 	return parseFloat( priceNum[1].replaceAll(",", ""));
						// }
					},
					sortAscending: {
						name: true,
						// price_lh: true,
						// price_hl: false
					}
				});

				$('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
					var element = $(this),
						elementFilter = element.children('a').attr('data-filter'),
						elementFilterContainer = element.parents('.custom-filter').attr('data-container');

					elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

					element.append('<span>'+ elementFilterCount +'</span>');

				});

				$('.product-sorting li').click( function() {
					$('.product-sorting').find('li').removeClass( 'active-filter' );
					$(this).addClass( 'active-filter' );
					var sortByValue = $(this).find('a').attr('data-sort-by');
					$('#product').isotope({ sortBy: sortByValue });
					return false;
				});


                $(".select-skills").select2({
                    placeholder: 'Choose tags',
                    tags: true,
                    tokenSeparators: [',', ' '],
                    dropdownParent: $('#modalCreateProject')
                });

                $("#price").keyup(function(){
                    let rupiah = formatRupiah($(this).val())
                    $("#price").val(rupiah)
                });
			});

            $('table.display').DataTable();

            /* Fungsi formatRupiah */
            function formatRupiah(angka){
                // console.log(typeof angka);
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

                
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
            }

            $("#formAddToCart").submit(function(e){
                e.preventDefault();
                var form_data = new FormData($(this)[0]);
                $.ajax({
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: false,
                    url: "{{route('web.product.add_cart')}}",
                    data: form_data,

                    success: function(response) {
                        success_toastr(response.message);
                        console.log(response.data);
                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                        error_toastr(errorThrown);
                    }

                });
            })

            // handle form create projects
            $("#formCreateProduct").submit(function(event) {
                event.preventDefault();

                var form_data = new FormData($(this)[0]);
                
                $.ajax({
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: false,
                    url: "{{route('web.product.create')}}",
                    data: form_data,

                    success: function(response) {
                        success_toastr(response.message);
                        // console.log(response.data);
                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                        error_toastr(errorThrown);
                    }

                });
            })

            $("input#choice-screenshot").change(function(){
                var filesAmount = this.files.length;
                if (filesAmount <= 3) {
                    container_preview_screenshot.empty()
                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
    
                        reader.onload = function(event) {
                            container_preview_screenshot.append(`
                                    <div class="card card-screenshot p-1 border me-3 rounded-2 col-3 overflow-hidden">
                                            <div class="preview-screenshot-container">
                                                    <div class="delete-screenshot" role="button">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" fill="#343738" viewBox="0 0 24 24"><path d="M23 20.168l-8.185-8.187 8.185-8.174-2.832-2.807-8.182 8.179-8.176-8.179-2.81 2.81 8.186 8.196-8.186 8.184 2.81 2.81 8.203-8.192 8.18 8.192z"/></svg>
                                                    </div>
                                                    <div class="preview-screenshot">
                                                        <img src="${event.target.result}" alt="Standard Post with Image">
                                                    </div>
                                            </div>
                                    </div>
                            `)
    
                            $(".delete-screenshot").click(function(){
                                console.log($(this).parent().parent().remove());
                            });
                            // $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(container_preview_screenshot);
                        }
    
                        reader.readAsDataURL(this.files[i]);
                    }
                } else {
                    error_toastr("You can select only 3 images");
                }

            })

            let container_preview_screenshot = $(".container-preview-screenshot")
            let list_screenshot = [] 
            
		});
	</script>
    @endsection
</x-web-layout>