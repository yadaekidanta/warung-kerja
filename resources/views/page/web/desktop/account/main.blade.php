<x-web-layout title="Akun Saya" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row clearfix">
                    <div class="col-9">
                        <div>
                            <h2>Akun Saya</h2>
                        </div>
                        <div class="row align-items-center justify-content-center grid-border">
                            <div class="col-sm-6 col-lg-3 text-center z-3 rounded shadow bg-white service-feature">
                                <i class="i-plain mx-auto mb-0 icon-diamond"></i>
                                <div class="counter counter-small" style="color: #1abc9c;">
                                    <span data-from="100" data-to="8465" data-refresh-interval="50" data-speed="2000">8465</span>
                                </div>
                                <h5>Poin Saya</h5>
                            </div>
                            <div class="col-sm-6 col-lg-3 text-center z-3 rounded shadow bg-white service-feature">
                                <i class="i-plain mx-auto mb-0 icon-money"></i>
                                <div class="counter counter-small" style="color: #3498db;">
                                    <span data-from="100" data-to="8465" data-refresh-interval="50" data-speed="2000">8465</span>
                                </div>
                                <h5>Saldo Saya</h5>
                            </div>
                            <div class="col-sm-6 col-lg-3 text-center z-3 rounded shadow bg-white service-feature">
                                <i class="i-plain mx-auto mb-0 icon-chart-line"></i>
                                <div class="counter counter-small" style="color: #9b59b6;">
                                    <span data-from="100" data-to="8465" data-refresh-interval="50" data-speed="2000">8465</span>
                                </div>
                                <h5>Penjualan Saya</h5>
                            </div>
                            <div class="col-sm-6 col-lg-3 text-center z-3 rounded shadow bg-white service-feature">
                                <i class="i-plain mx-auto mb-0 icon-line-star"></i>
                                <div class="counter counter-small" style="color: #e74c3c;">
                                    <span data-from="100" data-to="8465" data-refresh-interval="50" data-speed="2000">8465</span>
                                </div>
                                <h5>Peringkat Saya</h5>
                            </div>
                        </div>
                        <div class="row grid-border mt-5">
                            <div class="col-sm-12 col-lg-6 z-3 rounded">
                                <h3 class="mt-3">Aktifitas</h3>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Waktu</th>
                                            <th>Aktifitas</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <code>5/23/2021</code>
                                            </td>
                                            <td>Payment for VPS2 completed</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-12 col-lg-6 z-3 rounded">
                                <div class="card">
                                    <div class="card-body">
                                        <div style="display: flex; justify-content: space-between; align-items: center;">
                                            <h3 class="m-0">Portofolio</h3>
                                            <button data-bs-toggle="modal" data-bs-target="#modalAddPortofolio" class="button button-rounded button-small">Tambah</button>
                                        </div>

                                        <hr class="">
                                        
                                        <div class="grid-inner row g-2">
                                            <div class="col-lg-6">
                                                <div class="portfolio-image">
                                                    <div class="grid-inner">
                                                        <a href="portfolio-single.html">
                                                            <img src="{{asset('img/courses/1.jpg')}}" alt="Open Imagination">
                                                        </a>
                                                        <div class="bg-overlay">
                                                            <div class="bg-overlay-content dark" data-hover-animate="fadeIn">
                                                                <a role="button" data-bs-toggle="modal" data-bs-target="#modalAddPortofolio" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350"><i class="icon-edit2"></i></a>
                                                                <a id="btn-delete" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350"><i class="icon-trash"></i></a>
                                                            </div>
                                                            <div class="bg-overlay-bg dark" data-hover-animate="fadeIn"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portfolio-desc col-lg-6" style="margin-top:-5%;">
                                                <h3><a href="portfolio-single.html">Judul</a></h3>
                                                <span><a href="#">2021</a></span>
                                                <p class="d-none d-sm-block" style="line-height: 1;text-justify: inter-word;">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, ....
                                                </p>
                                            </div>
                                        </div>
                                        <div class="grid-inner row g-2">
                                            <div class="col-lg-6">
                                                <div class="portfolio-image">
                                                    <div class="grid-inner">
                                                        <a href="portfolio-single.html">
                                                            <img src="{{asset('img/courses/1.jpg')}}" alt="Open Imagination">
                                                        </a>
                                                        <div class="bg-overlay">
                                                            <div class="bg-overlay-content dark" data-hover-animate="fadeIn">
                                                                <a role="button" data-bs-toggle="modal" data-bs-target="#modalAddPortofolio" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350"><i class="icon-edit2"></i></a>
                                                                <a id="btn-delete" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350"><i class="icon-trash"></i></a>
                                                            </div>
                                                            <div class="bg-overlay-bg dark" data-hover-animate="fadeIn"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portfolio-desc col-lg-6" style="margin-top:-5%;">
                                                <h3><a href="portfolio-single.html">Judul</a></h3>
                                                <span><a href="#">2021</a></span>
                                                <p class="d-none d-sm-block" style="line-height: 1;text-justify: inter-word;">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, ....
                                                </p>
                                            </div>
                                        </div>
                                        <!-- <div class="grid-inner row g-0">
                                            <div class="col-lg-6">
                                                <div class="portfolio-image">
                                                    <div class="grid-inner">
                                                        <a href="portfolio-single.html">
                                                            <img src="{{asset('img/courses/1.jpg')}}" alt="Open Imagination">
                                                        </a>
                                                        <div class="bg-overlay">
                                                            <div class="bg-overlay-content dark" data-hover-animate="fadeIn">
                                                                <a href="{{asset('img/courses/1.jpg')}}" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350" data-lightbox="image" title="Image"><i class="icon-line-plus"></i></a>
                                                                <a href="portfolio-single.html" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350"><i class="icon-line-ellipsis"></i></a>
                                                            </div>
                                                            <div class="bg-overlay-bg dark" data-hover-animate="fadeIn"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portfolio-desc col-lg-6" style="margin-top:-5%;">
                                                <h3><a href="portfolio-single.html">Judul</a></h3>
                                                <span><a href="#">2021</a></span>
                                                <p class="d-none d-sm-block" style="line-height: 1;text-justify: inter-word;">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, ....
                                                </p>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-100 line d-block d-md-none"></div>
                    <div class="col-3">
                        @include('theme.web.desktop.account.sidebar')
                        
                        <div class="fancy-title topmargin title-border">
                        </div>
                        
                        <div class="card">
                            <div class="card-body">
                                <div style="display: flex; justify-content: space-between; align-items: center;">
                                    <h4 class="m-0">Keterampilan</h4>
                                    <button data-bs-toggle="modal" data-bs-target="#modalAddSkills" class="button button-rounded button-small">Tambah </button>
                                </div>

                                <hr>

                                <p class="mb-2 card-title-sub text-uppercase fw-normal ls1">
                                    <span class="badge bg-secondary text-light-50" style="margin-top: 3px;">Codeigniter</span>
                                    <span class="badge bg-secondary text-light-50" style="margin-top: 3px;">PHP</span>
                                    <span class="badge bg-secondary text-light-50" style="margin-top: 3px;">Laravel</span>
                                    <span class="badge bg-secondary text-light-50" style="margin-top: 3px;">Laravel</span>
                                    <span class="badge bg-secondary text-light-50" style="margin-top: 3px;">Laravel</span>
                                    <span class="badge bg-secondary text-light-50" style="margin-top: 3px;">Laravel</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal bid project -->
    <div class="modal fade" id="modalAddPortofolio" tabindex="-1" aria-labelledby="modalAddPortofolio" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalCreateProjectLabel">PLace New Bid</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="formAddPortofolio">
                    @csrf
                    <div class="modal-body">

                        <div class="mb-4">
                            <label for="date" class="form-label">Year</label>
                            <input type="date" id="date" name="date" class="form-control"  required>
                        </div>
        
                        <!-- wyswg -->
                        <div class="mb-4">
                            <label for="description" class="form-label">Description</label>
                            <textarea id="description" name="description"></textarea>
                        </div>
                        <!-- wyswg -->

                        <div class="mb-4">
                            <label for="link" class="form-label">Link</label>
                            <input type="text" id="link" name="link" class="form-control"  required>
                        </div>

                        <div class="mb-4">
                            <label for="image" class="form-label">Image</label>
                            <input class="form-control" type="file" id="image" name="image">
                        </div>

                        <button type="submit" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /end Modal bid project -->

    <!-- Modal bid project -->
    <div class="modal fade" id="modalAddSkills" tabindex="-1" aria-labelledby="modalAddSkills" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalCreateProjectLabel">PLace New Bid</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="formAddskills">
                    @csrf
                    <div class="modal-body">

                        <!-- select 2 library -->
                        <div class="mb-4">
                            <label for="requirements">Skill Requirements</label>
                            <div class="form-group">
                                <select id="skills" name="skills[]" class="form-select select-skills" data-width="100%" style="height: 100px;" multiple="multiple" required>
                                    <option selected="selected">orange</option>
                                    <option>white</option>
                                    <option>purple</option>
                                </select>
                            </div>
                        </div>
                        <!-- /select 2 library -->

                        <button type="submit" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /end Modal bid project -->


    @section('custom_js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".select-skills").select2({
                placeholder: 'Choose tags',
                tags: true,
                tokenSeparators: [',', ' '],
                dropdownParent: $('#modalAddSkills')
            });

            // handle form add portofolio
            $("#formAddPortofolio").submit(function(event) {
                event.preventDefault();
                var form_data = new FormData($(this)[0]);

                $.ajax({
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: false,
                    url: "{{route('web.portofolio.create')}}",
                    data: form_data,

                    success: function(response) {
                        success_toastr(response.message);
                        console.log(response.data);
                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                        error_toastr(errorThrown);
                    }

                });
            })

            // handle form add skills
            $("#formAddskills").submit(function(event) {
                event.preventDefault();
                var form_data = new FormData($(this)[0]);

                $.ajax({
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: false,
                    url: "{{route('web.skills.create')}}", // ganti the url nanti ketika sudah di sediakan
                    data: form_data,

                    success: function(response) {
                        success_toastr(response.message);
                        console.log(response.data);
                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                        error_toastr(errorThrown);
                    }

                });
            })
        });
    </script>
    @endsection

</x-web-layout>