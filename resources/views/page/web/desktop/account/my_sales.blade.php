<x-web-layout title="Daftar Penjualan" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap p-0">
            <!-- Wave Shape Divider ============================================= -->
            <!-- Section Courses ============================================= -->
            <div class="section parallax m-0" data-bottom-top="background-position:0px 100px;" data-top-bottom="background-position:0px -500px;">
                <!-- Wave Shape Divider ============================================= -->
                <div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
                <div class="container">
                    <div class="row">
                        <div class="postcontent col-9">
                            <div class="heading-block m-0 border-bottom-0">
                                <h3>Daftar Penjualan</h3>
                            </div>
							<div class="card shadow p-2 mt-2 pt-4 px-1 bg-white row grid-container gutter-20" data-layout="fitRows" style="margin-top:-7%;">
                                <table id="data-table" class="table table-striped table-border" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No</th>
                                            <th class="text-center">Pembeli</th>
                                            <th class="text-center">Nama produk</th>
                                            <th class="text-center">Tanggal</th>
                                            <th class="text-center">Harga</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td class="text-center">Misterrizky</td>
                                            <td class="text-center">Buku saku belajar Python</td>
                                            <td class="text-center">2011/04/25</td>
                                            <td class="text-center">Rp 1,000,000</td>
                                            <td class="text-center">
                                                <span class="p-1 alert-primary rounded">Completed</span>
                                            </td>
                                            <td class="text-center">
                                                <span class="p-1 button mt-0 hover rounded d-flex justify-content-center align-items-center">
                                                    <i class="icon-eye2 text-white m-0"></i>
                                                </span>
                                                <span class="p-1 button mt-0 bg-light border hover border-primary rounded rounded d-flex justify-content-center align-items-center">
                                                    <i class="icon-trash2 text-primary m-0"></i>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">2</td>
                                            <td class="text-center">Osi</td>
                                            <td class="text-center">Buku saku belajar Html</td>
                                            <td class="text-center">2011/04/25</td>
                                            <td class="text-center">Rp 1,000,000</td>
                                            <td class="text-center">
                                                <span class="p-1 alert-danger rounded">Canceled</span>
                                            </td>
                                            <td class="text-center">
                                                <span class="p-1 button mt-0 hover rounded d-flex justify-content-center align-items-center">
                                                    <i class="icon-eye2 text-white m-0"></i>
                                                </span>
                                                <span class="p-1 button mt-0 bg-light border hover border-primary rounded rounded d-flex justify-content-center align-items-center">
                                                    <i class="icon-trash2 text-primary m-0"></i>
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <div class="col-3">
                            @include('theme.web.desktop.account.sidebar')
                        </div>
                    </div>
                </div>
                <!-- Wave Shape Divider - Bottom ============================================= -->
                <div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
            </div>
        </div>
    </section>

    <!-- Modal create project -->
    <div class="modal fade" id="modalCreateProject" tabindex="-1" aria-labelledby="modalCreateProjectLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalCreateProjectLabel">BUAT PROYEK</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="formCreateProduct">
                @csrf
                <div class="modal-body">
                    <div class="mb-4">
                        <label for="title" class="form-label">Title</label>
                        <input type="text" class="form-control" id="title" name="title" required>
                    </div>

                    <div class="mb-4">
                        <label for="formLogo" class="form-label">Logo</label>
                        <input class="form-control" type="file" id="formLogo" name="deliverable">
                    </div>

                    <!-- select 2 library -->
                    <div class="mb-4">
                        <label for="requirements">Tags</label>
                        <div class="form-group">
                            <select id="skills" name="tags[]" class="form-select select-skills" data-width="100%" style="height: 100px;" multiple="multiple" required>
                                <option selected="selected">orange</option>
                                <option>white</option>
                                <option>purple</option>
                            </select>
                        </div>
                    </div>
                    <!-- /select 2 library -->
    
                    <!-- wyswg -->
                    <div class="mb-4">
                        <label for="description" class="form-label">Description</label>
                        <textarea class="description" id="section001" name="description"></textarea>
                    </div>
                    <!-- wyswg -->

                    <div class="mb-4">
                        <label for="formDeliverable" class="form-label">Deliverable</label>
                        <input class="form-control" type="file" id="formDeliverable" name="deliverable" required>
                    </div>

                    <div class="mb-4">
                        <label for="formTrialVersion" class="form-label">Trial Version</label>
                        <input class="form-control" type="file" id="formTrialVersion" name="trial_version">
                    </div>


                    <div class="mb-4">
                        <label for="price" class="form-label">Price</label>
                        <div class="input-group">
                            <span class="input-group-text">Rp</span>
                            <input type="text" id="price" name="price" class="form-control"  required>
                        </div>
                    </div>

                    <button type="submit" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Simpan dan publish</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- /End modal create project-->
    
    @section('custom_js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript" src="{{asset('semicolon/js/components/bs-datatable.js')}}"></script>
    <script>
		jQuery(document).ready( function($){
			$(window).on( 'pluginIsotopeReady', function(){
				$('#product').isotope({
					transitionDuration: '0.65s',
					getSortData: {
						name: '.product-title',
						// price_lh: function( itemElem ) {
						// 	if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
						// 		var price = $(itemElem).find('.product-price ins').text();
						// 	} else {
						// 		var price = $(itemElem).find('.product-price').text();
						// 	}

						// 	priceNum = price.split("Rp ");

						// 	return parseFloat( priceNum[1].replaceAll(",", ""));
						// },
						// price_hl: function( itemElem ) {
						// 	if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
						// 		var price = $(itemElem).find('.product-price ins').text();
						// 	} else {
						// 		var price = $(itemElem).find('.product-price').text();
						// 	}

						// 	priceNum = price.split("Rp ");
						// 	return parseFloat( priceNum[1].replaceAll(",", ""));
						// }
					},
					sortAscending: {
						name: true,
						// price_lh: true,
						// price_hl: false
					}
				});

				$('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
					var element = $(this),
						elementFilter = element.children('a').attr('data-filter'),
						elementFilterContainer = element.parents('.custom-filter').attr('data-container');

					elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

					element.append('<span>'+ elementFilterCount +'</span>');

				});

				$('.product-sorting li').click( function() {
					$('.product-sorting').find('li').removeClass( 'active-filter' );
					$(this).addClass( 'active-filter' );
					var sortByValue = $(this).find('a').attr('data-sort-by');
					$('#product').isotope({ sortBy: sortByValue });
					return false;
				});


                $(".select-skills").select2({
                    placeholder: 'Choose tags',
                    tags: true,
                    tokenSeparators: [',', ' '],
                    dropdownParent: $('#modalCreateProject')
                });

                $("#price").keyup(function(){
                    let rupiah = formatRupiah($(this).val())
                    $("#price").val(rupiah)
                });
			});

            $('#data-table').DataTable();

            /* Fungsi formatRupiah */
            function formatRupiah(angka){
                // console.log(typeof angka);
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

                
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
            }

            $("#formAddToCart").submit(function(e){
                e.preventDefault();
                var form_data = new FormData($(this)[0]);
                $.ajax({
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: false,
                    url: "{{route('web.product.add_cart')}}",
                    data: form_data,

                    success: function(response) {
                        success_toastr(response.message);
                        console.log(response.data);
                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                        error_toastr(errorThrown);
                    }

                });
            })

            // handle form create projects
            $("#formCreateProduct").submit(function(event) {
                event.preventDefault();

                var form_data = new FormData($(this)[0]);
                
                $.ajax({
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: false,
                    url: "{{route('web.product.create')}}",
                    data: form_data,

                    success: function(response) {
                        success_toastr(response.message);
                        // console.log(response.data);
                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                        error_toastr(errorThrown);
                    }

                });
            })

            $("input#choice-screenshot").change(function(){
                var filesAmount = this.files.length;
                if (filesAmount <= 3) {
                    container_preview_screenshot.empty()
                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
    
                        reader.onload = function(event) {
                            container_preview_screenshot.append(`
                                    <div class="card card-screenshot p-1 border me-3 rounded-2 col-3 overflow-hidden">
                                            <div class="preview-screenshot-container">
                                                    <div class="delete-screenshot" role="button">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" fill="#343738" viewBox="0 0 24 24"><path d="M23 20.168l-8.185-8.187 8.185-8.174-2.832-2.807-8.182 8.179-8.176-8.179-2.81 2.81 8.186 8.196-8.186 8.184 2.81 2.81 8.203-8.192 8.18 8.192z"/></svg>
                                                    </div>
                                                    <div class="preview-screenshot">
                                                        <img src="${event.target.result}" alt="Standard Post with Image">
                                                    </div>
                                            </div>
                                    </div>
                            `)
    
                            $(".delete-screenshot").click(function(){
                                console.log($(this).parent().parent().remove());
                            });
                            // $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(container_preview_screenshot);
                        }
    
                        reader.readAsDataURL(this.files[i]);
                    }
                } else {
                    error_toastr("You can select only 3 images");
                }

            })

            let container_preview_screenshot = $(".container-preview-screenshot")
            let list_screenshot = [] 
            
		});
	</script>
    @endsection
</x-web-layout>