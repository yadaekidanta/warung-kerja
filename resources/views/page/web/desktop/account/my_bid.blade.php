<x-web-layout title="List Proyek" keyword="Warung Kerja">
    <section id="content">
        <div class="content-wrap p-0">
            <!-- Wave Shape Divider ============================================= -->
            <!-- Section Courses ============================================= -->
            <div class="section m-0 parallax" background-size: auto; background-repeat: repeat"  data-bottom-top="background-position:0px 100px;" data-top-bottom="background-position:0px -500px;">
                <!-- Wave Shape Divider ============================================= -->
                <div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
                <div class="container">
                    <div class="mb-3 p-3" style="margin-top: -30px;">
                        
                    </div>

                    <div class="row">
                        <div class="postcontent col-9">
                            <div class="heading-block border-bottom-0">
                                    <h3>Daftar Penawaran</h3>
                            </div>
							<div id="project" class="shop row grid-container gutter-20" data-layout="fitRows" style="margin-top:-7%;">

                                <div class="project col-lg-12 wk-drafts">
                                    <div class="card course-card shadow hover-effect border-0">
                                        <div class="card-body" style="position: relative;">
                                            <table class="table table-borderless" cellpadding="10">
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 20%;">
                                                            <b class="float-end">Projects :</b>
                                                        </td>
                                                        <td>
                                                            <span>Membuat Web dan Mobile App dari API</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b class="float-end">Projects status :</b>
                                                        </td>
                                                        <td>
                                                            <span class="project-price badge alert-primary">Sedang berjalan</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b class="float-end">Date :</b>
                                                        </td>
                                                        <td>
                                                            <span>29/04/2022 13:54:02 WIB</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b class="float-end">Amount :</b>
                                                        </td>
                                                        <td>
                                                            <span class="project-price badge alert-primary">Rp 10,000,000</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b class="float-end">Message :</b>
                                                        </td>
                                                        <td>
                                                            <span>Hallo selamat pagi, perkenalkan nama sayidina ahmadal qososyi, saya seorang fullstack web developer, pengalaman saya menggunakan laravel sudah 1 tahun lebih, dan terkait dengan deskripsi projek ini saya sudah terbiasa menggunakan laravel, berikut salah satu project saya yang masih saya development : https://warungkerja.com/</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="card-footer py-3 bg-white text-muted">
                                            
                                            <button  class="button button-rounded button-small m-0 float-end ms-3" style="background-color: rgb(76,154,199, .15); border: 1px solid rgb(76,154,199);" ><span style="color: rgb(76,154,199); text-shadow: none;">Batalkan Penawaran</span></button>

                                            <button data-bs-toggle="modal" data-bs-target="#modalBidProject"
                                            style="border: 1px solid rgb(76,154,199);" class="button button-rounded button-small m-0 ms-3 float-end">Tambah Penawaran Baru</button>

                                            <button data-bs-toggle="modal" data-bs-target="#modalShowConversation" 
                                            style="border: 1px solid rgb(76,154,199);" class="button button-rounded button-small m-0 float-end">Lihat Diskusi</button>

                                            <!-- <button data-bs-toggle="modal" data-bs-target="#modalShowConversation" class="button button-rounded button-small m-0  float-end">Lihat Diskusi</button> -->
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-end mt-4">
                                    <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <div class="col-3">
                            @include('theme.web.desktop.account.sidebar')
                        </div>
                    </div>
                </div>
                <!-- Wave Shape Divider - Bottom ============================================= -->
                <div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url('{{asset('img/wave-3.svg')}}'); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
            </div>
        </div>





    </section>

    <!-- Modal bid project -->
    <div class="modal fade" id="modalShowConversation" tabindex="-1" aria-labelledby="modalShowConversation" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalCreateProjectLabel">Diskusi Proyek</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body row p-4">
                    <div class="col p-0">
                        <a href="javascript:void(0);" class="mt-3">
                            <div class="list-user-bids-detail-projects">
                                <img class="rounded-circle" src="{{asset('img/avatars/5m.png')}}"style="width: 40px;height:40px; object-fit: cover;">
                                <a href="javascript:void(0);">
                                    misterrizky
                                </a>
                                <div class="rating-stars mb-2"><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star-half-full"></i> <span>4.7</span></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-9 p-3 text-conversation m-0">
                        <div class="">
                            Hallo selamat pagi, perkenalkan nama sayidina ahmadal qososyi, saya seorang fullstack web developer, pengalaman saya menggunakan laravel sudah 1 tahun lebih, dan terkait dengan deskripsi projek ini saya sudah terbiasa menggunakan laravel, berikut salah satu project saya yang masih saya development : https://warungkerja.com/
                        </div>
                        <div class="mt-3 float-end text-blue text-small">
                            16/04/2022 11:00:20 WIB
                        </div>
                    </div>

                    <hr class="my-3">
                    <div class="info-bid">
                        <div>16/04/2022 10:45:56 WIB</div>
                        <div>
                            <a href="#">misterrizky</a> 
                            <span>memberikan penawaran sebesar Rp 300,000.</span>
                        </div>
                    </div>
                    <hr class="my-3">
                    
                    <div class="col mt-4">
                        <div class="row justify-content-end">
                            <div class="col-9 p-0 mt-3 align-self-end">
                                <form id="messageConversation">
                                    <!-- wyswg -->
                                    <div class="mb-4">
                                        <label for="message" class="form-label">Tulis Balasan</label>
                                        <textarea id="message" name="description"></textarea>
                                    </div>
                                    <!-- wyswg -->
        
                                    <button type="submit" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Kirim</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
    <!-- /end Modal bid project -->

    <!-- Modal bid project -->
    <div class="modal fade" id="modalBidProject" tabindex="-1" aria-labelledby="modalBidProject" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalCreateProjectLabel">PLace New Bid</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="formNewBid">
                    @csrf
                    <div class="modal-body">

                        <div class="mb-4">
                            <label for="amount" class="form-label">Amount</label>
                            <div class="input-group">
                                <span class="input-group-text">Rp</span>
                                <input type="text" id="amount" name="amount" class="form-control"  required>
                            </div>
                        </div>
        
                        <!-- wyswg -->
                        <div class="mb-4">
                            <label for="messagesecond" class="form-label">Message</label>
                            <textarea id="messagesecond" name="messagesecond"></textarea>
                        </div>
                        <!-- wyswg -->

                        <div class="mb-4">
                            <label for="formFileAttachment" class="form-label">Attachment</label>
                            <input class="form-control" type="file" id="fileAttachmentNewBid" name="attachment">
                        </div>

                        <button type="submit" class="button button-rounded button-small m-0 ms-2 mt-3" style="float:right;">Place new bid</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /end Modal bid project -->


    @section('custom_js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
		jQuery(document).ready( function($){
			$(window).on('pluginIsotopeReady', function(){
				$('#project').isotope({
					transitionDuration: '0.65s',
					getSortData: {
						price_lh: function( itemElem ) {
							if( $(itemElem).find('.project-price').find('ins').length > 0 ) {
								var price = $(itemElem).find('.project-price ins').text();
							} else {
								var price = $(itemElem).find('.project-price').text();
							}

							priceNum = price.split("Rp ");

							return parseFloat( priceNum[1].replaceAll(",", ""));
						},
						price_hl: function( itemElem ) {
							if( $(itemElem).find('.project-price').find('ins').length > 0 ) {
								var price = $(itemElem).find('.project-price ins').text();
							} else {
								var price = $(itemElem).find('.project-price').text();
							}

							priceNum = price.split("Rp ");

							return parseFloat( priceNum[1].replaceAll(",", ""));
						}
					},
					sortAscending: {
						price_lh: true,
						price_hl: false
					}
				});

                // pop up for user bid project
                $('.bid-user-container').click(function () {
                    // alert()
                    let element = $(this).parent().parent().children(".card-body").children(".popup-user-bid")
                    element.toggleClass("d-none")
                })

                // change is-checked class on buttons
                $('.custom-filter').each( function( i, listGroup ) {
                    var $listGroup = $( listGroup );
                    $listGroup.on( 'click', 'li', function() {
                        // console.log($listGroup.find('.active-filter'));
                        let currentList = $listGroup.find('.active-filter')
                        let nextList = $(this)
                        currentList.addClass("no-count")
                        currentList.removeClass('active-filter')
                        nextList.removeClass("no-count")
                        nextList.addClass('active-filter')

                        console.log(currentList, nextList);
                        // $listGroup.find('.active-filter').removeClass('active-filter');
                        // console.log($listGroup.find('.active-filter'));
                        // $( this ).removeClass('no-count');
                        // $( this ).addClass('active-filter');
                        // console.log($listGroup.find('.active-filter'));
                    });
                });

				$('.project-sorting li').click( function() {
					$('.project-sorting').find('li').removeClass( 'active-filter' );
                    
					$(this).addClass( 'active-filter' );
					var sortByValue = $(this).find('a').attr('data-sort-by');
					// $('#project').isotope({ sortBy: sortByValue });
					return false;
				});

                $(".select-skills").select2({
                    placeholder: 'Choose tags',
                    tags: true,
                    tokenSeparators: [',', ' '],
                    dropdownParent: $('#modalCreateProject')
                });

                $("#bidBudgetFrom").keyup(function(){
                    let rupiah = formatRupiah($(this).val())
                    $("#bidBudgetFrom").val(rupiah)
                });

                $("#bidBudgetTo").keyup(function(){
                    let rupiah = formatRupiah($(this).val())
                    $("#bidBudgetTo").val(rupiah)
                });
                
                $("#amount").keyup(function(){
                    let rupiah = formatRupiah($(this).val())
                    $("#amount").val(rupiah)
                });

                /* Fungsi formatRupiah */
                function formatRupiah(angka){
                    var number_string = angka.replace(/[^,\d]/g, '').toString(),
                    split   		= number_string.split(','),
                    sisa     		= split[0].length % 3,
                    rupiah     		= split[0].substr(0, sisa),
                    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
        
                    // tambahkan titik jika yang di input sudah menjadi angka ribuan
                    if(ribuan){
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }

                    
                    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                    return rupiah;
                }

                // handle form create projects
                $("#formCreateProjects").submit(function(event) {
                    event.preventDefault();

                    var form_data = new FormData($(this)[0]);
                    
                    $.ajax({
                        type: 'post',
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: false,
                        url: "{{route('web.project.create')}}",
                        data: form_data,

                        success: function(response) {
                            success_toastr(response.message);
                            // console.log(response.data);
                        },

                        error: function(jqXHR, textStatus, errorThrown) {
                            error_toastr(errorThrown);
                        }

                    });
                })

                // handle form place new bid
                $("#formNewBid").submit(function(event) {
                    event.preventDefault();
                    var form_data = new FormData($(this)[0]);

                    $.ajax({
                        type: 'post',
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: false,
                        url: "{{route('web.project.create_bid')}}",
                        data: form_data,

                        success: function(response) {
                            success_toastr(response.message);
                            // console.log(response.data);
                        },

                        error: function(jqXHR, textStatus, errorThrown) {
                            error_toastr(errorThrown);
                        }

                    });
                })

                
			});
		});
	</script>
    @endsection
</x-web-layout>