<x-web-layout title="Profil" keyword="Warung Kerja">
    <div class="page-title page-title-small">
        <h2><a href="#" data-back-button><i class="fa fa-arrow-left"></i></a>Akun Saya</h2>
        <a href="#" data-menu="menu-main" class="bg-fade-highlight-light shadow-xl preload-img" data-src="{{asset('img/avatars/5s.png')}}"></a>
    </div>
    <div class="card header-card shape-rounded" data-card-height="150">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{asset('img/pictures/20s.jpg')}}"></div>
    </div>
    
    <div class="card card-style">
        <div class="content mt-0 mb-0">
            <div class="list-group list-custom-large check-visited">
                <a href="{{route('web.profile', Str::before($profile->email, '@'))}}">
                    <i class="fa fa-user font-20 color-blue-dark"></i>
                    <span>Profil</span>
                    <strong>Atur profil Anda</strong>
                    <i class="fa fa-angle-right"></i>
                </a>
                <a href="javascript:;">
                    <i class="fa fa-briefcase font-20 color-red-dark"></i>
                    <span>Proyek</span>
                    <strong>Proyek Saya</strong>
                    <i class="fa fa-angle-right"></i>
                </a>
                <a href="javascript:;">
                    <i class="fa fa-hand-holding-usd font-20 color-grey-dark"></i>
                    <span>Penawaran</span>
                    <strong>Penawaran Saya</strong>
                    <i class="fa fa-angle-right"></i>
                </a>
                <a href="javascript:;">
                    <i class="fa fa-shopping-bag font-20 color-brown-dark"></i>
                    <span>Produk</span>
                    <strong>Atur Produk Anda</strong>
                    <i class="fa fa-angle-right"></i>
                </a>
                <a href="javascript:;">
                    <i class="fa fa-shopping-cart font-20 color-pink-dark"></i>
                    <span>Pembelian</span>
                    <strong>Pembelian Saya</strong>
                    <i class="fa fa-angle-right"></i>
                </a>
                <a href="javascript:;">
                    <i class="fa fa-chart-line font-20 color-mint-dark"></i>
                    <span>Penjualan</span>
                    <strong>Penjualan Saya</strong>
                    <i class="fa fa-angle-right"></i>
                </a>
                <a href="javascript:;">
                    <i class="fa fa-handshake font-20 color-aqua-light"></i>
                    <span>Rujukan</span>
                    <strong>Rujukan Saya</strong>
                    <i class="fa fa-angle-right"></i>
                </a>
                <a href="javascript:;">
                    <i class="fa fa-money-bill font-20 color-green-dark"></i>
                    <span>Keuangan</span>
                    <strong>Atur keuangan Anda</strong>
                    <i class="fa fa-angle-right"></i>
                </a>
                <a href="javascript:;">
                    <i class="fa fa-coins font-20 color-yellow-light"></i>
                    <span>Poin</span>
                    <strong>Poin Anda</strong>
                    <i class="fa fa-angle-right"></i>
                </a>
                <a href="{{route('web.auth.logout')}}">
                    <i class="fa fa-sign-out-alt font-20 color-magenta-dark"></i>
                    <span>Keluar</span>
                    <strong>Keluar dari akun Anda</strong>
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </div>
</x-web-layout>