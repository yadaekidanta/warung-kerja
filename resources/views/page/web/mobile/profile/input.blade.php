<x-web-layout title="Ubah Profil" keyword="Warung Kerja">
    <div class="page-title page-title-small">
        <h2><a href="#" data-back-button><i class="fa fa-arrow-left"></i></a>Ubah Profil</h2>
        <a href="#" data-menu="menu-main" class="bg-fade-highlight-light shadow-xl preload-img" data-src="{{asset('img/avatars/5s.png')}}"></a>
    </div>
    <div class="card header-card shape-rounded" data-card-height="150">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{asset('img/pictures/20s.jpg')}}"></div>
    </div>    
    <div class="card card-style">
        <div class="content mb-0">
            <h3 class="font-600">Basic Info</h3>
            <p>
                Basic details about you. Set them here. These can be connected to your database and shown on the user profile.
            </p>
            <form id="form_basic_info">
                <div class="input-style has-borders hnoas-icon input-style-always-active validate-field mb-4">
                    <input type="name" class="form-control validate-name" placeholder="John Doe" readonly>
                    <label for="form1" class="color-highlight font-400 font-13">Nama</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(dibutuhkan)</em>
                </div>
                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">
                    <input type="email" class="form-control validate-email" placeholder="name@domain.com" readonly>
                    <label for="form2" class="color-highlight font-400 font-13">Email</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(dibutuhkan)</em>
                </div>
                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">
                    <input type="tel" class="form-control validate-tel" placeholder="+1 234 567 8990" readonly>
                    <label for="form3" class="color-highlight font-400 font-13">No HP</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(dibutuhkan)</em>
                </div>
                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">
                    <input type="date" class="form-control validate-date" placeholder="{{date('Y-m-d')}}">
                    <label for="form3" class="color-highlight font-400 font-13">Tanggal Lahir</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(dibutuhkan)</em>
                </div>
                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">
                    <input type="date" class="form-control validate-date" placeholder="{{date('Y-m-d')}}">
                    <label for="form3" class="color-highlight font-400 font-13">Jenis Kelamin</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(dibutuhkan)</em>
                </div>
                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">
                    <textarea name="alamat" class="form-control validate-text" cols="30" rows="10"></textarea>
                    <label for="form44" class="color-highlight font-400 font-13">Alamat</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(dibutuhkan)</em>
                </div>
                <a href="#" class="btn btn-full btn-margins bg-highlight rounded-sm shadow-xl btn-m text-uppercase font-900">Simpan perubahan</a>
            </form>
        </div>
    </div>
    <div class="card card-style">
        <div class="content mb-0">
            <h3 class="font-600">Keamanan Akun</h3>
            <p>
                Basic details about you. Set them here. These can be connected to your database and shown on the user profile.
            </p>
            <form id="form_password">
                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">
                    <input type="password" class="form-control validate-password" id="form4" placeholder="******">
                    <label for="form4" class="color-highlight font-400 font-13">Kata Sandi saat ini</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(dibutuhkan)</em>
                </div>
                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">
                    <input type="password" class="form-control validate-password" id="form4" placeholder="******">
                    <label for="form4" class="color-highlight font-400 font-13">Kata Sandi</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(dibutuhkan)</em>
                </div>
                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">
                    <input type="password" class="form-control validate-password" id="form4" placeholder="******">
                    <label for="form4" class="color-highlight font-400 font-13">Konfirmasi Kata Sandi</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(dibutuhkan)</em>
                </div>
                <a href="#" class="btn btn-full btn-margins bg-highlight rounded-sm shadow-xl btn-m text-uppercase font-900">Ubah Kata Sandi</a>
            </form>
        </div>
    </div>
</x-web-layout>