<x-web-layout title="Profil Saya" keyword="Warung Kerja">
    <div class="page-title page-title-small">
        <h2><a href="#" data-back-button><i class="fa fa-arrow-left"></i></a>Edit Profile</h2>
        <a href="#" data-menu="menu-main" class="bg-fade-highlight-light shadow-xl preload-img" data-src="{{asset('img/avatars/5s.png')}}"></a>
    </div>
    <div class="card header-card shape-rounded" data-card-height="150">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{asset('img/pictures/20s.jpg')}}"></div>
    </div>
    
    <div class="card card-style">
        <div class="d-flex content mb-1">
            <!-- left side of profile -->
            <div class="flex-grow-1">
                <h1 class="font-700">{{Auth::guard('member')->user()->name}}<i class="fa fa-check-circle color-blue-dark float-end font-13 mt-2 me-3"></i></h1>
                <p class="mb-2">
                    Karla Black is a name used when you generate a text with an annonymous name.
                </p>
                <p class="font-10">
                    <strong class="color-theme pe-1">1k</strong>Poin
                    <strong class="color-theme ps-3 pe-1">342</strong>Proyek
                </p>
            </div>
            <!-- right side of profile. increase image width to increase column size-->
            <img src="{{asset('img/empty.png')}}" data-src="{{asset('img/avatars/4s.png')}}" width="115" class="bg-highlight rounded-circle mt-3 shadow-xl preload-img">
        </div>
        <!-- follow buttons-->
        <div class="content">
            <div class="row mb-0">
                <div class="col-6">
                    <a href="{{route('web.profile.edit')}}" class="btn btn-full btn-sm rounded-s text-uppercase font-900 bg-blue-dark">Edit</a>
                </div>
                <div class="col-6">
                    <a href="#" class="btn btn-full btn-sm btn-border rounded-s text-uppercase font-900 color-highlight border-blue-dark">Message</a>
                </div>
            </div>
        </div>
        <div class="divider mb-3 mt-1"></div>
    </div>
</x-web-layout>