<x-web-layout title="Masuk / Daftar" keyword="Warung Kerja">
    <div class="page-title page-title-small">
        <img src="{{asset('img/icon.png')}}" class="preload-img img-fluid rounded-circle entered loaded" style="width:15%;float:right;"/>
        <h2>Masuk / Buat Akun</h2>
    </div>
    <div class="card header-card shape-rounded" data-card-height="150">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{asset('img/logo.png')}}"></div>
    </div>
    <div class="card card-style">
        <div id="login_page">
            <div class="content mt-2 mb-0">
                <form id="form_login">
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-envelope"></i>
                        <input type="email" class="form-control validate-email" id="email" name="email" placeholder="Email">
                        <label for="email" class="color-blue-dark font-10 mt-1">Email</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(diperlukan)</em>
                    </div>
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-lock"></i>
                        <input type="password" class="form-control validate-name" id="password" name="password" placeholder="Kata Sandi">
                        <label for="password" class="color-blue-dark font-10 mt-1">Kata Sandi</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(diperlukan)</em>
                    </div>
                    <button id="tombol_login" onclick="do_auth('#tombol_login','#form_login','{{route('web.auth.login')}}','POST');" class="btn btn-m mt-2 mb-4 btn-full bg-green-dark rounded-sm text-uppercase font-900">Masuk</button>
                </form>
                <div class="divider d-none"></div>
                <a href="#" class="btn btn-icon btn-m btn-full shadow-l bg-facebook text-uppercase font-900 text-start d-none"><i class="fab fa-facebook-f text-center"></i>Login with Facebook</a>
                <a href="#" class="btn btn-icon btn-m mt-2 btn-full shadow-l bg-twitter text-uppercase font-900 text-start d-none"><i class="fab fa-twitter text-center"></i>Login with Twitter</a>
                <div class="divider mt-4 mb-3 d-none"></div>
                <div class="d-flex">
                    <div class="w-50 font-11 pb-2 color-theme opacity-60 pb-3 text-start">
                        <a href="#" onclick="auth_content('register_page');" class="color-theme">Buat Akun</a>
                    </div>
                    <div class="w-50 font-11 pb-2 color-theme opacity-60 pb-3 text-end">
                        <a href="#" onclick="auth_content('forgot_page');" class="color-theme">Lupa Kata Sandi ?</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="register_page">
            <div class="content mt-2 mb-0">
                <form id="form_register">
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-user"></i>
                        <input type="text" class="form-control validate-name" id="name" name="name" placeholder="Nama Lengkap">
                        <label for="fullname" class="color-blue-dark font-10 mt-1">Nama Lengkap</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(diperlukan)</em>
                    </div>
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-envelope"></i>
                        <input type="email" class="form-control validate-email" id="email_register" name="email" placeholder="Email">
                        <label for="form1a" class="color-blue-dark font-10 mt-1">Email</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(diperlukan)</em>
                    </div>
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-phone"></i>
                        <input type="tel" class="form-control validate-tel" id="phone" name="phone" placeholder="No HP">
                        <label for="form1a" class="color-blue-dark font-10 mt-1">No HP</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(diperlukan)</em>
                    </div>
                    <div class="input-style no-borders has-icon validate-field mb-4">
                        <i class="fa fa-lock"></i>
                        <input type="password" class="form-control validate-password" id="password_register" name="password" placeholder="Kata Sandi">
                        <label for="form3a" class="color-blue-dark font-10 mt-1">Kata Sandi</label>
                        <i class="fa fa-times disabled invalid color-red-dark"></i>
                        <i class="fa fa-check disabled valid color-green-dark"></i>
                        <em>(diperlukan)</em>
                    </div>
                    <button id="tombol_register" onclick="do_auth('#tombol_register','#form_register','{{route('web.auth.register')}}','POST');" class="btn btn-m mt-2 mb-4 btn-full bg-green-dark rounded-sm text-uppercase font-900">Buat Akun</button>
                </form>
                <p class="text-center">
                    <a href="#" onclick="auth_content('login_page');"" class="color-highlight opacity-80 font-12">Sudah terdaftar ? Masuk di sini</a>
                </p>
                <div class="divider d-none"></div>
                <a href="#" class="btn btn-icon btn-m btn-full shadow-l bg-facebook text-uppercase font-900 text-start d-none"><i class="fab fa-facebook-f text-center"></i>Login with Facebook</a>
                <a href="#" class="btn btn-icon btn-m mt-2 btn-full shadow-l bg-twitter text-uppercase font-900 text-start d-none"><i class="fab fa-twitter text-center"></i>Login with Twitter</a>
                <div class="divider mt-4 mb-3 d-none"></div>
            </div>
        </div>
        <div id="forgot_page">
            <div class="content mt-2 mb-0">
                <div class="input-style no-borders has-icon validate-field mb-4">
                    <i class="fa fa-envelope"></i>
                    <input type="email" class="form-control validate-email" id="form1a" placeholder="Email">
                    <label for="form1a" class="color-blue-dark font-10 mt-1">Email</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(diperlukan)</em>
                </div>
                <a href="#" class="btn btn-m mt-2 mb-4 btn-full bg-green-dark rounded-sm text-uppercase font-900">Minta Kata Sandi</a>
                <p class="text-center">
                    <a href="#" onclick="auth_content('login_page');"" class="color-highlight opacity-80 font-12">Sudah ingat akun Anda ? Masuk di sini</a>
                </p>
                <div class="divider mt-4 mb-3 d-none"></div>
                <div class="d-flex d-none">
                    <div class="w-50 font-11 pb-2 color-theme opacity-60 pb-3 text-start">
                        <a href="#" onclick="auth_content('register_page');" class="color-theme">Buat Akun</a>
                    </div>
                    <div class="w-50 font-11 pb-2 color-theme opacity-60 pb-3 text-end">
                        <a href="#" onclick="auth_content('forgot_page');" class="color-theme">Lupa Kata Sandi ?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('custom_js')
        <script type="text/javascript">
            auth_content('login_page');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
    @endsection
</x-web-layout>