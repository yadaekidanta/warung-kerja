<x-web-layout title="Profil Saya" keyword="Warung Kerja">
    <div class="page-title page-title-small">
        <h2><a href="#" data-back-button><i class="fa fa-arrow-left"></i></a>Edit Profile</h2>
        <a href="#" data-menu="menu-main" class="bg-fade-highlight-light shadow-xl preload-img" data-src="{{asset('img/avatars/5s.png')}}"></a>
    </div>
    <div class="card header-card shape-rounded" data-card-height="150">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{asset('img/pictures/20s.jpg')}}"></div>
    </div>
    <div class="card card-style bg-20">
        <div class="card-body text-center pt-4 pb-4">
            <h1 class="color-white">Kode QR saya</h1>
            <img width="200" class="qr-image generate-qr-auto mx-auto polaroid-effect shadow-l mb-3" src="">
        </div>
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
    </div>
    @section('custom_js')
        <script type="text/javascript">
            $("#generate_qr").click();
        </script>
    @endsection
</x-web-layout>