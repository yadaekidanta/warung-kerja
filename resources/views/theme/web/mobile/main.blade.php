<!DOCTYPE HTML>
<html lang="en">
@include('theme.web.mobile.head')
@yield('custom_css')
<body class="theme-light" data-highlight="blue2">
    <div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>
    <div id="page">
        <!-- header and footer bar go here-->
        @include('theme.web.mobile.header')
        @include('theme.web.mobile.menu')
        <div class="page-content">
            {{$slot}}
            <!-- footer and footer card-->
            <div class="footer">
                @include('theme.web.mobile.footer')
            </div>
        </div>
        <div id="custom_notif" data-dismiss="custom_notif" data-bs-delay="3000" data-bs-autohide="true" class="notification bg-blue-dark shadow-xl fade hide">
            <div class="toast-header bg-highlight">
                <strong id="notif_messages" class="me-auto color-white"></strong>
                <small class="color-white opacity-50 me-2">Baru Saja</small>
            </div>
        </div>
        <!-- end of page content-->
        <div id="menu-share" 
            class="menu menu-box-bottom menu-box-detached rounded-m" 
            data-menu-height="420" 
            data-menu-effect="menu-over">
            @include('theme.web.mobile.share')
        </div>    
        <div id="menu-highlights" 
            class="menu menu-box-bottom menu-box-detached rounded-m" 
            data-menu-height="510" 
            data-menu-effect="menu-over">
            @include('theme.web.mobile.highlights')
        </div>
        <div id="menu-main"
            class="menu menu-box-right menu-box-detached rounded-m"
            data-menu-width="260"
            data-menu-active="nav-welcome"
            data-menu-effect="menu-over">
            @include('theme.web.mobile.menu-main')
        </div>
        <!-- Be sure this is on your main visiting page, for example, the index.html page-->
        <!-- Install Prompt for Android -->
        @include('theme.web.mobile.install')    
    </div>
    @include('theme.web.mobile.js')
    @yield('custom_js')
</body>
</html>