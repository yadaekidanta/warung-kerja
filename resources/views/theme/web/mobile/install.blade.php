<div id="menu-install-pwa-android" class="menu menu-box-bottom menu-box-detached rounded-l"
        data-menu-height="350" 
    data-menu-effect="menu-parallax">
    <div class="boxed-text-l mt-4">
        <img class="rounded-l mb-3" src="{{asset('img/icon.PNG')}}" alt="img" width="90">
        <h4 class="mt-3">Warung Kerja di layar anda</h4>
        <p>
            Pasang Warung Kerja di layar anda, dan mengaksesnya seperti aplikasi biasa. Ini benar-benar sederhana!
        </p>
        <a href="#" class="pwa-install btn btn-s rounded-s shadow-l text-uppercase font-900 bg-highlight mb-2">Tambahkan ke Layar Utama</a><br>
        <a href="#" class="pwa-dismiss close-menu color-gray2-light text-uppercase font-900 opacity-60 font-10">Mungkin nanti</a>
        <div class="clear"></div>
    </div>
</div>   
<!-- Install instructions for iOS -->
<div id="menu-install-pwa-ios" 
    class="menu menu-box-bottom menu-box-detached rounded-l"
        data-menu-height="320" 
    data-menu-effect="menu-parallax">
    <div class="boxed-text-xl mt-4">
        <img class="rounded-l mb-3" src="{{asset('img/logo.svg')}}" alt="img" width="90">
        <h4 class="mt-3">Warung Kerja di layar anda</h4>
        <p class="mb-0 pb-3">
            Install Warung Kerja di layar anda, dan mengaksesnya seperti aplikasi biasa. Buka menu Safari Anda dan ketuk "Tambahkan ke Layar Beranda".
        </p>
        <div class="clear"></div>
        <a href="#" class="pwa-dismiss close-menu color-highlight font-800 opacity-80 text-center text-uppercase">Mungkin nanti</a><br>
        <i class="fa-ios-arrow fa fa-caret-down font-40"></i>
    </div>
</div>