<h1 class="text-center font-700 font-26 mt-3 pt-2">Bagikan Warung Kerja</h1>
<p class="boxed-text-xl under-heading">
    Bagikan halaman kami dengan dunia, tingkatkan <br>
    eksposur halaman Anda dengan dunia.
</p>
<div class="divider divider-margins"></div>

<div class="px-4">
    <div class="row text-center mb-0">
        <div class="col-3 mb-n2">
            <a href="javascript:;" class="external-link shareToFacebook icon icon-l bg-facebook rounded-s shadow-l"><i class="fab fa-facebook-f font-22"></i><br></a>
            <p class="font-11 opacity-70">Facebook</p>
        </div>
        <div class="col-3 mb-n2">
            <a href="javascript:;" class="external-link shareToTwitter icon icon-l bg-twitter rounded-s shadow-l"><i class="fab fa-twitter font-22"></i><br></a>
            <p class="font-11 opacity-70">Twitter</p>
        </div>
        <div class="col-3 mb-n2">
            <a href="javascript:;" class="external-link shareToLinkedIn icon icon-l bg-linkedin rounded-s shadow-l"><i class="fab fa-linkedin-in font-22"></i><br></a>
            <p class="font-11 opacity-70">LinkedIn</p>
        </div>
        <div class="col-3 mb-n2">
            <a href="javascript:;" class="external-link shareToMail icon icon-l bg-mail rounded-s shadow-l"><i class="fa fa-envelope font-22"></i><br></a>
            <p class="font-11 opacity-70">Email</p>
        </div>
        <div class="col-3 mb-n2">
            <a href="javascript:;" class="external-link shareToWhatsApp icon icon-l bg-whatsapp rounded-s shadow-l"><i class="fab fa-whatsapp font-22"></i><br></a>
            <p class="font-11 opacity-70">WhatsApp</p>
        </div>
        <div class="col-3 mb-n2">
            <a href="javascript:;" class="shareToCopyLink icon icon-l bg-blue-dark rounded-s shadow-l">
                <i class="fa fa-link not-copied font-22"></i>
                <i class="fa fa-check copied disabled font-22"></i>
            <br></a>
            <p class="font-11 opacity-70">Copy URL</p>
        </div>
        <div class="col-3 mb-n2">
            <a href="javascript:;" class="external-link shareToPinterest icon icon-l bg-pinterest rounded-s shadow-l"><i class="fab fa-pinterest-p font-22"></i><br></a>
            <p class="font-11 opacity-70">Pinterest</p>
        </div>
        <div class="col-3 mb-n2">
            <a href="javascript:;" class="close-menu icon icon-l bg-red-dark rounded-s shadow-l"><i class="fa fa-times font-22"></i><br></a>
            <p class="font-11 opacity-70">Close</p>
        </div>
    </div>
</div>

<div class="divider divider-margins mt-n1 mb-3"></div>
<p class="text-center font-10 mb-0">Hak Cipta &copy; 2020 - <span class="copyright-year">{{date('Y')}}</span> - Warung Kerja. Semua Hak Dilindungi Undang-undang.</p>