<script type="text/javascript" src="{{asset('azures/scripts/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('azures/scripts/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
@guest
<script type="text/javascript" src="{{asset('js/auth-mobile.js')}}"></script>
@endguest