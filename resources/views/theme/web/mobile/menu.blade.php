<div id="footer-bar" class="footer-bar-5">
    <a href="{{route('web.home')}}" class="{{request()->is('/') ? 'active-nav' : ''}}"><i data-feather="home" data-feather-line="1" data-feather-size="21" data-feather-color="blue-dark" data-feather-bg="blue-fade-light"></i><span>Beranda</span></a>
    <a href="{{route('web.project')}}" class="{{request()->is('project') ? 'active-nav' : ''}}"><i data-feather="heart" data-feather-line="1" data-feather-size="21" data-feather-color="red-dark" data-feather-bg="red-fade-light"></i><span>Proyek</span></a>
    <a href="{{route('web.product')}}" class="{{request()->is('product') ? 'active-nav' : ''}}"><i data-feather="image" data-feather-line="1" data-feather-size="21" data-feather-color="green-dark" data-feather-bg="green-fade-light"></i><span>Produk</span></a>
    @if(Auth::guard('member')->check())
    <a href="{{route('web.product')}}" class="{{request()->is('/') ? 'active-nav' : ''}}"><i data-feather="file" data-feather-line="1" data-feather-size="21" data-feather-color="brown-dark" data-feather-bg="brown-fade-light"></i><span>Pesan</span></a>
    <a href="{{route('web.account')}}" class="{{request()->is('account') || request()->is('profile/*')  ? 'active-nav' : ''}}"><i data-feather="user" data-feather-line="1" data-feather-size="21" data-feather-color="dark-dark" data-feather-bg="gray-fade-light"></i><span>Akun</span></a>
    @else
    <a href="{{route('web.auth')}}" class="{{request()->is('auth')  ? 'active-nav' : ''}}"><i data-feather="log-in" data-feather-line="1" data-feather-size="21" data-feather-color="dark-dark" data-feather-bg="gray-fade-light"></i><span>Masuk</span></a>
    @endif
</div>