@php
function getAvatar()
{
    $photo = Auth::guard('member')->user()->photo ?? '';
    if($photo){
        return asset('storage/' . Auth::guard('member')->user()->photo);
    }
    else{
        return asset('img/avatars/5m.png');
    }
}
@endphp
<header id="header" class="header-size-sm" data-sticky-shrink="false">
    <div class="container">
        <div class="header-row">
            <nav class="navbar navbar-expand-lg p-0 m-0 w-100">
                <div id="logo" style="margin-top:1%;margin-bottom:1%;">
                    <a href="javascript:;" class="standard-logo">
                        <img src="{{asset('img/logo.png')}}" style="height:30px;" alt="{{config('app.name')}}">
                    </a>
                    <a href="javascript:;" class="retina-logo">
                        <img src="{{asset('img/logo.png')}}" style="height:30px;" alt="{{config('app.name')}}">
                    </a>
                </div>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-line-menu"></span>
                </button>
                <div class="collapse navbar-collapse align-items-end" id="navbarNav">
                    <ul class="navbar-nav ms-auto">
                        <li class="nav-item {{request()->is('/') ? 'active' : ''}}">
                            <a class="nav-link" href="{{route('web.home')}}">Halaman Utama</a>
                        </li>
                        <li class="nav-item dropdown d-none">
                            <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Members
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Dropdown Item 1</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Another Dropdown Item</a>
                            </div>
                        </li>
                        <li class="nav-item d-none">
                            <a href="javascript:;" class="nav-link">Lowongan Kerja</a>
                        </li>
                        <li class="nav-item {{request()->is('project') ? 'active' : ''}}">
                            <a href="{{route('web.project')}}" class="nav-link">Proyek</a>
                        </li>
                        <li class="nav-item {{request()->is('product') ? 'active' : ''}}">
                            <a href="{{route('web.product')}}" class="nav-link">Produk</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <div id="header-wrap" class="bg-light">
        <div class="container">
            <div class="header-row flex-row-reverse flex-lg-row justify-content-between">
                <div class="header-misc">
                    @if(!Auth::guard('member')->check())
                    <div class="header-buttons me-3">
                        <a href="{{route('web.auth')}}" class="button button-rounded button-border button-small m-0">Masuk</a>
                        <a href="{{route('web.auth.view.register')}}" class="button button-rounded button-small m-0 ms-2">Daftar</a>
                    </div>
                    @else
                    <!-- Top Search ============================================= -->
                    <div id="top-search" class="header-misc-icon">
                        <a href="javascript:;" id="top-search-trigger"><i class="icon-line-search"></i><i class="icon-line-cross"></i></a>
                    </div>
                    <!-- #top-search end -->
                    <!-- Top Cart ============================================= -->
                    <div id="top-cart" class="header-misc-icon d-none d-sm-block">
                        <a href="javascript:;" id="top-cart-trigger"><i class="icon-line-bag"></i><span class="top-cart-number">5</span></a>
                        <div class="top-cart-content">
                            <div class="top-cart-title">
                                <h4>Keranjang Belanja</h4>
                            </div>
                            <div class="top-cart-items">
                                <div class="top-cart-item">
                                    <div class="top-cart-item-image">
                                        <a href="#"><img src="{{asset('img/product/1.jpg')}}" alt="Blue Round-Neck Tshirt" /></a>
                                    </div>
                                    <div class="top-cart-item-desc">
                                        <div class="top-cart-item-desc-title">
                                            <a href="#">Blue Round-Neck Tshirt with a Button</a>
                                            <span class="top-cart-item-price d-block">$19.99</span>
                                        </div>
                                        <div class="top-cart-item-quantity">x 2</div>
                                    </div>
                                </div>
                                <div class="top-cart-item">
                                    <div class="top-cart-item-image">
                                        <a href="#"><img src="{{asset('img/product/6.jpg')}}" alt="Light Blue Denim Dress" /></a>
                                    </div>
                                    <div class="top-cart-item-desc">
                                        <div class="top-cart-item-desc-title">
                                            <a href="#">Light Blue Denim Dress</a>
                                            <span class="top-cart-item-price d-block">$24.99</span>
                                        </div>
                                        <div class="top-cart-item-quantity">x 3</div>
                                    </div>
                                </div>
                            </div>
                            <div class="top-cart-action">
                                <span class="top-checkout-price">$114.95</span>
                                <a href="{{route('web.cart')}}" class="button button-3d button-small m-0">View Cart</a>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown mx-3 me-lg-0">
                        <a href="javascript:;" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <img src="{{ getAvatar()}}"
                                class="img-circle img-thumbnail" style="width: 30px; height: 30px; object-fit: cover;">
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenu1">
                            <a class="dropdown-item text-start" href="{{route('web.account')}}">Akun Saya</a>
                            <a class="dropdown-item text-start d-flex" style="align-items: center; justify-content: space-between;" href="javascript:;">
                                <span>Pesan</span>        
                                <span class="bg-info text-light float-end" style="width: 25px; height: 25px; border-radius: 100%; display: flex; justify-content: center; align-items: center;">5</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item text-start" href="{{route('web.auth.logout')}}">Keluar <i class="icon-signout"></i></a>
                        </ul>
                    </div>
                    @endif
                    <!-- #top-cart end -->
                </div>
                <div id="primary-menu-trigger">
                    <svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
                </div>
                <!-- Primary Navigation ============================================= -->
                <nav class="primary-menu with-arrows">
                    <ul class="menu-container">
                        <li class="menu-item"><a class="menu-link" href="javascript:;" class="ps-0"><div><i class="icon-line-grid"></i>Semua Kategori</div></a>
                            <ul class="sub-menu-container">
                                <li class="menu-item"><a class="menu-link" href="#"><div><i class="icon-line2-user"></i>Teacher Training</div></a>
                                    <ul class="sub-menu-container">
                                        <li class="menu-item"><a class="menu-link" href="#"><div>All Teacher Training</div></a>
                                            <ul class="sub-menu-container">
                                                <li class="menu-item"><a class="menu-link" href="#"><div>Level 3</div></a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Educational Training</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Teaching Tools</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Others</div></a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a class="menu-link" href="#"><div><i class="icon-chart-bar1"></i>Business</div></a>
                                    <ul class="sub-menu-container">
                                        <li class="menu-item"><a class="menu-link" href="#"><div>All Business Classes</div></a>
                                            <ul class="sub-menu-container">
                                                <li class="menu-item"><a class="menu-link" href="#"><div>Level 3</div></a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Finance</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Sales</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Marketing</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Industry</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Real Estates</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Others</div></a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a class="menu-link" href="#"><div><i class="icon-code1"></i>Development</div></a>
                                    <ul class="sub-menu-container">
                                        <li class="menu-item"><a class="menu-link" href="#"><div>All Development Training</div></a>
                                            <ul class="sub-menu-container">
                                                <li class="menu-item"><a class="menu-link" href="#"><div>Level 3</div></a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Software Training</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Graphics Tools</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Development Skills</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Others</div></a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a class="menu-link" href="#"><div><i class="icon-line2-screen-smartphone"></i>IT & Software</div></a>
                                    <ul class="sub-menu-container">
                                        <li class="menu-item"><a class="menu-link" href="#"><div>All IT & Software Training</div></a>
                                            <ul class="sub-menu-container">
                                                <li class="menu-item"><a class="menu-link" href="#"><div>Level 3</div></a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Software Training</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Application Tools</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Others</div></a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a class="menu-link" href="#"><div><i class="icon-music1"></i>Music</div></a>
                                    <ul class="sub-menu-container">
                                        <li class="menu-item"><a class="menu-link" href="#"><div>All Music Classes</div></a>
                                            <ul class="sub-menu-container">
                                                <li class="menu-item"><a class="menu-link" href="#"><div>Level 3</div></a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Instrumental</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Vocals</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Lyrics</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Others</div></a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a class="menu-link" href="#"><div><i class="icon-line2-game-controller"></i>Lifestyle</div></a>
                                    <ul class="sub-menu-container">
                                        <li class="menu-item"><a class="menu-link" href="#"><div>All Lifestyle Training</div></a>
                                            <ul class="sub-menu-container">
                                                <li class="menu-item"><a class="menu-link" href="#"><div>Level 3</div></a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Art &amp; Crafts</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Foods & Beverages</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Gaming</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Others</div></a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a class="menu-link" href="#"><div><i class="icon-line2-globe"></i>Language</div></a>
                                    <ul class="sub-menu-container">
                                        <li class="menu-item"><a class="menu-link" href="#"><div>All Languages</div></a>
                                            <ul class="sub-menu-container">
                                                <li class="menu-item"><a class="menu-link" href="#"><div>Level 3</div></a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>English</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Spanish</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Germans</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Hindi</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Russian</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Others</div></a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a class="menu-link" href="#"><div><i class="icon-health"></i>Health &amp; Fitness</div></a>
                                    <ul class="sub-menu-container">
                                        <li class="menu-item"><a class="menu-link" href="#"><div>All Health &amp; Fitness</div></a>
                                            <ul class="sub-menu-container">
                                                <li class="menu-item"><a class="menu-link" href="#"><div>Level 3</div></a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Yoga</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Gym</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Sports</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Nutrition</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Others</div></a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a class="menu-link" href="#"><div><i class="icon-line2-crop"></i>Design</div></a>
                                    <ul class="sub-menu-container">
                                        <li class="menu-item"><a class="menu-link" href="#"><div>All Designs</div></a>
                                            <ul class="sub-menu-container">
                                                <li class="menu-item"><a class="menu-link" href="#"><div>Level 3</div></a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Game Design</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Graphic Design</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Web Design</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Others</div></a></li>
                                    </ul>
                                </li>

                                <li class="menu-item"><a class="menu-link" href="#"><div><i class="icon-food"></i>Food</div></a>
                                    <ul class="sub-menu-container">
                                        <li class="menu-item"><a class="menu-link" href="#"><div>All Food Recipes</div></a>
                                            <ul class="sub-menu-container">
                                                <li class="menu-item"><a class="menu-link" href="#"><div>Level 3</div></a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Heathy Foods</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Fast Foods</div></a></li>
                                        <li class="menu-item"><a class="menu-link" href="#"><div>Others</div></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>

                </nav><!-- #primary-menu end -->

                <form class="top-search-form" action="search.html" method="get">
                    <input type="text" name="q" class="form-control" value="" placeholder="Ketik &amp; Tekan Enter.." autocomplete="off">
                </form>

            </div>
        </div>
    </div>
    <div class="header-wrap-clone"></div>
</header>