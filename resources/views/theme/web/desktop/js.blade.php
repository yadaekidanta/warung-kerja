<script src="{{asset('semicolon/js/jquery.js')}}"></script>
<script src="{{asset('semicolon/js/plugins.min.js')}}"></script>

<!-- Footer Scripts ============================================= -->
<script src="{{asset('semicolon/js/functions.js')}}"></script>
<script src="{{asset('js/toastr.js')}}"></script>
<script src="{{asset('js/plugin.js')}}"></script>
@guest
<script src="{{asset('js/auth.js')}}"></script>
@endguest
@if(Auth::guard('member')->user())
<script src="{{asset('js/method.js')}}"></script>
@endif