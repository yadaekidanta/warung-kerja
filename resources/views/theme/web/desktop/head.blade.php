<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Warung Kerja" />
	<meta name="description" content="Warung Kerja" />
	<meta name="keywords" content="{{$keyword ?? ''}}" />
	<link rel="canonical" href="https://warungkerja.com" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link rel="shortcut icon" href="{{asset('img/icon.png')}}" />
	<!-- Stylesheets ============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Istok+Web:400,700&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/style.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{asset('semicolon/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/magnific-popup.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{asset('semicolon/custom/css/et-line.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{asset('semicolon/css/custom.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<link rel="stylesheet" href="{{asset('semicolon/css/components/custom-datatable.css')}}" type="text/css" />
	

	<!-- select 2 library -->
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<!-- / -->

	<!-- datatable -->
	<link rel="stylesheet" type="text/css" href="{{asset('semicolon/css/colors.php?color=00b2ef')}}"/>
	<!-- / -->
	
	<!-- Hosting Demo Specific Stylesheet -->
	<link rel="stylesheet" href="{{asset('semicolon/custom/css/fonts.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/custom/course.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/toastr.css')}}" type="text/css" />
	<!-- / -->

	<!-- Document Title ============================================= -->
	<title>{{config('app.name') . ': ' .$title ?? config('app.name')}}</title>
	<style>
		.cursor_pointer{
			cursor: pointer;
		}
	</style>

	<x-head.tinymce-config/>
</head>