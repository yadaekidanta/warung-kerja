<section id="slider" class="slider-element min-vh-75">
    <div class="slider-inner">
        <div class="vertical-middle">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-md-7">
                        <div class="slider-title mb-5 dark clearfix">
                            <h2 class="text-white text-rotater mb-2" data-separator="," data-rotate="fadeIn" data-speed="3500">TEMUKAN FREELANCER DARI SELURUH PENJURU INDONESIA<span class="t-rotate text-white d-inline-block">Development,Photography,Teacher Training,Business,Lifestyle,Language,Health,Fitness,Music</span>.</h2>
                            <p class="lead mb-0 text-uppercase ls2" style="color: #CCC; font-size: 110%">Apa yang Anda cari?</p>
                        </div>
                        <div class="clear"></div>
                        <div class="input-group input-group-lg mt-1">
                            <input class="form-control rounded border-0" type="search" placeholder="Cari kebutuhan anda.." aria-label="Search">
                            <button class="btn" type="submit"><i class="icon-line-search fw-bold text-white"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- HTML5 Video Wrap ============================================= -->
        <div class="video-wrap">
            <video id="slide-video" poster="{{asset('img/video/poster.jpg')}}" preload="auto" loop autoplay muted>
                <source src='{{asset('img/video/1.mp4')}}' type='video/mp4' />
            </video>
            <div class="video-overlay" style="background: rgba(0,0,0,0.5); z-index: 1"></div>
        </div>

    </div>
</section>