<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    @include('theme.web.desktop.head')
	<body class="stretched">
		<!-- Document Wrapper ============================================= -->
		<div id="wrapper" class="clearfix">
			<!-- Top Bar ============================================= -->
			{{-- @include('theme.web.desktop.topbar') --}}
			<!-- Header ============================================= -->
			@include('theme.web.desktop.header')
			<!-- #header end -->
			<!-- Slider ============================================= -->
			@if (request()->is('/'))
				@include('theme.web.desktop.slider')
			@endif
			<!-- Content ============================================= -->
			{{$slot}}
			<!-- #content end -->
			<!-- Footer ============================================= -->
			@include('theme.web.desktop.footer')
			<!-- #footer end -->
		</div>
		<!-- #wrapper end -->
		<!-- Go To Top ============================================= -->
		<div id="gotoTop" class="icon-angle-up"></div>
		<!-- JavaScripts ============================================= -->
		@include('theme.web.desktop.js')
		@yield('custom_js')
	</body>
</html>