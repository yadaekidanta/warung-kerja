<footer id="footer" class="dark" style="background: url('{{asset('img/footer-bg.jpg')}}') repeat fixed; background-size: 100% 100%;">
    <div class="container">
        <!-- Footer Widgets ============================================= -->
        <div class="footer-widgets-wrap" style="margin-bottom: -5%;">
            <div class="row">
                <div class="col-6 col-md">
                    <img class="mb-1" src="{{asset('img/ye.png')}}" alt="Image" width="70" style="margin-top: -20%;">
                    <small class="d-block" style="margin-bottom: -5%;">
                        CV. Yada Ekidanta 
                    </small>
                    <p class="text-white-50" style="font-size: 16px;margin-bottom: 0%;">
                        </br>
                        Komplek Permata Buah Batu C 15B </br>
                        Bandung, 40287 - Indonesia
                    </p>
                    <a href="javascript:;" class="mb-2 d-block">
                        <i class="icon-line2-call-in me-2"></i>
                        +62-877-4800-5611
                    </a>
                    <a href="javascript:;"><i class="icon-line2-envelope me-2"></i>hello@warungkerja.com</a>
                    <div class="widget subscribe-widget" style="margin-top: 0%;">
                        <h4 class="text-uppercase ls2 fw-normal mt-3" style="margin-bottom: 0%;">Buletin</h4>
                        <h5><strong>Berlangganan</strong> surat Berita Kami untuk mendapatkan Berita Penting & Penawaran Menakjubkan:</h5>
                        <div class="widget-subscribe-form-result"></div>
                        <form id="widget-subscribe-form" action="include/subscribe.php" method="post" class="mb-0">
                            <div class="input-group mx-auto">
                                <div class="input-group-text"><i class="icon-email2"></i></div>
                                <input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Masukkan E-mail Anda">
                                <button class="button button-rounded button-small m-0 ms-2" type="submit">Berlangganan</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-6 col-md col-sm mt-4 mt-md-0 mt-lg-0 mt-xl-0">
                    <h4 class="text-uppercase ls2 fw-normal">Tentang Kami</h4>
                    <ul class="list-unstyled mb-0">
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Perusahaan Kami</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Pengelola</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Syarat Layanan</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Kebijakan Privasi</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">FAQ</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Blog</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Komunitas</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Kontak</a></h5></li>
                    </ul>
                </div>
                <div class="col-6 col-md">
                    <h4 class="text-uppercase ls2 fw-normal">Pengetahuan Dasar</h4>
                    <ul class="list-unstyled mb-0">
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Pengenalan</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Cara Kerja</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Keunggulan</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Aturan Main</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Menjadi Pemilik Proyek</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Menjadi Pekerja</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Menjadi Penjual</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Menjadi Afiliasi</a></h5></li>
                        <li><h5 class="mb-2 fw-normal"><a href="javascript:;">Tentang Arbitrase</a></h5></li>
                    </ul>
                </div>
            </div>

        </div><!-- .footer-widgets-wrap end -->

    </div>
    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">

        <div class="container" style="margin-top: -2%;margin-bottom: -3%;">

            <div class="row align-items-center justify-content-between">
                <div class="col-md-10">
                    <div class="copyright-links">
                        Hak Cipta &copy; 2020-{{date("Y")}} Semua Hak Dilindungi oleh CV. Yada Ekidanta.
                        <a href="javascript:;">Syarat Layanan</a> /
                        <a href="javascript:;">Kebijakan Privasi</a>
                    </div>
                    <p style="line-height: 1;text-align: justify;text-justify: inter-word;">
                        Warung Kerja adalah sebuah platform yang berfungsi sebagai ladang untuk para pencari kerja untuk mendapatkan pekerjaan sesuai bidang nya, freelancer dalam mencari
                        proyek atau tempat para designer berkreasi. Tentunya dengan platform bergaya friendly dan chill disertai dengan responsif, Kami akan merespon setiap konsumen dengan
                        baik sebagai salah satu moto Kami dan di harapkan dapat mengatasi berbagai masalah dalam mencari pekerjaan atau masalah kreativitas.
                    </p>
                </div>

                <div class="col-md-2 d-flex justify-content-md-end">
                    <div class="d-flex justify-content-center justify-content-md-end">
                        <a href="#" class="social-icon si-small si-borderless si-facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>
                        <a href="#" class="social-icon si-small si-borderless si-twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>
                        <a href="#" class="social-icon si-small si-borderless si-linkedin">
                            <i class="icon-linkedin"></i>
                            <i class="icon-linkedin"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #copyrights end -->
</footer>