<script src="https://cdn.tiny.cloud/1/l26ciu603r1v9i9m962kc5gk22kiz76sk9hkf1rr7rdbqlm5/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
 <script>
   tinymce.init({
    selector: 'textarea#description', 
    image_class_list: [
        {title: 'img-responsive', value: 'img-responsive'},
    ],
    menubar: false,
    height: 450,
    plugins: [
        "image",
        "searchreplace visualblocks code fullscreen",
        "table"
    ],
    media_live_embeds: true,
    // media_alt_source: false,
    // media_dimensions: false,
    // media_poster: false,
    image_title: true,
    toolbar: 'formatselect| bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table| image | fullscreen',
    automatic_uploads: false,
    file_picker_types: 'image',
    images_upload_handler: function (blobInfo, success, failure) {
        // console.log(blobInfo.filename());
        
        var xhr, formData;
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', '/home/profile/about/img');
        var token = '{{ csrf_token() }}';
        xhr.setRequestHeader("X-CSRF-Token", token);
        xhr.onload = function() {
            var json;
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
            json = JSON.parse(xhr.responseText);

            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
            success(json.location);
        };
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
        xhr.send(formData);
    },
    file_picker_callback: function(cb, value, meta) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.onchange = function() {
            var file = this.files[0];

            var reader = new FileReader();
            // reader.readAsDataURL(file);
            reader.onload = function () {
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
                cb(blobInfo.blobUri(), { title: file.name });
            };
            reader.readAsDataURL(file);
        };
        input.click();
    }
   });

   tinymce.init({
    selector: 'textarea.description', 
    image_class_list: [
        {title: 'img-responsive', value: 'img-responsive'},
    ],
    menubar: false,
    height: 450,
    plugins: [
        "image",
        "searchreplace visualblocks code fullscreen",
        "table"
    ],
    media_live_embeds: true,
    // media_alt_source: false,
    // media_dimensions: false,
    // media_poster: false,
    image_title: true,
    toolbar: 'formatselect| bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table| image | fullscreen',
    automatic_uploads: false,
    file_picker_types: 'image',
    images_upload_handler: function (blobInfo, success, failure) {
        // console.log(blobInfo.filename());
        
        var xhr, formData;
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', '/home/profile/about/img');
        var token = '{{ csrf_token() }}';
        xhr.setRequestHeader("X-CSRF-Token", token);
        xhr.onload = function() {
            var json;
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
            json = JSON.parse(xhr.responseText);

            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
            success(json.location);
        };
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
        xhr.send(formData);
    },
    file_picker_callback: function(cb, value, meta) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.onchange = function() {
            var file = this.files[0];

            var reader = new FileReader();
            // reader.readAsDataURL(file);
            reader.onload = function () {
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
                cb(blobInfo.blobUri(), { title: file.name });
            };
            reader.readAsDataURL(file);
        };
        input.click();
    }
   });

   tinymce.init({
    selector: 'textarea#message', 
    image_class_list: [
        {title: 'img-responsive', value: 'img-responsive'},
    ],
    menubar: false,
    height: 450,
    plugins: [
        "image",
        "searchreplace visualblocks code fullscreen",
        "table"
    ],
    media_live_embeds: true,
    // media_alt_source: false,
    // media_dimensions: false,
    // media_poster: false,
    image_title: true,
    toolbar: 'formatselect| bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table| image | fullscreen',
    automatic_uploads: false,
    file_picker_types: 'image',
    images_upload_handler: function (blobInfo, success, failure) {
        // console.log(blobInfo.filename());
        
        var xhr, formData;
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', '/home/profile/about/img');
        var token = '{{ csrf_token() }}';
        xhr.setRequestHeader("X-CSRF-Token", token);
        xhr.onload = function() {
            var json;
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
            json = JSON.parse(xhr.responseText);

            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
            success(json.location);
        };
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
        xhr.send(formData);
    },
    file_picker_callback: function(cb, value, meta) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.onchange = function() {
            var file = this.files[0];

            var reader = new FileReader();
            // reader.readAsDataURL(file);
            reader.onload = function () {
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
                cb(blobInfo.blobUri(), { title: file.name });
            };
            reader.readAsDataURL(file);
        };
        input.click();
    }
   });

   tinymce.init({
    selector: 'textarea#messagesecond', 
    image_class_list: [
        {title: 'img-responsive', value: 'img-responsive'},
    ],
    menubar: false,
    height: 450,
    plugins: [
        "image",
        "searchreplace visualblocks code fullscreen",
        "table"
    ],
    media_live_embeds: true,
    // media_alt_source: false,
    // media_dimensions: false,
    // media_poster: false,
    image_title: true,
    toolbar: 'formatselect| bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table| image | fullscreen',
    automatic_uploads: false,
    file_picker_types: 'image',
    images_upload_handler: function (blobInfo, success, failure) {
        // console.log(blobInfo.filename());
        
        var xhr, formData;
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', '/home/profile/about/img');
        var token = '{{ csrf_token() }}';
        xhr.setRequestHeader("X-CSRF-Token", token);
        xhr.onload = function() {
            var json;
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
            json = JSON.parse(xhr.responseText);

            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
            success(json.location);
        };
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
        xhr.send(formData);
    },
    file_picker_callback: function(cb, value, meta) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.onchange = function() {
            var file = this.files[0];

            var reader = new FileReader();
            // reader.readAsDataURL(file);
            reader.onload = function () {
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
                cb(blobInfo.blobUri(), { title: file.name });
            };
            reader.readAsDataURL(file);
        };
        input.click();
    }
   });

   tinymce.init({
    selector: 'textarea#comment', 
    image_class_list: [
        {title: 'img-responsive', value: 'img-responsive'},
    ],
    menubar: false,
    height: 450,
    plugins: [
        "searchreplace visualblocks code fullscreen",
        "table link lists"
    ],
    toolbar: 'formatselect| bold italic| bullist numlist | link |fullscreen',
    automatic_uploads: false
   });

 </script>